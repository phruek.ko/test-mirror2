package com.eggdigital.rpp.edcapp.service;

import com.eggdigital.rpp.edcapp.service.impl.TransactionServiceImpl;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by bombay on 10/17/2017 AD.
 */
public class TransactionServiceTest {

    private TransactionServiceImpl transactionService;

    @Before
    public void setUp() throws Exception {
        transactionService = new TransactionServiceImpl();
    }

    @Test
    public void genBatchId() throws Exception {
        for (int i = 1; i <= 5; i++) {
            Assert.assertEquals("00000${i}".replace("${i}", String.valueOf(i)), StringUtils.leftPad(String.valueOf(i), 6, "0"));
        }
    }

    @Test
    public void endCodeBase64UsernamePassword() throws Exception {
        String authStr = "egg1" + ":" + "1234";
        String authEncoded = Base64.encodeBase64String(authStr.getBytes());
        Assert.assertEquals("ZWdnMToxMjM0", authEncoded);
    }
}
