package com.eggdigital.rpp.edcapp.controller;

import com.eggdigital.rpp.edcapp.dto.RedeemRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by jaruwan.yen on 10/16/2017 AD.
 */
@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class RedeemRestTest {
    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    private RedeemRequest redeemRequestDto;

    private ObjectMapper objectMapper;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        objectMapper = new ObjectMapper();

        redeemRequestDto = new RedeemRequest();
        redeemRequestDto.setBrandId("0006163");
        redeemRequestDto.setOutletId("00001");
        redeemRequestDto.setTerminalId("88000001");
        redeemRequestDto.setTxRefId("11111");
        redeemRequestDto.setAccType("THAIID");
        redeemRequestDto.setAccValue("1100400055471");
        redeemRequestDto.setCampaignCode("101");
    }

    @Test
    public void whenCallApiRedeemByCampaign_ShouldBeStatusOk() throws Exception {
        mockMvc.perform(post("/redeems/campaign")
                .header("X-Egg-Signature")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(redeemRequestDto))
        ).andExpect(status().isOk());
    }

    @Test
    public void whenCallApiRedeemByCampaign_ShouldBeStatusNotFound() throws Exception {
        mockMvc.perform(post("/redeems/campaignxx")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(redeemRequestDto))
        ).andExpect(status().isNotFound());
    }

    @Test
    public void whenCallApiRedeemByCode_ShouldBeStatusOk() throws Exception {
        redeemRequestDto.setRewardCode("xx");
        mockMvc.perform(post("/redeems/code")
                .header("X-Egg-Signature")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(redeemRequestDto))
        ).andExpect(status().isOk());
    }

    @Test
    public void whenCallApiRedeemByCode_ShouldBeStatusNotFound() throws Exception {
        mockMvc.perform(post("/redeems/code23")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(redeemRequestDto))
        ).andExpect(status().isNotFound());
    }

    @Test
    public void whenCallApiRedeemReversal_ShouldBeStatusOk() throws Exception {
        mockMvc.perform(post("/redeems/reversal")
                .header("X-Egg-Signature")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(redeemRequestDto))
        ).andExpect(status().isOk());
    }

    @Test
    public void whenCallApiRedeemReversal_ShouldBeStatusNotFound() throws Exception {
        mockMvc.perform(post("/redeems/reversalxx")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(redeemRequestDto))
        ).andExpect(status().isNotFound());
    }
}
