package com.eggdigital.rpp.edcapp.controller;

import com.eggdigital.rpp.edcapp.domain.Transaction;
import com.eggdigital.rpp.edcapp.domain.TransactionType;
import org.junit.Before;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;


/**
 * Created by bombay on 11/3/2017 AD.ø
 */
public class Test {

    public static final DecimalFormat decimalFormat = new DecimalFormat("######0.00");

    private List<Transaction> transactions;


    @Before
    public void setUp() throws Exception {
        Transaction transaction = new Transaction();
        transaction.setTransactionType(TransactionType.PAYMENT_CHARGE);
        transaction.setAmount("100");

        Transaction transaction2 = new Transaction();
        transaction2.setTransactionType(TransactionType.PAYMENT_CHARGE);
        transaction2.setAmount("200");

        Transaction transaction3 = new Transaction();
        transaction3.setTransactionType(TransactionType.POINT_EARN);
        transaction3.setAmount("300");
        transactions = Arrays.asList(transaction, transaction2, transaction3);

    }

    @org.junit.Test
    public void name() throws Exception {

        Supplier<List<Transaction>> streamSupplier = () -> transactions;
        streamSupplier.get().forEach(d -> {
            System.out.println(d.getAmount());
        });
        System.out.println(streamSupplier.get().stream().count());
        System.out.println(streamSupplier.get().stream().mapToInt(d -> Integer.valueOf(d.getAmount())).sum());

    }

    @org.junit.Test
    public void test2() throws Exception {
        System.out.println(getAmountDivideOneHundred2Digit("100"));
        String dennis = getAmountDivideOneHundred2Digit("100");
        System.out.println("Float : " + Float.parseFloat(dennis));

    }

    public static String getAmountDivideOneHundred2Digit(String amount) {
        BigDecimal amountDivide = new BigDecimal("100");
        BigDecimal total = new BigDecimal(amount).divide(amountDivide);
        return decimalFormat.format(total);
    }
}
