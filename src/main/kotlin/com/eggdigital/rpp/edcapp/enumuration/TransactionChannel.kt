package com.eggdigital.rpp.edcapp.enumuration

/**
 * Created by Patthara on 07/24/2018 AD.
 *
 */
enum class TransactionChannel {
    EDC_ANDROID, POS, TSM, POS_PARTNER, TM4
}