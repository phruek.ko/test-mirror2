package com.eggdigital.rpp.edcapp.enumuration

/**
 * Created by Patthara on 08/29/2018 AD.
 *
 */
enum class CampaignType {
    CUSTOMER, MERCHANT, ALL
}
