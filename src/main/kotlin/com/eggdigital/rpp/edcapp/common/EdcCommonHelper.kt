package com.eggdigital.rpp.edcapp.common

import java.text.DecimalFormat

class EdcCommonHelper {

    companion object {

        fun convertBahtToSatang(thbString: String): String {
            var newThbString = thbString
            if (newThbString.contains(",")) {
                newThbString = thbString.replace(",", "")
            }
            var decimalFormatter = DecimalFormat("######0.##")
            var satang: String = decimalFormatter.format(newThbString.toFloat())
            return satang
        }

        fun convertSatangStringToBahtTwoDecimalFormat(satang: String): String {
            var decimalFormatter = DecimalFormat("######0.00")
            var baht = decimalFormatter.format(satang.toFloat() / 100)
            return baht
        }

    }


}