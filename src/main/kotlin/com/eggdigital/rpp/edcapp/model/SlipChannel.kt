package com.eggdigital.rpp.edcapp.model

enum class SlipChannel {
    SMS, EMAIL, NONE
}