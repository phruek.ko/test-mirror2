package com.eggdigital.rpp.edcapp.model

import com.fasterxml.jackson.annotation.JsonProperty

data class CustomerSlipChannel(

        @JsonProperty("thai_id")
        var thaiId: String,

        @JsonProperty("channel")
        var channel: SlipChannel,

        @JsonProperty("value")
        var value: String)