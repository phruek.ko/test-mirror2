package com.eggdigital.rpp.edcapp.model

data class CodeMessage(var code: String, var message: String)