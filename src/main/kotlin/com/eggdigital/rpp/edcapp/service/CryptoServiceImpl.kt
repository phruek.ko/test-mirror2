package com.eggdigital.rpp.edcapp.service

import com.eggdigital.rpp.edcapp.config.prop.SecurityProps
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.security.KeyFactory
import java.security.PrivateKey
import java.security.PublicKey
import java.security.spec.PKCS8EncodedKeySpec
import java.security.spec.X509EncodedKeySpec
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit
import java.util.*
import javax.crypto.Cipher

interface CryptoService {

    fun validate(cipherText: String): Boolean
    fun decryptRsa(cipherText: String, privateKey: PrivateKey): String
    fun getPrivateKey(): PrivateKey
    fun getCipherText(): String
}

@Service
class CryptoServiceImpl : CryptoService {

    private val log = LoggerFactory.getLogger(this.javaClass)

    @Value("\${rpp.x-signature.time-diff}")
    private lateinit var diffTimeProperty: String

    @Value("\${rpp.privateKey}")
    private lateinit var privateKey: String

    @Value("\${rpp.publicKey}")
    private lateinit var publicKey: String

    @Autowired
    private lateinit var securityProps: SecurityProps

    override fun validate(cipherText: String): Boolean {
        var privateKey = securityProps.privateKey
        var plainText = decryptRsa(cipherText, privateKey)
        var tmp: List<String> = plainText.split("|")
        var signature = tmp[0]
        var transactionAt: LocalDateTime = LocalDateTime.parse(tmp[1])
        var now = LocalDateTime.now()

        if (signature.equals("eggdigital")) {
            var diffTime = ChronoUnit.MINUTES.between(transactionAt, now)
            if (diffTime >= -diffTimeProperty.toInt() && diffTime <= diffTimeProperty.toInt()) {
                return true
            }
        }
        return false
    }

    override fun decryptRsa(cipherText: String, privateKey: PrivateKey): String {
        var cipherBytes = Base64.getDecoder().decode(cipherText.toByteArray())
        var cipher: Cipher = Cipher.getInstance("RSA")
        cipher.init(Cipher.DECRYPT_MODE, privateKey)
        return String(cipher.doFinal(cipherBytes), Charsets.UTF_8)
    }

    override fun getPrivateKey(): PrivateKey {
        var privateKeyFile = Thread.currentThread().contextClassLoader.getResourceAsStream(privateKey)
        var privateKeyText = privateKeyFile.reader().readText()
        var privateKeyBytes: ByteArray = Base64.getDecoder().decode(privateKeyText.toByteArray())
        var keyFactory: KeyFactory = KeyFactory.getInstance("RSA")
        return keyFactory.generatePrivate(PKCS8EncodedKeySpec(privateKeyBytes))
    }

    override fun getCipherText(): String {
        var now = LocalDateTime.now()
        var dateTimeFormatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME
        var plainText = "eggdigital|${now.format(dateTimeFormatter)}"

        var publicKeyFile = Thread.currentThread().contextClassLoader.getResourceAsStream(publicKey)
        var publicKeyText = publicKeyFile.reader().readText()
        var publicKeyBytes = Base64.getDecoder().decode(publicKeyText.toByteArray())
        var keyFactory: KeyFactory = KeyFactory.getInstance("RSA")
        var publicKey: PublicKey = keyFactory.generatePublic(X509EncodedKeySpec(publicKeyBytes))
        var cipherText = encrypt(plainText, publicKey)
        return cipherText
    }

    fun encrypt(plainText: String, publicKey: PublicKey): String {
        var cipher: Cipher = Cipher.getInstance("RSA")
        cipher.init(Cipher.ENCRYPT_MODE, publicKey)
        var cipherBytes: ByteArray = cipher.doFinal(plainText.toByteArray())
        return Base64.getEncoder().encodeToString(cipherBytes)
    }
}