package com.eggdigital.rpp.edcapp.service

import com.eggdigital.rpp.edcapp.api.ApiKotlinErrorException
import com.eggdigital.rpp.edcapp.api.bs.ApiBsService
import com.eggdigital.rpp.edcapp.api.edccampaign.ApiEdcCampaignService
import com.eggdigital.rpp.edcapp.api.edccampaign.dto.ApiEdcCampaignDetailResponseDto
import com.eggdigital.rpp.edcapp.api.model.ApiStatus
import com.eggdigital.rpp.edcapp.api.point.ApiPointService
import com.eggdigital.rpp.edcapp.domain.PaymentMethod
import com.eggdigital.rpp.edcapp.domain.Status
import com.eggdigital.rpp.edcapp.domain.Transaction
import com.eggdigital.rpp.edcapp.domain.TransactionType
import com.eggdigital.rpp.edcapp.dto.VoidResponseDto
import com.eggdigital.rpp.edcapp.enumuration.CampaignType
import com.eggdigital.rpp.edcapp.exception.CustomThirdPartyExcepton
import com.eggdigital.rpp.edcapp.exception.NotFoundException
import com.eggdigital.rpp.edcapp.merchant.service.MerchantService
import com.eggdigital.rpp.edcapp.repository.MerchantRepository
import com.eggdigital.rpp.edcapp.repository.TransactionRepository
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import java.time.ZoneId

interface VoidService {
    fun voidTransaction(brandId: String, outletId: String, terminalId: String, traceId: String): VoidResponseDto
    fun preVoidTransaction(brandId: String, outletId: String, terminalId: String, traceId: String): VoidResponseDto
    fun voidTransactionByTxRefId(brandId: String, outletId: String, terminalId: String, traceId: String, txRefId: String): VoidResponseDto
    fun preVoidTransactionByTxRefId(brandId: String, outletId: String, terminalId: String, traceId: String, txRefId: String): VoidResponseDto
}

@Service
class VoidServiceImpl(val transactionRepository: TransactionRepository) : VoidService {

    var log = LoggerFactory.getLogger(this.javaClass)

    @Autowired
    lateinit var transactionService: TransactionService

    @Autowired
    lateinit var merchantRepository: MerchantRepository

    @Autowired
    lateinit var transactionBatchService: TransactionBatchService

    @Autowired
    lateinit var apiBsService: ApiBsService

    @Autowired
    lateinit var apiPointService: ApiPointService

    @Autowired
    lateinit var apiEdcCampaignService: ApiEdcCampaignService

    @Autowired
    lateinit var merchantService: MerchantService

    override fun voidTransactionByTxRefId(brandId: String, outletId: String, terminalId: String, traceId: String, txRefId: String): VoidResponseDto {
        //todo implement after migration data and implement registry on prem
        //val merchant = merchantService.findByMerchantId(brandId)
        val merchant = merchantRepository.findByMerchantId(brandId)
        if (!merchant.isPresent) {
            throw CustomThirdPartyExcepton(ApiStatus("4102", "Not found data for Merchant"))
        }

        val transactions = transactionRepository
                .findByBrandIdAndOutletIdAndTerminalIdAndTraceIdAndRefIdAndSettleStatusIsNotAndStatus(brandId, outletId, terminalId, traceId, txRefId, Status.SETTLE, Status.SUCCESS)

        if (transactions.size == 0) {
            throw CustomThirdPartyExcepton(ApiStatus("404", "not found"))
        }

        if (isTransactionDuplicated(transactions)) {
            throw CustomThirdPartyExcepton(ApiStatus("409", "duplicated."))
        }

        if (transactions
                        .filter { t ->
                            t.transactionType == TransactionType.PAYMENT_CHARGE ||
                                    t.transactionType == TransactionType.REDEEM_BENEFIT ||
                                    t.transactionType == TransactionType.POINT_EARN
                        }
                        .isEmpty()) {
            throw CustomThirdPartyExcepton(ApiStatus("404", "not found"))
        }

        val transaction = transactions
                .filter { t ->
                    t.transactionType == TransactionType.PAYMENT_CHARGE ||
                            t.transactionType == TransactionType.REDEEM_BENEFIT ||
                            t.transactionType == TransactionType.POINT_EARN
                }.first()


        var newTransaction = Transaction()
        newTransaction.brandId = transaction.brandId
        newTransaction.outletId = transaction.outletId
        newTransaction.terminalId = transaction.terminalId
        newTransaction.refId = transaction.refId
        newTransaction.status = Status.PENDING
        newTransaction.amount = transaction.amount
        newTransaction.paymentCode = transaction.paymentCode
        newTransaction.paymentMethod = transaction.paymentMethod
        newTransaction.currency = transaction.currency
        newTransaction.batchId = transaction.batchId
        newTransaction.traceId = transaction.traceId

        newTransaction.point = transaction.point
        newTransaction.customerRef = transaction.customerRef
        newTransaction.campaignCode = transaction.campaignCode
        newTransaction.couponCode = transaction.campaignCode
        newTransaction.accountType = transaction.accountType
        newTransaction.transactionChannel = transaction.transactionChannel
        newTransaction.campaignType = transaction.campaignType ?: CampaignType.CUSTOMER

        transactionRepository.save(newTransaction)

        var createdAt = LocalDateTime.ofInstant(transaction.createDate.toInstant(), ZoneId.systemDefault())

        val voidResponseDto = VoidResponseDto(
                transaction.brandId,
                transaction.outletId,
                transaction.terminalId,
                transaction.traceId,
                transaction.batchId,
                transaction.refId,
                createdAt,
                if (merchant.isPresent) merchant.get().trueyouId else "",
                transaction.amount ?: "",
                transaction.paymentCode ?: "",
                transaction.currency?.name ?: "",
                transaction.paymentMethod?.name ?: ""
        )

        var transactionType = transaction.transactionType

        when (transactionType) {
            TransactionType.PAYMENT_CHARGE -> {
                var paymentMethod = transaction.paymentMethod

                when (paymentMethod) {
                    PaymentMethod.ALIPAY, PaymentMethod.WALLET -> {
                        try {
                            var cancelResponse = apiBsService.paymentCancel(brandId, outletId, terminalId, transaction.refId)
                            newTransaction.status = Status.SUCCESS
                            newTransaction.transactionType = TransactionType.PAYMENT_CANCEL
                            newTransaction.statusCode = cancelResponse.status!!.code.toString()
                            newTransaction.statusDescription = cancelResponse.status!!.message
                            transactionRepository.save(newTransaction)
                            voidResponseDto.transactionType = TransactionType.PAYMENT_CANCEL.name
                        } catch (ex: ApiKotlinErrorException) {
                            newTransaction.status = Status.FAIL
                            newTransaction.transactionType = TransactionType.PAYMENT_CANCEL
                            newTransaction.statusCode = ex.status!!.code
                            newTransaction.statusDescription = ex.status!!.message
                            transactionRepository.save(newTransaction)
                            voidResponseDto.transactionType = TransactionType.PAYMENT_CANCEL.name
                            throw ApiKotlinErrorException(ex.message, ex.errors)
                        }
                    }

                    else -> throw NotFoundException("Not found paymentMethod")
                }
            }
            TransactionType.REDEEM_BENEFIT -> {
                try {

                    var campaignDetailResponseDto: ApiEdcCampaignDetailResponseDto = apiEdcCampaignService.getCampaignDetail(transaction.campaignCode, transaction.terminalId, CampaignType.ALL)
                    var campaignDetails = campaignDetailResponseDto.data as List<ApiEdcCampaignDetailResponseDto.Data>
                    var campaignName = campaignDetails.first().campaignName
                    voidResponseDto.campaignName = campaignName

                    var redeemCancelResponse = apiBsService.redeemCancel(brandId, outletId, terminalId, transaction.refId)
                    newTransaction.status = Status.SUCCESS
                    newTransaction.transactionType = TransactionType.REDEEM_ROLLBACK
                    newTransaction.statusCode = redeemCancelResponse.status!!.code.toString()
                    newTransaction.statusDescription = redeemCancelResponse.status!!.message
                    transactionRepository.save(newTransaction)
                    voidResponseDto.transactionType = TransactionType.REDEEM_ROLLBACK.name
                    voidResponseDto.accType = transaction.accountType.name
                    voidResponseDto.accValue = transaction.customerRef
                } catch (ex: ApiKotlinErrorException) {
                    newTransaction.status = Status.FAIL
                    newTransaction.transactionType = TransactionType.REDEEM_ROLLBACK
                    newTransaction.statusCode = ex.status!!.code
                    newTransaction.statusDescription = ex.status!!.message
                    transactionRepository.save(newTransaction)
                    voidResponseDto.transactionType = TransactionType.REDEEM_ROLLBACK.name
                    voidResponseDto.accType = transaction.accountType.name
                    voidResponseDto.accValue = transaction.customerRef
                    throw ex
                }
            }
            TransactionType.POINT_EARN -> {
                try {
                    var reversalResponse = apiPointService.cancelPoint(brandId, outletId, terminalId, transaction.refId)
                    newTransaction.status = Status.SUCCESS
                    newTransaction.transactionType = TransactionType.POINT_CANCEL
                    newTransaction.statusCode = reversalResponse.status!!.code.toString()
                    newTransaction.statusDescription = reversalResponse.status!!.message
                    transactionRepository.save(newTransaction)
                    voidResponseDto.point = transaction.point.toString()
                    voidResponseDto.transactionType = TransactionType.POINT_CANCEL.name
                    voidResponseDto.accType = transaction.accountType.name
                    voidResponseDto.accValue = transaction.customerRef
                } catch (ex: ApiKotlinErrorException) {
                    newTransaction.status = Status.FAIL
                    newTransaction.transactionType = TransactionType.POINT_CANCEL
                    newTransaction.statusCode = ex.status!!.code
                    newTransaction.statusDescription = ex.status!!.message
                    transactionRepository.save(newTransaction)
                    voidResponseDto.point = transaction.point.toString()
                    voidResponseDto.transactionType = TransactionType.POINT_CANCEL.name
                    voidResponseDto.accType = transaction.accountType.name
                    voidResponseDto.accValue = transaction.customerRef
                    throw ex
                }
            }
            else -> throw NotFoundException("Not found transactionType")
        }

        return voidResponseDto
    }

    override fun preVoidTransactionByTxRefId(brandId: String, outletId: String, terminalId: String, traceId: String, txRefId: String): VoidResponseDto {
        //todo implement after migration data and implement registry on prem
        //val merchant = merchantService.findByMerchantId(brandId)
        val merchant = merchantRepository.findByMerchantId(brandId)
        if (!merchant.isPresent) {
            throw CustomThirdPartyExcepton(ApiStatus("4102", "Not found data for Merchant"))
        }

        val transactions = transactionRepository
                .findByBrandIdAndOutletIdAndTerminalIdAndTraceIdAndRefIdAndSettleStatusIsNotAndStatus(brandId, outletId, terminalId, traceId, txRefId, Status.SETTLE, Status.SUCCESS)

        if (transactions.size == 0) {
            throw CustomThirdPartyExcepton(ApiStatus("404", "not found"))
        }

        if (isTransactionDuplicated(transactions)) {
            throw CustomThirdPartyExcepton(ApiStatus("409", "duplicated."))
        }

        val transaction = transactions
                .filter { t ->
                    t.transactionType == TransactionType.PAYMENT_CHARGE ||
                            t.transactionType == TransactionType.REDEEM_BENEFIT ||
                            t.transactionType == TransactionType.POINT_EARN
                }.first()

        var createdAt = LocalDateTime.ofInstant(transaction.createDate.toInstant(), ZoneId.systemDefault())

        val voidResponseDto = VoidResponseDto(
                transaction.brandId,
                transaction.outletId,
                transaction.terminalId,
                transaction.traceId,
                transaction.batchId,
                transaction.refId,
                createdAt,
                if (merchant.isPresent) merchant.get().trueyouId else "",
                transaction.amount ?: "",
                transaction.paymentCode ?: "",
                transaction.currency?.name ?: "",
                transaction.paymentMethod?.name ?: "",
                transaction.transactionType?.name ?: "",
                transaction?.point?.toString() ?: "",
                transaction.accountType?.name ?: "",
                transaction.customerRef ?: ""

        )

        if (transaction.transactionType == TransactionType.REDEEM_BENEFIT) {

            var campaignDetailResponseDto: ApiEdcCampaignDetailResponseDto = apiEdcCampaignService.getCampaignDetail(transaction.campaignCode, transaction.terminalId, CampaignType.ALL)

            var campaignDetails = campaignDetailResponseDto.data as List<ApiEdcCampaignDetailResponseDto.Data>
            var campaignName = campaignDetails.first().campaignName

            voidResponseDto.campaignName = campaignName
        }

        return voidResponseDto
    }


    override fun voidTransaction(brandId: String, outletId: String, terminalId: String, traceId: String): VoidResponseDto {
        //todo implement after migration data and implement registry on prem
        //val merchant = merchantService.findByMerchantId(brandId)
        val merchant = merchantRepository.findByMerchantId(brandId)
        if (!merchant.isPresent) {
            throw CustomThirdPartyExcepton(ApiStatus("4102", "Not found data for Merchant"))
        }

        val transactions = transactionRepository
                .findByBrandIdAndOutletIdAndTerminalIdAndTraceIdAndSettleStatusIsNotAndStatus(brandId, outletId, terminalId, traceId, Status.SETTLE, Status.SUCCESS)

        if (transactions.size == 0) {
            throw CustomThirdPartyExcepton(ApiStatus("404", "not found"))
        }

        if (isTransactionDuplicated(transactions)) {
            throw CustomThirdPartyExcepton(ApiStatus("409", "duplicated."))
        }

        if (transactions
                        .filter { t ->
                            t.transactionType == TransactionType.PAYMENT_CHARGE ||
                                    t.transactionType == TransactionType.REDEEM_BENEFIT ||
                                    t.transactionType == TransactionType.POINT_EARN
                        }
                        .isEmpty()) {
            throw CustomThirdPartyExcepton(ApiStatus("404", "not found"))
        }

        val transaction = transactions
                .filter { t ->
                    t.transactionType == TransactionType.PAYMENT_CHARGE ||
                            t.transactionType == TransactionType.REDEEM_BENEFIT ||
                            t.transactionType == TransactionType.POINT_EARN
                }.first()


        var newTransaction = Transaction()
        newTransaction.brandId = transaction.brandId
        newTransaction.outletId = transaction.outletId
        newTransaction.terminalId = transaction.terminalId
        newTransaction.refId = transaction.refId
        newTransaction.status = Status.PENDING
        newTransaction.amount = transaction.amount
        newTransaction.paymentCode = transaction.paymentCode
        newTransaction.paymentMethod = transaction.paymentMethod
        newTransaction.currency = transaction.currency
        newTransaction.batchId = transaction.batchId
        newTransaction.traceId = transaction.traceId

        newTransaction.point = transaction.point
        newTransaction.customerRef = transaction.customerRef
        newTransaction.campaignCode = transaction.campaignCode
        newTransaction.couponCode = transaction.campaignCode
        newTransaction.accountType = transaction.accountType
        newTransaction.transactionChannel = transaction.transactionChannel
        newTransaction.campaignType = transaction.campaignType ?: CampaignType.CUSTOMER


        transactionRepository.save(newTransaction)

        var createdAt = LocalDateTime.ofInstant(transaction.createDate.toInstant(), ZoneId.systemDefault())

        val voidResponseDto = VoidResponseDto(
                transaction.brandId,
                transaction.outletId,
                transaction.terminalId,
                transaction.traceId,
                transaction.batchId,
                transaction.refId,
                createdAt,
                if (merchant.isPresent) merchant.get().trueyouId else "",
                transaction.amount ?: "",
                transaction.paymentCode ?: "",
                transaction.currency?.name ?: "",
                transaction.paymentMethod?.name ?: ""
        )

        var transactionType = transaction.transactionType

        when (transactionType) {
            TransactionType.PAYMENT_CHARGE -> {
                var paymentMethod = transaction.paymentMethod

                when (paymentMethod) {
                    PaymentMethod.ALIPAY, PaymentMethod.WALLET -> {
                        try {
                            var cancelResponse = apiBsService.paymentCancel(brandId, outletId, terminalId, transaction.refId)
                            newTransaction.status = Status.SUCCESS
                            newTransaction.transactionType = TransactionType.PAYMENT_CANCEL
                            newTransaction.statusCode = cancelResponse.status!!.code.toString()
                            newTransaction.statusDescription = cancelResponse.status!!.message
                            transactionRepository.save(newTransaction)
                            voidResponseDto.transactionType = TransactionType.PAYMENT_CANCEL.name
                        } catch (ex: ApiKotlinErrorException) {
                            newTransaction.status = Status.FAIL
                            newTransaction.transactionType = TransactionType.PAYMENT_CANCEL
                            newTransaction.statusCode = ex.status!!.code
                            newTransaction.statusDescription = ex.status!!.message
                            transactionRepository.save(newTransaction)
                            voidResponseDto.transactionType = TransactionType.PAYMENT_CANCEL.name
                            throw ApiKotlinErrorException(ex.message, ex.errors)
                        }
                    }

                    else -> throw NotFoundException("Not found paymentMethod")
                }
            }
            TransactionType.REDEEM_BENEFIT -> {
                try {

                    var campaignDetailResponseDto: ApiEdcCampaignDetailResponseDto = apiEdcCampaignService.getCampaignDetail(transaction.campaignCode, transaction.terminalId, CampaignType.ALL)
                    var campaignDetails = campaignDetailResponseDto.data as List<ApiEdcCampaignDetailResponseDto.Data>
                    var campaignName = campaignDetails.first().campaignName
                    voidResponseDto.campaignName = campaignName

                    var redeemCancelResponse = apiBsService.redeemCancel(brandId, outletId, terminalId, transaction.refId)
                    newTransaction.status = Status.SUCCESS
                    newTransaction.transactionType = TransactionType.REDEEM_ROLLBACK
                    newTransaction.statusCode = redeemCancelResponse.status!!.code.toString()
                    newTransaction.statusDescription = redeemCancelResponse.status!!.message
                    transactionRepository.save(newTransaction)
                    voidResponseDto.transactionType = TransactionType.REDEEM_ROLLBACK.name
                    voidResponseDto.accType = transaction.accountType.name
                    voidResponseDto.accValue = transaction.customerRef
                } catch (ex: ApiKotlinErrorException) {
                    newTransaction.status = Status.FAIL
                    newTransaction.transactionType = TransactionType.REDEEM_ROLLBACK
                    newTransaction.statusCode = ex.status!!.code
                    newTransaction.statusDescription = ex.status!!.message
                    transactionRepository.save(newTransaction)
                    voidResponseDto.transactionType = TransactionType.REDEEM_ROLLBACK.name
                    voidResponseDto.accType = transaction.accountType.name
                    voidResponseDto.accValue = transaction.customerRef
                    throw ex
                }
            }
            TransactionType.POINT_EARN -> {
                try {
                    var reversalResponse = apiPointService.cancelPoint(brandId, outletId, terminalId, transaction.refId)
                    newTransaction.status = Status.SUCCESS
                    newTransaction.transactionType = TransactionType.POINT_CANCEL
                    newTransaction.statusCode = reversalResponse.status!!.code.toString()
                    newTransaction.statusDescription = reversalResponse.status!!.message
                    transactionRepository.save(newTransaction)
                    voidResponseDto.point = transaction.point.toString()
                    voidResponseDto.transactionType = TransactionType.POINT_CANCEL.name
                    voidResponseDto.accType = transaction.accountType.name
                    voidResponseDto.accValue = transaction.customerRef
                } catch (ex: ApiKotlinErrorException) {
                    newTransaction.status = Status.FAIL
                    newTransaction.transactionType = TransactionType.POINT_CANCEL
                    newTransaction.statusCode = ex.status!!.code
                    newTransaction.statusDescription = ex.status!!.message
                    transactionRepository.save(newTransaction)
                    voidResponseDto.point = transaction.point.toString()
                    voidResponseDto.transactionType = TransactionType.POINT_CANCEL.name
                    voidResponseDto.accType = transaction.accountType.name
                    voidResponseDto.accValue = transaction.customerRef
                    throw ex
                }
            }
            else -> throw NotFoundException("Not found transactionType")
        }

        return voidResponseDto
    }


    override fun preVoidTransaction(brandId: String, outletId: String, terminalId: String, traceId: String): VoidResponseDto {
        //todo implement after migration data and implement registry on prem
        //val merchant = merchantService.findByMerchantId(brandId)
        val merchant = merchantRepository.findByMerchantId(brandId)
        if (!merchant.isPresent) {
            throw CustomThirdPartyExcepton(ApiStatus("4102", "Not found data for Merchant"))
        }

        val transactions = transactionRepository
                .findByBrandIdAndOutletIdAndTerminalIdAndTraceIdAndSettleStatusIsNotAndStatus(brandId, outletId, terminalId, traceId, Status.SETTLE, Status.SUCCESS)

        if (transactions.size == 0) {
            throw CustomThirdPartyExcepton(ApiStatus("404", "not found"))
        }

        if (isTransactionDuplicated(transactions)) {
            throw CustomThirdPartyExcepton(ApiStatus("409", "duplicated."))
        }

        val transaction = transactions
                .filter { t ->
                    t.transactionType == TransactionType.PAYMENT_CHARGE ||
                            t.transactionType == TransactionType.REDEEM_BENEFIT ||
                            t.transactionType == TransactionType.POINT_EARN
                }.first()

        var createdAt = LocalDateTime.ofInstant(transaction.createDate.toInstant(), ZoneId.systemDefault())

        val voidResponseDto = VoidResponseDto(
                transaction.brandId,
                transaction.outletId,
                transaction.terminalId,
                transaction.traceId,
                transaction.batchId,
                transaction.refId,
                createdAt,
                if (merchant.isPresent) merchant.get().trueyouId else "",
                transaction.amount ?: "",
                transaction.paymentCode ?: "",
                transaction.currency?.name ?: "",
                transaction.paymentMethod?.name ?: "",
                transaction.transactionType?.name ?: "",
                transaction?.point?.toString() ?: "",
                transaction.accountType?.name ?: "",
                transaction.customerRef ?: ""

        )

        if (transaction.transactionType == TransactionType.REDEEM_BENEFIT) {

            var campaignDetailResponseDto: ApiEdcCampaignDetailResponseDto = apiEdcCampaignService.getCampaignDetail(transaction.campaignCode, transaction.terminalId, CampaignType.ALL)

            var campaignDetails = campaignDetailResponseDto.data as List<ApiEdcCampaignDetailResponseDto.Data>
            var campaignName = campaignDetails.first().campaignName

            voidResponseDto.campaignName = campaignName
        }

        return voidResponseDto
    }

    fun isTransactionDuplicated(transactions: List<Transaction>): Boolean {
        var isDone = false
        for (t in transactions) {
            when (t.transactionType) {
                TransactionType.PAYMENT_CHARGE -> {
                    isDone = transactionRepository.findByBrandIdAndOutletIdAndTerminalIdAndTraceIdAndSettleStatusIsNotAndStatusAndTransactionType(t.brandId,
                            t.outletId,
                            t.terminalId,
                            t.traceId,
                            Status.SETTLE,
                            Status.SUCCESS,
                            TransactionType.PAYMENT_CANCEL).isPresent
                }
                TransactionType.REDEEM_BENEFIT -> {
                    isDone = transactionRepository.findByBrandIdAndOutletIdAndTerminalIdAndTraceIdAndSettleStatusIsNotAndStatusAndTransactionType(t.brandId,
                            t.outletId,
                            t.terminalId,
                            t.traceId,
                            Status.SETTLE,
                            Status.SUCCESS,
                            TransactionType.REDEEM_ROLLBACK).isPresent

                }
                TransactionType.POINT_EARN -> {
                    isDone = transactionRepository.findByBrandIdAndOutletIdAndTerminalIdAndTraceIdAndSettleStatusIsNotAndStatusAndTransactionType(t.brandId,
                            t.outletId,
                            t.terminalId,
                            t.traceId,
                            Status.SETTLE,
                            Status.SUCCESS,
                            TransactionType.POINT_CANCEL).isPresent
                }
            }
            if (isDone) {
                break
            }
        }
        return isDone
    }

}
