package com.eggdigital.rpp.edcapp.service

import com.eggdigital.rpp.edcapp.api.user.ApiUserService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

interface AuthorizationService {

    fun validate(credential: String): Boolean
}

@Service
class AuthorizationServiceImpl : AuthorizationService {

    private val log = LoggerFactory.getLogger(this.javaClass)

    @Autowired
    lateinit var apiUserService: ApiUserService

    override fun validate(credential: String): Boolean {

        var isValid = false

        try {
            var textBase64 = credential.split(" ").get(1)
            var textDecoded = Base64.getDecoder()
                    .decode(textBase64)
                    .toString(Charsets.UTF_8)
            var username = textDecoded.split(":").get(0)
            var password = textDecoded.split(":").get(1)

            try {
                apiUserService.login(username, password, "o2o")
                isValid = true
            } catch (ex: Exception) {
                isValid = false
            }

        } catch (ex: Exception) {
            log.error("{}", ex)
        }

        return isValid
    }

}