package com.eggdigital.rpp.edcapp.service

import com.eggdigital.rpp.edcapp.domain.PaymentMethod
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.MessageSource
import org.springframework.context.i18n.LocaleContextHolder
import org.springframework.mail.SimpleMailMessage
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.stereotype.Service

interface EmailService {

    fun sendMessage(to: String, subject: String, msg: String)
}

@Service
class EmailServiceImpl : EmailService {

    @Autowired
    lateinit var emailSender: JavaMailSender

    @Autowired
    lateinit var messageSource: MessageSource

    fun getTrueyouSubject(): String {
        return messageSource.getMessage("email.trueyou.subject", null, LocaleContextHolder.getLocale())
    }


    fun sendRedeemByCampaign(to: String, campaignName: String, traceId: String) {

        var text = messageSource.getMessage("email.trueyou.redeem_by_campaign", null, LocaleContextHolder.getLocale())
        text = text.replace("{{campaign_name}}", campaignName)
                .replace("{{trace_id}}", traceId)

        sendMessage(to, getTrueyouSubject(), text)
    }

    fun sendRedeemByCode(to: String, campaignName: String, rewardCode: String, traceId: String) {

        var text = messageSource.getMessage("email.trueyou.redeem_by_code", null, LocaleContextHolder.getLocale())
        text = text.replace("{{campaign_name}}", campaignName)
                .replace("{{reward_code}}", rewardCode)
                .replace("{{trace_id}}", traceId)

        sendMessage(to, getTrueyouSubject(), text)
    }

    fun sendEarnPoint(to: String, amount: String, point: String) {
        var text = messageSource.getMessage("email.trueyou.earn", null, LocaleContextHolder.getLocale())
        text = text.replace("{{amount}}", amount)
                .replace("{{point}}", point)

        sendMessage(to, getTrueyouSubject(), text)
    }

    fun sendPayment(to: String, paymentMethod: PaymentMethod, amount: String) {

        var text = messageSource.getMessage("email.trueyou.payment", null, LocaleContextHolder.getLocale())
        text = text.replace("{{paymentMethod}}", paymentMethod.name)
                .replace("{{amount}}", amount)

        sendMessage(to, getTrueyouSubject(), text)
    }

    fun sendVoidRedeemByCampaign(to: String, campaignName: String) {

        var text = messageSource.getMessage("email.trueyou.void.redeem_by_campaign", null, LocaleContextHolder.getLocale())
        text = text.replace("{{campaign_name}}", campaignName)

        sendMessage(to, getTrueyouSubject(), text)
    }

    fun sendVoidPoint(to: String, amount: String, point: String) {

        var text = messageSource.getMessage("email.trueyou.void.point", null, LocaleContextHolder.getLocale())
        text = text.replace("{{amount}}", amount)
                .replace("{{point}}", point)

        sendMessage(to, getTrueyouSubject(), text)
    }

    fun sendVoidPayment(to: String, amount: String) {

        var text = messageSource.getMessage("email.trueyou.void.payment", null, LocaleContextHolder.getLocale())
        text = text.replace("{{amount}}", amount)

        sendMessage(to, getTrueyouSubject(), text)
    }

    override fun sendMessage(to: String, subject: String, msg: String) {

        var message = SimpleMailMessage()
        message.to = arrayOf(to)
        message.subject = subject
        message.text = msg

        emailSender.send(message)
    }
}