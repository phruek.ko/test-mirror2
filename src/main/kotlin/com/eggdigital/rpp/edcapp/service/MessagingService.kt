package com.eggdigital.rpp.edcapp.service

import com.eggdigital.rpp.edcapp.api.edccampaign.ApiEdcCampaignService
import com.eggdigital.rpp.edcapp.api.edccampaign.dto.ApiEdcCampaignDetailResponseDto
import com.eggdigital.rpp.edcapp.common.EdcCommonHelper
import com.eggdigital.rpp.edcapp.domain.Status
import com.eggdigital.rpp.edcapp.domain.TransactionType
import com.eggdigital.rpp.edcapp.dto.MessagingRequestDto
import com.eggdigital.rpp.edcapp.exception.NotFoundException
import com.eggdigital.rpp.edcapp.model.SlipChannel
import com.eggdigital.rpp.edcapp.ms.RppBsApi
import com.eggdigital.rpp.edcapp.repository.TransactionRepository
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.math.BigDecimal

interface MessagingService {

    fun sendByTraceId(request: MessagingRequestDto)
}

@Service
class MessagingServiceImpl : MessagingService {

    var log = LoggerFactory.getLogger(this.javaClass)

    @Autowired
    lateinit var emailService: EmailServiceImpl

    @Autowired
    lateinit var smsService: SMSService

    @Autowired
    lateinit var apiBsService: RppBsApi

    @Autowired
    lateinit var transactionRepository: TransactionRepository

    @Autowired
    lateinit var apiEdcCampaignService: ApiEdcCampaignService

    override fun sendByTraceId(request: MessagingRequestDto) {

        var to = request.value

        var transactionOrm = transactionRepository.findByBrandIdAndOutletIdAndTerminalIdAndTraceIdAndSettleStatusIsNotAndStatus(
                request.brandId, request.outletId, request.terminalId, request.traceId, Status.SETTLE, Status.SUCCESS)

        if (transactionOrm.size == 0) {
            throw NotFoundException("Not found traceId : " + request.traceId)
        }

        var transaction = transactionOrm[0]

        if (transactionOrm.size == 2) {

            transaction = transactionOrm[0]

            var createdTxFirst = transactionOrm[0].createDate
            var createdTxSecond = transactionOrm[1].createDate
            if (createdTxSecond.after(createdTxFirst)) {
                transaction = transactionOrm[1]
            }
        }

        try {

            when (request.channel) {
                SlipChannel.SMS -> {

                    when (transaction.transactionType) {

                        TransactionType.REDEEM_BENEFIT -> {

                            var campaignDetailResponseDto: ApiEdcCampaignDetailResponseDto = apiEdcCampaignService.getCampaignDetail(transaction.campaignCode, transaction.terminalId)
                            var campaignDetails = campaignDetailResponseDto.data as List<ApiEdcCampaignDetailResponseDto.Data>
                            var campaignName = campaignDetails.first().campaignName

                            smsService.sendRedeemByCampaign(to, campaignName, request.traceId)
                        }

                        TransactionType.REDEEM_MARKUSE -> {

                            var campaignDetailResponseDto: ApiEdcCampaignDetailResponseDto = apiEdcCampaignService.getCampaignDetail(transaction.campaignCode, transaction.terminalId)
                            var campaignDetails = campaignDetailResponseDto.data as List<ApiEdcCampaignDetailResponseDto.Data>
                            var campaignName = campaignDetails.first().campaignName

                            smsService.sendRedeemByCode(to, campaignName, transaction.couponCode, request.traceId)
                        }

                        TransactionType.POINT_EARN -> {

                            var amount = BigDecimal(EdcCommonHelper.convertSatangStringToBahtTwoDecimalFormat(transaction.amount))
                            var point = BigDecimal(transaction.point)

                            smsService.sendEarnPoint(to, amount, point)
                        }


                        TransactionType.REDEEM_ROLLBACK -> {

                            var campaignDetailResponseDto: ApiEdcCampaignDetailResponseDto = apiEdcCampaignService.getCampaignDetail(transaction.campaignCode, transaction.terminalId)
                            var campaignDetails = campaignDetailResponseDto.data as List<ApiEdcCampaignDetailResponseDto.Data>
                            var campaignName = campaignDetails.first().campaignName

                            smsService.sendVoidRedeemByCampaign(to, campaignName)
                        }

                        TransactionType.PAYMENT_CANCEL -> {

                            var baht = EdcCommonHelper.convertSatangStringToBahtTwoDecimalFormat(transaction.amount)

                            smsService.sendVoidPayment(to, baht)
                        }

                        TransactionType.POINT_CANCEL -> {

                            var amount = EdcCommonHelper.convertSatangStringToBahtTwoDecimalFormat(transaction.amount)
                            smsService.sendVoidEarnPoint(to, amount, transaction.point.toString())

                        }

                        else -> {

                        }

                    }
                }

                SlipChannel.EMAIL -> {

                    when (transaction.transactionType) {

                        TransactionType.REDEEM_BENEFIT -> {
                            var campaignDetailResponseDto: ApiEdcCampaignDetailResponseDto = apiEdcCampaignService.getCampaignDetail(transaction.campaignCode, transaction.terminalId)
                            var campaignDetails = campaignDetailResponseDto.data as List<ApiEdcCampaignDetailResponseDto.Data>
                            var campaignName = campaignDetails.first().campaignName

                            emailService.sendRedeemByCampaign(to, campaignName, request.traceId)
                        }

                        TransactionType.REDEEM_MARKUSE -> {

                            var campaignDetailResponseDto: ApiEdcCampaignDetailResponseDto = apiEdcCampaignService.getCampaignDetail(transaction.campaignCode, transaction.terminalId)
                            var campaignDetails = campaignDetailResponseDto.data as List<ApiEdcCampaignDetailResponseDto.Data>
                            var campaignName = campaignDetails.first().campaignName

                            emailService.sendRedeemByCode(to, campaignName, transaction.couponCode, request.traceId)

                        }

                        TransactionType.POINT_EARN -> {

                            var amount = EdcCommonHelper.convertSatangStringToBahtTwoDecimalFormat(transaction.amount)
                            var point = transaction.point.toString()

                            emailService.sendEarnPoint(to, amount, point)
                        }

                        TransactionType.REDEEM_ROLLBACK -> {

                            var campaignDetailResponseDto: ApiEdcCampaignDetailResponseDto = apiEdcCampaignService.getCampaignDetail(transaction.campaignCode, transaction.terminalId)
                            var campaignDetails = campaignDetailResponseDto.data as List<ApiEdcCampaignDetailResponseDto.Data>
                            var campaignName = campaignDetails.first().campaignName

                            emailService.sendVoidRedeemByCampaign(to, campaignName)
                        }

                        TransactionType.PAYMENT_CANCEL -> {

                            var baht = EdcCommonHelper.convertSatangStringToBahtTwoDecimalFormat(transaction.amount)

                            emailService.sendVoidPayment(to, baht)
                        }

                        TransactionType.POINT_CANCEL -> {

                            var amount = EdcCommonHelper.convertSatangStringToBahtTwoDecimalFormat(transaction.amount)
                            emailService.sendVoidPoint(to, amount, transaction.point.toString())

                        }

                        else -> {

                        }
                    }
                }
                else -> {

                }
            }


        } catch (ex: Exception) {
            log.error("{}", ex)
            throw Exception(ex)
        }

    }
}