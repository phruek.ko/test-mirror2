package com.eggdigital.rpp.edcapp.service

import com.eggdigital.rpp.edcapp.api.ApiKotlinErrorException
import com.eggdigital.rpp.edcapp.api.bs.ApiBsService
import com.eggdigital.rpp.edcapp.api.bs.dto.ApiPaymentChargeResponseDto
import com.eggdigital.rpp.edcapp.api.model.ApiStatus
import com.eggdigital.rpp.edcapp.domain.*
import com.eggdigital.rpp.edcapp.dto.PaymentChargeRequestDto
import com.eggdigital.rpp.edcapp.dto.PaymentChargeResponseDto
import com.eggdigital.rpp.edcapp.enumuration.TransactionChannel
import com.eggdigital.rpp.edcapp.exception.CustomThirdPartyExcepton
import com.eggdigital.rpp.edcapp.merchant.service.MerchantService
import com.eggdigital.rpp.edcapp.repository.MerchantRepository
import com.eggdigital.rpp.edcapp.repository.TransactionRepository
import com.google.common.base.Strings
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import java.time.ZoneId

interface PaymentService {
    fun charge(request: PaymentChargeRequestDto): PaymentChargeResponseDto
}

@Service
class PaymentServiceImpl : PaymentService {

    var log = LoggerFactory.getLogger(this.javaClass)

    @Autowired
    lateinit var transactionBatchService: TransactionBatchService

    @Autowired
    lateinit var transactionRepository: TransactionRepository

    @Autowired
    lateinit var merchantRepository: MerchantRepository

    @Autowired
    lateinit var apiBsService: ApiBsService

    @Autowired
    lateinit var merchantService: MerchantService

    @Throws(ApiKotlinErrorException::class)
    override fun charge(request: PaymentChargeRequestDto): PaymentChargeResponseDto {

        //val merchant = merchantService.findByMerchantId(request.brandId)
        val merchant = merchantRepository.findByMerchantId(request.brandId)
        if (!merchant.isPresent) {
            throw CustomThirdPartyExcepton(ApiStatus("4102", "Not found data for Merchant"))
        }

        var response: PaymentChargeResponseDto

        var transaction = Transaction()
        transaction.status = Status.PENDING
        transaction.brandId = request.brandId
        transaction.outletId = request.outletId
        transaction.terminalId = request.terminalId
        transaction.amount = request.amount
        transaction.currency = Currency.THB
        transaction.paymentCode = request.paymentCode
        transaction.paymentMethod = PaymentMethod.valueOf(request.paymentMethod!!)
        transaction.transactionType = TransactionType.PAYMENT_CHARGE
        if (!Strings.isNullOrEmpty(request.transactionChannel)) {
            transaction.transactionChannel = TransactionChannel.valueOf(request.transactionChannel!!)
        }

        var transactionBatch: TransactionBatch = transactionBatchService.genTransactionBatch(request.brandId, request.outletId, request.terminalId)
        transaction.batchId = transactionBatch.batchId
        transaction.traceId = transactionBatch.traceId
        transaction.refId = transactionBatch.txRefId

        transactionRepository.save(transaction)

        try {
            var chargeResponse: ApiPaymentChargeResponseDto = apiBsService.paymentCharge(request.brandId,
                    request.outletId,
                    request.terminalId,
                    transaction.refId,
                    request.amount,
                    request.currency!!.toLowerCase(),
                    request.paymentCode,
                    request.paymentMethod,
                    request.description!!)

            var createdAt = LocalDateTime.ofInstant(transaction.createDate.toInstant(), ZoneId.systemDefault())

            response = PaymentChargeResponseDto(
                    transaction.brandId,
                    transaction.outletId,
                    transaction.terminalId,
                    transaction.traceId,
                    transaction.batchId,
                    transaction.refId,
                    createdAt,
                    if (merchant.isPresent) merchant.get().trueyouId else "",
                    transaction.amount,
                    transaction.paymentCode,
                    transaction.paymentMethod.name,
                    transaction.currency.name)

            transaction.status = Status.SUCCESS
            transaction.statusCode = chargeResponse.status!!.code.toString()
            transaction.statusDescription = chargeResponse.status!!.message
            transactionRepository.save(transaction)

        } catch (ex: ApiKotlinErrorException) {
            log.error("{}", ex)
            transaction.status = Status.FAIL
            transaction.statusCode = ex.status!!.code
            transaction.statusDescription = ex.status!!.message
            transactionRepository.save(transaction)
            throw ex
        }

        return response
    }
}