package com.eggdigital.rpp.edcapp.service

import com.eggdigital.rpp.edcapp.api.ApiKotlinErrorException
import com.eggdigital.rpp.edcapp.api.bs.ApiBsService
import com.eggdigital.rpp.edcapp.api.bs.dto.ApiBsCustomerInfoResponseDto
import com.eggdigital.rpp.edcapp.api.model.ApiStatus
import com.eggdigital.rpp.edcapp.domain.AccountType
import com.eggdigital.rpp.edcapp.dto.CustomerSlipChannelResponseDto
import com.eggdigital.rpp.edcapp.exception.CustomThirdPartyExcepton
import com.eggdigital.rpp.edcapp.model.CodeMessage
import com.eggdigital.rpp.edcapp.model.CustomerSlipChannel
import com.eggdigital.rpp.edcapp.model.SlipChannel
import com.eggdigital.rpp.edcapp.repository.SlipChannelRepository
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

interface SlipChannelService {

    fun checkFirstTime(terminalId: String, accType: AccountType, accValue: String): CustomerSlipChannelResponseDto

    fun createOrUpdateSlipChannel(terminalId: String, thaiId: String, slipChannel: SlipChannel, value: String): CustomerSlipChannelResponseDto
}

@Service
class SlipChannelServiceImp : SlipChannelService {

    var log = LoggerFactory.getLogger(this.javaClass)

    @Autowired
    lateinit var apiBsService: ApiBsService

    @Autowired
    lateinit var slipChannelRepository: SlipChannelRepository

    override fun checkFirstTime(terminalId: String, accType: AccountType, accValue: String): CustomerSlipChannelResponseDto {

        var tmpAccountType = accType.toString()
        if (accType == AccountType.TRUECARD) {
            tmpAccountType = "CARD"
        }

        var customerInfoResponseDto: ApiBsCustomerInfoResponseDto = apiBsService.getCustomerInfo(terminalId, tmpAccountType, accValue)

        if (customerInfoResponseDto.error != null) {
            throw  ApiKotlinErrorException(msg = "truecard not found",
                    status = ApiStatus((customerInfoResponseDto.error as CodeMessage).code,
                            (customerInfoResponseDto.error as CodeMessage).message
                    ))
        }

        var thaiId: String = customerInfoResponseDto.cardInfo!!.thaiId!!

        var customerSlipChannel = slipChannelRepository.findOne(thaiId)

        var response = CustomerSlipChannelResponseDto(true, CustomerSlipChannel(thaiId, SlipChannel.NONE, ""))

        if (customerSlipChannel != null) {
            response.firstTime = false
            response.customer = CustomerSlipChannel(customerSlipChannel.thaiId,
                    customerSlipChannel.channel,
                    if (customerSlipChannel.value == null) "" else customerSlipChannel.value)
        }

        return response
    }

    override fun createOrUpdateSlipChannel(terminalId: String, thaiId: String, slipChannel: SlipChannel, value: String): CustomerSlipChannelResponseDto {

        var customerSlipChannel: CustomerSlipChannelResponseDto

        try {
            customerSlipChannel = checkFirstTime(terminalId, AccountType.THAIID, thaiId)
            var newSlipChannel = com.eggdigital.rpp.edcapp.domain.SlipChannel()
            newSlipChannel.channel = slipChannel
            newSlipChannel.value = value
            newSlipChannel.thaiId = thaiId

            slipChannelRepository.save(newSlipChannel)

            customerSlipChannel.customer.channel = slipChannel
            customerSlipChannel.customer.value = value

        } catch (ex: ApiKotlinErrorException) {
            throw CustomThirdPartyExcepton(ApiStatus(ex.status!!.code, ex.status!!.message))
        }

        return customerSlipChannel
    }
}