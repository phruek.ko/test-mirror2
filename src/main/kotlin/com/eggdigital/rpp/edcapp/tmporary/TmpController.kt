package com.eggdigital.rpp.edcapp.tmporary

import com.eggdigital.rpp.edcapp.tmporary.model.EggSignature
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import java.security.KeyFactory
import java.security.PublicKey
import java.security.spec.X509EncodedKeySpec
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import javax.crypto.Cipher

@RestController
class TmpController {

    private var log = LoggerFactory.getLogger(this.javaClass)

    @GetMapping("/signature")
    fun getSignature(): EggSignature {
        var now = LocalDateTime.now()
        var dateTimeFormatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME
        var plainText = "eggdigital|${now.format(dateTimeFormatter)}"

        var publicKeyFile = Thread.currentThread().contextClassLoader.getResourceAsStream("key/public-key.pem")
        var publicKeyText = publicKeyFile.reader().readText()
        var publicKeyBytes = Base64.getDecoder().decode(publicKeyText.toByteArray())
        var keyFactory: KeyFactory = KeyFactory.getInstance("RSA")
        var publicKey: PublicKey = keyFactory.generatePublic(X509EncodedKeySpec(publicKeyBytes))
        var cipherText = encrypt(plainText, publicKey)
        return EggSignature("eggdigital", now, cipherText)
    }

    fun encrypt(plainText: String, publicKey: PublicKey): String {
        var cipher: Cipher = Cipher.getInstance("RSA")
        cipher.init(Cipher.ENCRYPT_MODE, publicKey)
        var cipherBytes: ByteArray = cipher.doFinal(plainText.toByteArray())
        return Base64.getEncoder().encodeToString(cipherBytes)
    }
}