package com.eggdigital.rpp.edcapp.tmporary.model

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonProperty
import java.time.LocalDateTime

class EggSignature(
        @get:JsonProperty("signature")
        val signature: String,

        @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:MM:ss")
        @get:JsonProperty("dateTime")
        val dateTime: LocalDateTime,

        @get:JsonProperty("cipherText")
        val cipherText: String)