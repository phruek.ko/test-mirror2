package com.eggdigital.rpp.edcapp.api.edccampaign.dto

import com.eggdigital.rpp.edcapp.api.common.ApiCommonResponseDto
import com.fasterxml.jackson.annotation.JsonProperty

class ApiEdcCampaignDetailResponseDto(status: Status,
                                      data: List<Data>?,
                                      val total: Any?) : ApiCommonResponseDto(status, data) {

    class Data(

            @JsonProperty("brand_id")
            val brandId: String,

            @JsonProperty("terminal_id")
            val terminalId: String,

            @JsonProperty("outlet_id")
            val outletId: String,

            @JsonProperty("campaign_id")
            val campaignId: String,

            @JsonProperty("campaign_name")
            val campaignName: String,

            @JsonProperty("image")
            val image: String,

            @JsonProperty("campaign_start")
            val campaignStart: String,

            @JsonProperty("campaign_end")
            val campaignEnd: String,

            @JsonProperty("modified_date")
            val modifiedDate: String,

            @JsonProperty("status")
            val status: String)

}