package com.eggdigital.rpp.edcapp.api.bs.dto

import com.fasterxml.jackson.annotation.JsonProperty
import javax.validation.constraints.NotNull

class ApiPaymentChargeRequestDto(

        @JvmField
        @NotNull
        @JsonProperty("brand_id")
        val brandId: String,

        @JvmField
        @NotNull
        @JsonProperty("outlet_id")
        val outletId: String,

        @JvmField
        @NotNull
        @JsonProperty("terminal_id")
        val terminalId: String,

        @JvmField
        @NotNull
        @JsonProperty("amount")
        val amount: String,

        @JvmField
        @NotNull
        @JsonProperty("currency")
        val currency: String,

        @JvmField
        @NotNull
        @JsonProperty("payment_code")
        val paymentCode: String,

        @JvmField
        @NotNull
        @JsonProperty("payment_method")
        val paymentMethod: String,

        @JvmField
        @NotNull
        @JsonProperty("description")
        val description: String? = ""
)