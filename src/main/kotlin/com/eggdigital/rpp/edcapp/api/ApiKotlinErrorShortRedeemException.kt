package com.eggdigital.rpp.edcapp.api

import com.eggdigital.rpp.edcapp.api.model.ApiError
import com.eggdigital.rpp.edcapp.api.model.ApiStatus

class ApiKotlinErrorShortRedeemException(msg: String?,
                              var errors: MutableList<ApiError>? = mutableListOf(),
                              var status: ApiStatus? = null) : RuntimeException(msg)