package com.eggdigital.rpp.edcapp.api.bs.dto

import com.eggdigital.rpp.edcapp.api.common.ApiCommonResponseDto
import com.fasterxml.jackson.annotation.JsonProperty

class ApiBsRedeemCancelResponseDto(status: Status,
                                   data: Data?,

                                   @JsonProperty("tx_ref_id")
                                   val txRefId: String?,

                                   @JsonProperty("total")
                                   val total: Int?) : ApiCommonResponseDto(status, data) {

    class Data(
            @JsonProperty("timestamp")
            val timetamp: String)

}