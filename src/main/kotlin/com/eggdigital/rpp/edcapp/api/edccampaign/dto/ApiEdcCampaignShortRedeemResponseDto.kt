package com.eggdigital.rpp.edcapp.api.edccampaign.dto

import com.eggdigital.rpp.edcapp.api.common.ApiCommonResponseDto
import com.fasterxml.jackson.annotation.JsonProperty

class ApiEdcCampaignShortRedeemResponseDto(status: Status,
                                           data: List<Data>?) : ApiCommonResponseDto(status, data) {

    class Data (
        @JsonProperty("brand_id")
        val brandId: String,

        @JsonProperty("terminal_id")
        val terminalId: String,

        @JsonProperty("outlet_id")
        val outletId: String,

        @JsonProperty("campaign_id")
        val campaignId: String,

        @JsonProperty("campaign_name")
        val campaignName: String)

}