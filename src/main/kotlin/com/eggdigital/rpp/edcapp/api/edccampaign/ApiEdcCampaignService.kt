package com.eggdigital.rpp.edcapp.api.edccampaign

import com.eggdigital.rpp.edcapp.api.ApiKotlinSender
import com.eggdigital.rpp.edcapp.api.edccampaign.dto.ApiEdcCampaignDetailResponseDto
import com.eggdigital.rpp.edcapp.api.edccampaign.dto.ApiEdcCampaignShortRedeemResponseDto
import com.eggdigital.rpp.edcapp.enumuration.CampaignType
import com.eggdigital.rpp.edcapp.service.CryptoService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpMethod
import org.springframework.stereotype.Service

interface ApiEdcCampaignService {

    fun getCampaignDetail(campaignCode: String): ApiEdcCampaignDetailResponseDto
    fun getCampaignDetail(campaignCode: String, terminalId: String): ApiEdcCampaignDetailResponseDto
    fun getCampaignDetail(campaignCode: String, terminalId: String, campaignType: CampaignType): ApiEdcCampaignDetailResponseDto
    fun getCampaignDetail(brandId: String, outletId: String, terminal: String, campaignId: String): ApiEdcCampaignDetailResponseDto
    fun getCampaignShortRedeem(brandId: String, outletId: String, terminal: String, campaignId: String): ApiEdcCampaignShortRedeemResponseDto
}


@Service
class ApiEdcCampaignServiceImpl : ApiEdcCampaignService {

    @Autowired
    private lateinit var apiKotlinSender: ApiKotlinSender

    @Autowired
    lateinit var cryptoService: CryptoService

    override fun getCampaignDetail(campaignCode: String, terminalId: String): ApiEdcCampaignDetailResponseDto {

        var params = mutableMapOf<String, String>()
        params.put("campaign_id", campaignCode)
        params.put("terminal_id", terminalId)

        var customHeaders = hashMapOf<String, String>(
                Pair<String, String>("X-Signature", cryptoService.getCipherText()))

        return apiKotlinSender.exchange(HttpMethod.GET,
                ApiKotlinSender.Microservice.EDC_CAMPAIGN,
                ApiEdcCampaignDetailResponseDto::class.java,
                null, params, customHeaders = customHeaders)
    }

    override fun getCampaignDetail(campaignCode: String, terminalId: String, campaignType: CampaignType): ApiEdcCampaignDetailResponseDto {
        var params = mutableMapOf<String, String>()
        params.put("campaign_id", campaignCode)
        params.put("terminal_id", terminalId)
        params.put("campaign_type", campaignType.name)

        var customHeaders = hashMapOf<String, String>(
                Pair<String, String>("X-Signature", cryptoService.getCipherText()))

        return apiKotlinSender.exchange(HttpMethod.GET,
                ApiKotlinSender.Microservice.EDC_CAMPAIGN,
                ApiEdcCampaignDetailResponseDto::class.java,
                null, params, customHeaders = customHeaders)
    }

    override fun getCampaignDetail(brandId: String, outletId: String, terminal: String, campaignId: String): ApiEdcCampaignDetailResponseDto {
        var params = mutableMapOf<String, String>()
        params.put("brand_id", brandId)
        params.put("outlet_id", outletId)
        params.put("terminal_id", terminal)
        params.put("campaign_id", campaignId)
        params.put("campaign_type", CampaignType.ALL.name)

        var customHeaders = hashMapOf<String, String>(
                Pair<String, String>("X-Signature", cryptoService.getCipherText()))

        return apiKotlinSender.exchange(HttpMethod.GET,
                ApiKotlinSender.Microservice.EDC_CAMPAIGN,
                ApiEdcCampaignDetailResponseDto::class.java,
                null, params, customHeaders = customHeaders)

    }

    override fun getCampaignDetail(campaignId: String): ApiEdcCampaignDetailResponseDto {
        var params = mutableMapOf<String, String>()
        params.put("campaign_id", campaignId)

        var customHeaders = hashMapOf<String, String>(
                Pair<String, String>("X-Signature", cryptoService.getCipherText()))

        return apiKotlinSender.exchange(HttpMethod.GET,
                ApiKotlinSender.Microservice.EDC_CAMPAIGN,
                ApiEdcCampaignDetailResponseDto::class.java,
                null, params, customHeaders = customHeaders)
    }

    override fun getCampaignShortRedeem(brandId: String, outletId: String, terminal: String, rewardCode: String): ApiEdcCampaignShortRedeemResponseDto {
        var customHeaders = hashMapOf<String, String>(
                Pair<String, String>("X-Signature", cryptoService.getCipherText()))

        var params = mutableMapOf<String, String>()
        params.put("brand_id", brandId)
        params.put("outlet_id", outletId)
        params.put("terminal_id", terminal)
        params.put("campaign_code", rewardCode)

        return apiKotlinSender.exchange(HttpMethod.GET,
                ApiKotlinSender.Microservice.EDC_CAMPAIGN_FIND,
                ApiEdcCampaignShortRedeemResponseDto::class.java,
                null, params, customHeaders = customHeaders)
    }
}