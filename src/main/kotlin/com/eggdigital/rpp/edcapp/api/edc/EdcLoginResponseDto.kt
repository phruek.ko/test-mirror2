package com.eggdigital.rpp.edcapp.api.edc

import com.eggdigital.rpp.edcapp.api.common.ApiCommonResponseDto
import com.fasterxml.jackson.annotation.JsonProperty

class EdcLoginResponseDto(data: Data? = null) : ApiCommonResponseDto(data = data) {
    class Data(
            @JsonProperty("username")
            var username: String,

            @JsonProperty("password")
            var password: String,

            @JsonProperty("content_type")
            var contentType: String,

            @JsonProperty("content_id")
            var contentId: String,

            @JsonProperty("status")
            var status: String,

            @JsonProperty("created_at")
            var createdAt: String,

            @JsonProperty("updated_at")
            var updatedAt: String,

            @JsonProperty("id")
            var id: String)

}