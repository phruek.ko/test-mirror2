package com.eggdigital.rpp.edcapp.api.bs.dto

import com.eggdigital.rpp.edcapp.api.common.ApiCommonResponseDto
import com.eggdigital.rpp.edcapp.model.CodeMessage

class ApiBsCustomerInfoResponseDto(var cardInfo: CardInfo? = null,
                                   error: CodeMessage? = null) : ApiCommonResponseDto(error = error) {

    class CardInfo {
        var cardStatus: String? = ""
        var cardType: String? = ""
        var cardId: String? = ""
        var cardExpired: String? = ""
        var thaiId: String? = ""
        var customerName: String? = ""
    }
}