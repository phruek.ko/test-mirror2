package com.eggdigital.rpp.edcapp.api.model

class ApiStatus(
        val code: String,
        val message: String)