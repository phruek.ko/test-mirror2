package com.eggdigital.rpp.edcapp.api.edc

import com.eggdigital.rpp.edcapp.api.ApiKotlinSender
import com.eggdigital.rpp.edcapp.service.CryptoService
import org.apache.commons.codec.binary.Base64
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.stereotype.Service
import java.util.*

interface ApiEdcService {
    fun verifyLogin(username: String, password: String): Boolean
}

@Service
class ApiEdcServiceImpl : ApiEdcService {

    @Autowired
    lateinit var apiKotlinSender: ApiKotlinSender

    @Autowired
    lateinit var cryptoService: CryptoService

    override fun verifyLogin(username: String, password: String): Boolean {

        val token = Base64.encodeBase64String((username + ":" + password).toByteArray())
        val customHeaders = HashMap<String, String>()
        customHeaders[HttpHeaders.AUTHORIZATION] = "Basic " + token
        customHeaders["X-Signature"] = cryptoService.getCipherText()

        val response = apiKotlinSender.exchange(HttpMethod.POST, ApiKotlinSender.Microservice.EDC_LOGIN,
                EdcLoginResponseDto::class.java, customHeaders = customHeaders, checkSuccess = false)

        if (response.status!!.code == 200) {

            val data = response.data as EdcLoginResponseDto.Data

            if (data.status.equals("active")) {
                return true
            }
        }

        return false
    }
}

