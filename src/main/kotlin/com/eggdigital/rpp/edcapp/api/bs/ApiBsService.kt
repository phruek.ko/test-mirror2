package com.eggdigital.rpp.edcapp.api.bs

import com.eggdigital.rpp.edcapp.api.ApiKotlinSender
import com.eggdigital.rpp.edcapp.api.bs.dto.ApiBsCustomerInfoResponseDto
import com.eggdigital.rpp.edcapp.api.bs.dto.ApiBsRedeemCancelResponseDto
import com.eggdigital.rpp.edcapp.api.bs.dto.ApiPaymentCancelResponseDto
import com.eggdigital.rpp.edcapp.api.bs.dto.ApiPaymentChargeResponseDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpMethod
import org.springframework.stereotype.Service

interface ApiBsService {

    fun paymentCharge(brandId: String,
                      outletId: String,
                      terminalId: String,
                      txRefId: String,
                      amount: String,
                      currency: String,
                      paymentCode: String,
                      paymentMethod: String,
                      description: String): ApiPaymentChargeResponseDto

    fun paymentCancel(brandId: String,
                      outletId: String,
                      terminalId: String,
                      txRefId: String): ApiPaymentCancelResponseDto

    fun redeemCancel(brandId: String, outletId: String, terminalId: String, txRefId: String): ApiBsRedeemCancelResponseDto


    fun getCustomerInfo(terminalId: String, accType: String, accValue: String): ApiBsCustomerInfoResponseDto
}

@Service
class ApiBsServiceImpl : ApiBsService {

    @Autowired
    lateinit var apiKotlinSender: ApiKotlinSender

    override fun paymentCharge(brandId: String, outletId: String, terminalId: String, txRefId: String, amount: String, currency: String, paymentCode: String, paymentMethod: String, description: String): ApiPaymentChargeResponseDto {

        var body = hashMapOf<String, String>()
        body.put("brand_id", brandId)
        body.put("outlet_id", outletId)
        body.put("terminal_id", terminalId)
        body.put("tx_ref_id", txRefId)
        body.put("amount", amount)
        body.put("currency", currency)
        body.put("payment_code", paymentCode)
        body.put("payment_method", paymentMethod)
        var desp = description
        if (desp.isEmpty()) {
            desp = "no description"
        }
        body.put("description", desp)

        return apiKotlinSender.exchange(HttpMethod.POST,
                ApiKotlinSender.Microservice.PAYMENT_CHARGE,
                ApiPaymentChargeResponseDto::class.java,
                paramsOrBody = body)
    }

    override fun paymentCancel(brandId: String, outletId: String, terminalId: String, txRefId: String): ApiPaymentCancelResponseDto {
        var body = hashMapOf<String, String>()
        body.put("brand_id", brandId)
        body.put("outlet_id", outletId)
        body.put("terminal_id", terminalId)
        body.put("tx_ref_id", txRefId)

        return apiKotlinSender.exchange(HttpMethod.POST,
                ApiKotlinSender.Microservice.PAYMENT_CANCEL,
                ApiPaymentCancelResponseDto::class.java,
                paramsOrBody = body)
    }

    override fun redeemCancel(brandId: String, outletId: String, terminalId: String, txRefId: String): ApiBsRedeemCancelResponseDto {
        var body = hashMapOf<String, String>()
        body.put("brand_id", brandId)
        body.put("outlet_id", outletId)
        body.put("terminal_id", terminalId)
        body.put("tx_ref_id", txRefId)

        return apiKotlinSender.exchange(HttpMethod.POST,
                ApiKotlinSender.Microservice.REDEEM_REVERSAL,
                ApiBsRedeemCancelResponseDto::class.java,
                paramsOrBody = body)
    }

    override fun getCustomerInfo(terminalId: String, accType: String, accValue: String): ApiBsCustomerInfoResponseDto {
        var params = hashMapOf<String, String>()
        params.put("terminal_id", terminalId)
        params.put("acc_type", accType)
        params.put("acc_value", accValue)

        return apiKotlinSender.exchange(HttpMethod.GET,
                ApiKotlinSender.Microservice.CUSTOMER_INFO,
                ApiBsCustomerInfoResponseDto::class.java,
                paramsOrBody = params,
                checkSuccess = false)
    }
}