package com.eggdigital.rpp.edcapp.api.common

import com.eggdigital.rpp.edcapp.api.model.ApiError
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonSetter

@JsonInclude(JsonInclude.Include.NON_NULL)
open class ApiCommonResponseDto(

        @JsonProperty("status")
        val status: Status? = null,

        @JsonProperty("data")
        val data: Any? = null,

        @JsonProperty("error")
        val error: Any? = null,

        @JsonProperty("errors")
        val errors: List<ApiError>? = null

) {
    class Status(
            @JsonProperty("code")
            val code: Int,

            @JsonProperty("message")
            var message: String? = null
    ) {
        @JsonSetter("text")
        fun setText(text: String) {
            this.message = text
        }
    }
}
