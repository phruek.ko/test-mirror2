package com.eggdigital.rpp.edcapp.api.point.dto

import com.eggdigital.rpp.edcapp.api.common.ApiCommonResponseDto
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

@JsonInclude(JsonInclude.Include.NON_NULL)
class ApiPointReversalResponseDto(status: Status,
                                  data: Data? = null,
                                  @JsonProperty("tx_ref_id")
                                  val txRefId: String? = null,

                                  @JsonProperty("total")
                                  val total: Int? = null,

                                  @JsonProperty("errors")
                                  val errors2: List<Message>? = null) : ApiCommonResponseDto(status, data) {

    class Data(
            @JsonProperty("brand_id")
            var brandId: String,

            @JsonProperty("outlet_id")
            var outletId: String,

            @JsonProperty("terminal_id")
            var terminalId: String,

            @JsonProperty("timestamp")
            var timestamp: String)

    class Message(
            @JsonProperty("message")
            val message: String
    )
}