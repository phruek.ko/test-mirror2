package com.eggdigital.rpp.edcapp.api.user

import com.eggdigital.rpp.edcapp.api.ApiKotlinSender
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpMethod
import org.springframework.stereotype.Service

interface ApiUserService {

    fun login(username: String, password: String, userType: String): ApiUserResponseDto
}

@Service
class ApiUserServiceImpl : ApiUserService {

    @Autowired
    lateinit var apiKotlinSender: ApiKotlinSender

    override fun login(username: String, password: String, userType: String): ApiUserResponseDto {

        var body = hashMapOf<String, String>()
        body.put("username", username)
        body.put("password", password)
        body.put("user_type", userType)

        return apiKotlinSender.exchange(HttpMethod.POST,
                ApiKotlinSender.Microservice.USER_LOGIN,
                ApiUserResponseDto::class.java,
                paramsOrBody = body)
    }
}