package com.eggdigital.rpp.edcapp.api.model

import com.fasterxml.jackson.annotation.JsonProperty

class ApiError(
        @JsonProperty("property")
        var property: String? = "",

        @JsonProperty("message")
        var message: String? = "")