package com.eggdigital.rpp.edcapp.api.bs.dto

import com.fasterxml.jackson.annotation.JsonProperty

class ApiPaymentCancelRequestDto(

        @JsonProperty("brand_id")
        brandId: String,

        @JsonProperty("outlet_id")
        outletId: String,

        @JsonProperty("terminal_id")
        terminalId: String,

        @JsonProperty("tx_ref_id")
        txRefId: String)