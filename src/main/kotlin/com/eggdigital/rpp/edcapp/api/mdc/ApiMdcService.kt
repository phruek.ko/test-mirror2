package com.eggdigital.rpp.edcapp.api.mdc

import com.eggdigital.rpp.edcapp.api.ApiKotlinSender
import com.eggdigital.rpp.edcapp.api.mdc.dto.ApiMdcSearchAllProductResponseDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpMethod
import org.springframework.stereotype.Service

interface ApiMdcService {

    fun getSearchAllProduct(certificateId: String): ApiMdcSearchAllProductResponseDto
}

@Service
class ApiMdcServiceImpl : ApiMdcService {

    @Autowired
    lateinit var apiKotlinSender: ApiKotlinSender

    override fun getSearchAllProduct(certificateId: String): ApiMdcSearchAllProductResponseDto {

        var params = hashMapOf<String, String>()
        params.put("transaction_id", "000000000")
        params.put("certificate_id", certificateId)

        return apiKotlinSender.exchange(HttpMethod.GET,
                ApiKotlinSender.Microservice.MCP_SEARCH_ALL_PRODUCT,
                ApiMdcSearchAllProductResponseDto::class.java,
                paramsOrBody = params,
                checkSuccess = false)
    }
}