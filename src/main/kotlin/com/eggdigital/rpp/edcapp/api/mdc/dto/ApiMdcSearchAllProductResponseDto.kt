package com.eggdigital.rpp.edcapp.api.mdc.dto

import com.eggdigital.rpp.edcapp.api.common.ApiCommonResponseDto
import com.eggdigital.rpp.edcapp.model.CodeMessage
import com.fasterxml.jackson.annotation.JsonProperty

class ApiMdcSearchAllProductResponseDto(data: Data?, var header: CodeMessage?) : ApiCommonResponseDto(data = data) {

    class Data(var product: Product) {

        class Product(

                @JsonProperty("tmvh_product")
                var tmvhProduct: Map<String, Any>? = null)
    }
}