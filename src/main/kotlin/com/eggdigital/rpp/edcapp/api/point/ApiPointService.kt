package com.eggdigital.rpp.edcapp.api.point

import com.eggdigital.rpp.edcapp.api.ApiKotlinSender
import com.eggdigital.rpp.edcapp.api.point.dto.ApiPointReversalResponseDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpMethod
import org.springframework.stereotype.Service

interface ApiPointService {

    fun cancelPoint(brandId: String, outletId: String, terminalId: String, txRefId: String): ApiPointReversalResponseDto
}

@Service
class ApiPointServiceImpl : ApiPointService {

    @Autowired
    lateinit var apiKotlinSender: ApiKotlinSender

    override fun cancelPoint(brandId: String, outletId: String, terminalId: String, txRefId: String): ApiPointReversalResponseDto {

        var body = hashMapOf<String, String>()
        body.put("brand_id", brandId)
        body.put("outlet_id", outletId)
        body.put("terminal_id", terminalId)
        body.put("tx_ref_id", txRefId)

        return apiKotlinSender.exchange(HttpMethod.POST,
                ApiKotlinSender.Microservice.POINT_REVERSAL,
                ApiPointReversalResponseDto::class.java,
                paramsOrBody = body
        )
    }
}