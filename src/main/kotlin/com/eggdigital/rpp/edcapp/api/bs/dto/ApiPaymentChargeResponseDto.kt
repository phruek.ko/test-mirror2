package com.eggdigital.rpp.edcapp.api.bs.dto

import com.eggdigital.rpp.edcapp.api.common.ApiCommonResponseDto
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

@JsonInclude(JsonInclude.Include.NON_NULL)
class ApiPaymentChargeResponseDto(status: Status,
                                  data: Data?,

                                  @JsonProperty("txRefId")
                                  val txRefId: String?,

                                  @JsonProperty("total")
                                  val total: Int?) : ApiCommonResponseDto(status, data) {

    class Data(
            @JsonProperty("timestamp")
            val timetamp: String)
}