package com.eggdigital.rpp.edcapp.api

import com.eggdigital.rpp.edcapp.api.common.ApiCommonResponseDto
import com.eggdigital.rpp.edcapp.api.model.ApiError
import com.eggdigital.rpp.edcapp.api.model.ApiStatus
import com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationContext
import org.springframework.http.*
import org.springframework.http.client.ClientHttpResponse
import org.springframework.stereotype.Component
import org.springframework.web.client.ResponseErrorHandler
import org.springframework.web.client.RestTemplate
import org.springframework.web.util.UriComponentsBuilder
import java.io.IOException

@Component
class ApiKotlinSender {

    private var log = LoggerFactory.getLogger(this.javaClass)

    @Autowired
    lateinit var restTemplate: RestTemplate

    @Autowired
    lateinit var applicationContext: ApplicationContext

    enum class Microservice {
        PAYMENT_CHARGE,
        PAYMENT_CANCEL,
        REDEEM_REVERSAL,
        POINT_REVERSAL,
        EDC_CAMPAIGN,
        CUSTOMER_INFO,
        MCP_SEARCH_ALL_PRODUCT,
        EDC_LOGIN,
        USER_LOGIN,
        EDC_CAMPAIGN_FIND
    }

    fun getUrlFromMicroservice(ms: Microservice): String {
        var env = applicationContext.environment
        when (ms) {
            Microservice.CUSTOMER_INFO -> return env.getProperty("ms.bs.customer.info")
            Microservice.PAYMENT_CHARGE -> return env.getProperty("ms.bs.payment.charge")
            Microservice.PAYMENT_CANCEL -> return env.getProperty("ms.bs.payment.charge.cancel")
            Microservice.REDEEM_REVERSAL -> return env.getProperty("ms.bs.redeem.reversal")
            Microservice.POINT_REVERSAL -> return env.getProperty("ms.point.reversal")
            Microservice.EDC_CAMPAIGN -> return env.getProperty("ms.edc.campaign.detail")
            Microservice.MCP_SEARCH_ALL_PRODUCT -> return env.getProperty("ms.mcp.searchAllProduct")
            Microservice.EDC_LOGIN -> return env.getProperty("ms.edc.android.login")
            Microservice.USER_LOGIN -> return env.getProperty("ms.user.login")
            Microservice.EDC_CAMPAIGN_FIND -> return env.getProperty("ms.edc.campaign.find")

            else -> return ""
        }
    }

    @Throws(ApiKotlinErrorException::class, ApiKotlinErrorShortRedeemException::class)
    fun <T> exchange(httpMethod: HttpMethod,
                     ms: Microservice,
                     responseType: Class<T>,
                     uriVariables: Map<String, String>? = null,
                     paramsOrBody: Map<String, String>? = null,
                     body: Any? = null,
                     checkSuccess: Boolean? = true,
                     customHeaders: Map<String, String>? = null): T {

        var responseEntity: ResponseEntity<T>? = null
        var headers = HttpHeaders()
        headers.set("Accept", MediaType.APPLICATION_JSON_UTF8_VALUE)
        if (customHeaders != null) {
            customHeaders.forEach { map ->
                headers.set(map.key, map.value)
            }
        }

        var url = getUrlFromMicroservice(ms)
        var builder = UriComponentsBuilder.fromHttpUrl(url)
        log.info("request api [{}] : {} {}", ms.toString(), ObjectMapper().writeValueAsString(paramsOrBody), ObjectMapper().writeValueAsString(body))

        restTemplate!!.errorHandler = object : ResponseErrorHandler {
            @Throws(IOException::class)
            override fun hasError(response: ClientHttpResponse): Boolean {
                return false
            }

            @Throws(IOException::class)
            override fun handleError(response: ClientHttpResponse) {

            }
        }

        try {

            when (httpMethod) {

                HttpMethod.GET -> {
                    var entityMap = HttpEntity<Any>(headers)
                    if (paramsOrBody != null) {
                        paramsOrBody?.forEach({ name, values -> builder.queryParam(name, values) })
                    }

                    if (uriVariables == null) {
                        responseEntity = restTemplate.exchange(builder.build().toUriString(), httpMethod, entityMap, responseType)
                    } else {
                        responseEntity = restTemplate.exchange(builder.build().toUriString(), httpMethod, entityMap, responseType, uriVariables)
                    }

                }

                HttpMethod.POST, HttpMethod.PUT -> {
                    if (paramsOrBody != null) {
                        var entityMap = HttpEntity<Any>(paramsOrBody, headers)
                        if (uriVariables == null) {
                            responseEntity = restTemplate.exchange(builder.build().toUriString(), httpMethod, entityMap, responseType)
                        } else {
                            responseEntity = restTemplate.exchange(builder.build().toUriString(), httpMethod, entityMap, responseType, uriVariables)

                        }
                    }

                    if (body != null) {
                        var entityMap = HttpEntity<Any>(body, headers)
                        if (uriVariables == null) {
                            responseEntity = restTemplate.exchange(builder.build().toUriString(), httpMethod, entityMap, responseType)
                        } else {
                            responseEntity = restTemplate.exchange(builder.build().toUriString(), httpMethod, entityMap, responseType, uriVariables)

                        }
                    }

                    if (body == null && paramsOrBody == null) {
                        var entityMap = HttpEntity<Any>(headers)
                        responseEntity = restTemplate.exchange(builder.build().toUriString(), httpMethod, entityMap, responseType)
                    }
                }
            }

            if (responseEntity == null) {
                throw ApiKotlinErrorException("No response")
            }

            var response = responseEntity.body as ApiCommonResponseDto
            log.info("response api [{}] : {}", ms.toString(), ObjectMapper().writeValueAsString(response))

            if (checkSuccess!!) {
                if (!responseEntity.statusCode.is2xxSuccessful) {
                    var errors = mutableListOf<ApiError>()
                    if (response.errors != null) {
                        errors = response.errors as MutableList<ApiError>
                    }
                    throw ApiKotlinErrorException("${responseEntity.statusCode.value()} ${responseEntity.statusCode.reasonPhrase}",
                            errors,
                            ApiStatus(responseEntity.statusCode.value().toString(), responseEntity.statusCode.reasonPhrase))
                }
                if (response.status!!.code != HttpStatus.OK.value()) {
                    var errors = mutableListOf<ApiError>()
                    if (response.errors != null) {
                        errors = response.errors as MutableList<ApiError>
                    }
                    throw ApiKotlinErrorException("${response.status!!.code} ${response.status!!.message!!}",
                            errors,
                            ApiStatus(response.status!!.code.toString(), response.status!!.message!!))
                }
            }
        } catch (ex: ApiKotlinErrorException) {
            log.error("{}", ex)
            if(!ex.errors!!.isEmpty() && ex.errors!!.get(0).message!!.contains("Can not found this reward code")){
                throw ApiKotlinErrorShortRedeemException("$ms : ${ex.message}", ex.errors, ex.status)
            }else{
                throw ApiKotlinErrorException("$ms : ${ex.message}", ex.errors, ex.status)
            }
        }  catch (ex: Exception) {
            log.error("{}", ex)
            throw Exception("$ms : ${ex.message}")
        }
        return responseEntity.body
    }
}


