package com.eggdigital.rpp.edcapp.exception

class NotFoundException(msg: String) : RuntimeException(msg)