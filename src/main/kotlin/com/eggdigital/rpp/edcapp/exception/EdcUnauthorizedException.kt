package com.eggdigital.rpp.edcapp.exception

class EdcUnauthorizedException : RuntimeException()