package com.eggdigital.rpp.edcapp.exception

class MessagingException(msg: String) : RuntimeException(msg)