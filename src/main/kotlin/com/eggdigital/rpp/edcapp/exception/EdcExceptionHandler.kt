package com.eggdigital.rpp.edcapp.exception

import com.eggdigital.common.util.StringUtils
import com.eggdigital.rpp.edcapp.api.ApiKotlinErrorException
import com.eggdigital.rpp.edcapp.api.ApiKotlinErrorShortRedeemException
import com.eggdigital.rpp.edcapp.config.EdcMessageConfig
import com.eggdigital.rpp.edcapp.dto.ErrorResponseDto
import com.fasterxml.jackson.module.kotlin.MissingKotlinParameterException
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.validation.BindException
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.MissingServletRequestParameterException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import javax.servlet.http.HttpServletResponse

@ControllerAdvice
class EdcExceptionHandler {

    var log = LoggerFactory.getLogger(this.javaClass)

    @Autowired
    lateinit var edcMessageConfig: EdcMessageConfig

    @ExceptionHandler(Exception::class)
    fun exception(response: HttpServletResponse, ex: Exception): ResponseEntity<Any> {
        log.error("global exception : {}", ex)
        var body = ErrorResponseDto(
                ErrorResponseDto.Erorr(
                        code = HttpStatus.INTERNAL_SERVER_ERROR.value().toString(),
                        message = HttpStatus.INTERNAL_SERVER_ERROR.reasonPhrase,
                        displayCode = HttpStatus.INTERNAL_SERVER_ERROR.value().toString()
                )
        )
        return ResponseEntity(body, HttpStatus.INTERNAL_SERVER_ERROR)
    }

    @ExceptionHandler(BindException::class,
            HttpMessageNotReadableException::class,
            MethodArgumentNotValidException::class,
            MissingServletRequestParameterException::class,
            MissingKotlinParameterException::class)
    fun badRequest(response: HttpServletResponse, ex: Exception): ResponseEntity<Any> {
        log.error("bad request : {}", ex)

        var body = ErrorResponseDto(
                ErrorResponseDto.Erorr(
                        code = HttpStatus.BAD_REQUEST.value().toString(),
                        message = HttpStatus.BAD_REQUEST.reasonPhrase,
                        displayCode = HttpStatus.BAD_REQUEST.value().toString()
                )
        )

        when (ex) {
            is MethodArgumentNotValidException -> {
                if (!ex.bindingResult.fieldErrors.isEmpty()) {
                    ex.bindingResult.fieldErrors.forEach { error ->
                        body.error.errors.add(
                                ErrorResponseDto.Erorr.PropertyMessage(
                                        StringUtils.toSnakeCase(error.field),
                                        error.defaultMessage)
                        )
                    }
                } else {
                    var allError = ex.bindingResult.allErrors[0]
                    body.error.errors.add(
                            ErrorResponseDto.Erorr.PropertyMessage(allError.code, allError.defaultMessage)
                    )
                }
            }
            is BindException -> {
                ex.bindingResult.fieldErrors.forEach { error ->
                    body.error.errors.add(
                            ErrorResponseDto.Erorr.PropertyMessage(
                                    StringUtils.toSnakeCase(error.field),
                                    error.defaultMessage
                            )
                    )
                }
            }
        }
        return ResponseEntity(body, HttpStatus.BAD_REQUEST)
    }

    @ExceptionHandler(NotFoundException::class)
    fun notFound(response: HttpServletResponse, ex: Exception): ResponseEntity<Any> {
        log.error("not found exception : {}", ex.message)
        var body = ErrorResponseDto(
                ErrorResponseDto.Erorr(
                        code = HttpStatus.BAD_REQUEST.value().toString(),
                        message = HttpStatus.BAD_REQUEST.reasonPhrase,
                        displayCode = HttpStatus.BAD_REQUEST.value().toString()
                )
        )
        return ResponseEntity(body, HttpStatus.BAD_REQUEST)
    }

    @ExceptionHandler(ApiKotlinErrorException::class)
    fun thirdPartyNotSuccess(response: HttpServletResponse, ex: ApiKotlinErrorException): ResponseEntity<Any> {
        log.error("third party not success")
        var newCodeMessage = getDisplayCodeMessage(ex.status!!.code, ex.status!!.message)

        var body = ErrorResponseDto(
                ErrorResponseDto.Erorr(
                        code = "API_3_PARTY",
                        message = newCodeMessage.second,
                        displayCode = newCodeMessage.first
                )
        )

        if (ex.errors!!.isNotEmpty()) {
            ex.errors!!.forEach { error ->
                body.error.errors.add(
                        ErrorResponseDto.Erorr.PropertyMessage(
                                error.property!!,
                                error.message!!)
                )
            }

        }
        return ResponseEntity(body, HttpStatus.BAD_REQUEST)
    }

    @ExceptionHandler(ApiKotlinErrorShortRedeemException::class)
    fun apiKotlinErrorShortRedeemExceptionNotSuccess(response: HttpServletResponse, ex: ApiKotlinErrorShortRedeemException): ResponseEntity<Any> {
        log.error("third party not success")
        var newCodeMessage = Pair("","")

        var errorMessage = ex.errors!!.get(0).message
        if(errorMessage!!.contains("Can not found this reward code")){

            newCodeMessage = getDisplayCodeMessage("1014", errorMessage)
        } else {
            newCodeMessage = getDisplayCodeMessage(ex.status!!.code, ex.status!!.message)
        }

        var body = ErrorResponseDto(
                ErrorResponseDto.Erorr(
                        code = "API_3_PARTY",
                        message = newCodeMessage.second,
                        displayCode = newCodeMessage.first
                )
        )

        if (ex.errors!!.isNotEmpty()) {
            ex.errors!!.forEach { error ->
                body.error.errors.add(
                        ErrorResponseDto.Erorr.PropertyMessage(
                                error.property!!,
                                error.message!!)
                )
            }

        }
        return ResponseEntity(body, HttpStatus.BAD_REQUEST)
    }

    @ExceptionHandler(CustomThirdPartyExcepton::class)
    fun customThirdPartyException(response: HttpServletResponse, ex: CustomThirdPartyExcepton): ResponseEntity<Any> {
        log.error("custom api_3_party exception")

        var newCodeMessage = getDisplayCodeMessage(ex.status!!.code, ex.status!!.message)

        var body = ErrorResponseDto(
                ErrorResponseDto.Erorr(
                        code = "API_3_PARTY",
                        message = newCodeMessage.second,
                        displayCode = newCodeMessage.first
                )
        )
        return ResponseEntity(body, HttpStatus.BAD_REQUEST)
    }

    fun getDisplayCodeMessage(code: String, message: String): Pair<String, String> {
        val codeMessages = edcMessageConfig.edcDisplayMessages
        var code = code
        var message = message

        if (codeMessages.containsKey(code)) {
            var newCodeMessage = codeMessages.get(code)
            code = newCodeMessage!!.code
            message = newCodeMessage!!.message
        }
        return Pair(code, message)
    }
}