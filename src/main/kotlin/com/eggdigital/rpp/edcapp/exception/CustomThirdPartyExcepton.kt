package com.eggdigital.rpp.edcapp.exception

import com.eggdigital.rpp.edcapp.api.model.ApiStatus

class CustomThirdPartyExcepton(var status: ApiStatus) : RuntimeException()