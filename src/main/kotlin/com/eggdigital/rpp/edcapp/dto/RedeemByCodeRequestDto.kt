package com.eggdigital.rpp.edcapp.dto

import com.eggdigital.common.validation.EnumType
import com.eggdigital.rpp.edcapp.enumuration.CampaignType
import com.eggdigital.rpp.edcapp.enumuration.TransactionChannel
import com.eggdigital.rpp.edcapp.validation.constraints.RequiredField
import com.fasterxml.jackson.annotation.JsonProperty

class RedeemByCodeRequestDto : CommonRequestDto() {

    @RequiredField(field = "campaign code")
    @JsonProperty("campaign_code")
    lateinit var campaignCode: String

    @RequiredField(field = "reward code")
    @JsonProperty("reward_code")
    lateinit var rewardCode: String

    @JsonProperty("transaction_channel")
    @EnumType(enumClass = TransactionChannel::class, ignoreCase = true, message = "{error.transactionChannel.invalid}")
    var transactionChannel: String = ""

    @JsonProperty("campaign_type")
    @EnumType(enumClass = CampaignType::class, ignoreCase = true, message = "{error.campaign_type.invalid}")
    var campaignType: String = CampaignType.CUSTOMER.toString()
}