package com.eggdigital.rpp.edcapp.dto

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.PropertyNamingStrategy
import com.fasterxml.jackson.databind.annotation.JsonNaming
import java.time.LocalDateTime

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy::class)
class EarnPointResponseDto(
        brandId: String,
        outletId: String,
        terminalId: String,
        traceId: String,
        batchId: String,
        txRefId: String,
        transactionDate: LocalDateTime,
        trueyouId: String,

        @JvmField
        @JsonProperty("amount")
        val amount: String,

        @JvmField
        @JsonProperty("acc_type")
        val accType: String,

        @JvmField
        @JsonProperty("acc_value")
        val accValue: String,

        @JsonProperty("point")
        val point: String

) : CommonResponseDto(brandId,
        outletId,
        terminalId,
        traceId,
        batchId,
        txRefId,
        transactionDate,
        trueyouId)