package com.eggdigital.rpp.edcapp.dto

import com.eggdigital.common.validation.EnumType
import com.eggdigital.rpp.edcapp.domain.AccountType
import com.eggdigital.rpp.edcapp.enumuration.TransactionChannel
import com.eggdigital.rpp.edcapp.validation.constraints.RequiredField
import com.fasterxml.jackson.annotation.JsonProperty
import javax.validation.constraints.Pattern

class EarnPointRequestDto : CommonRequestDto() {

    @JsonProperty("acc_type")
    @RequiredField(field = "Account type")
    @EnumType(enumClass = AccountType::class, ignoreCase = true, message = "{error.accountType.invalid}")
    lateinit var accType: String

    @JsonProperty("acc_value")
    @RequiredField(field = "Account value")
    lateinit var accValue: String

    @JsonProperty("amount")
    @field:Pattern(regexp = "\\d{1,9}", message = "incorrect amount format.")
    @RequiredField(field = "amount")
    lateinit var amount: String

    @JsonProperty("transaction_channel")
    @EnumType(enumClass = TransactionChannel::class, ignoreCase = true, message = "{error.transactionChannel.invalid}")
    var transactionChannel: String = ""
}