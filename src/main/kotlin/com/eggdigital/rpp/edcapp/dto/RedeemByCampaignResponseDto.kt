package com.eggdigital.rpp.edcapp.dto

import com.fasterxml.jackson.annotation.JsonProperty
import java.time.LocalDateTime

class RedeemByCampaignResponseDto(
        brandId: String,
        outletId: String,
        terminalId: String,
        traceId: String,
        batchId: String,
        txRefId: String,
        transactionDate: LocalDateTime,
        trueyouId: String,

        @JsonProperty("customer_ref")
        var customerRef: String? = "",

        @JsonProperty("point_used")
        var pointUsed: String? = "",

        @JsonProperty("point_balance")
        var pointBalance: String? = ""
) : CommonResponseDto(brandId, outletId, terminalId, traceId, batchId, txRefId, transactionDate, trueyouId)
