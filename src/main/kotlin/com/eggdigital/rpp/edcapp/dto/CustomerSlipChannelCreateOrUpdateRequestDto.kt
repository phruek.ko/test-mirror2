package com.eggdigital.rpp.edcapp.dto

import com.eggdigital.rpp.edcapp.model.SlipChannel
import com.eggdigital.rpp.edcapp.validation.constraints.RequiredField
import com.fasterxml.jackson.annotation.JsonProperty

class CustomerSlipChannelCreateOrUpdateRequestDto : CommonRequestDto() {

    @RequiredField(message = "thai_id")
    @JsonProperty("thai_id")
    lateinit var thaiId: String

    @RequiredField(message = "channel")
    @JsonProperty("channel")
    lateinit var channel: SlipChannel

    @JsonProperty("value")
    var value: String? = ""

}