package com.eggdigital.rpp.edcapp.dto

import com.eggdigital.rpp.edcapp.domain.AccountType
import com.eggdigital.rpp.edcapp.validation.constraints.RequiredField
import com.fasterxml.jackson.annotation.JsonProperty

class CustomerInfoRequestDto : CommonRequestDto() {

    @RequiredField(message = "acc type")
    @JsonProperty("acc_type")
    lateinit var accType: AccountType

    @RequiredField(message = "acc value")
    @JsonProperty("acc_value")
    lateinit var accValue: String

}