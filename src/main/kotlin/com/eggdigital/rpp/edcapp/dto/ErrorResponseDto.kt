package com.eggdigital.rpp.edcapp.dto

import com.fasterxml.jackson.annotation.JsonProperty

data class ErrorResponseDto(var error: ErrorResponseDto.Erorr) {


    data class Erorr(var code: String,
                     var message: String,

                     @JsonProperty("display_code")
                     var displayCode: String,
                     var errors: MutableList<PropertyMessage> = mutableListOf()) {

        data class PropertyMessage(var property: String,
                                   var message: String)
    }


}