package com.eggdigital.rpp.edcapp.dto

import com.eggdigital.common.validation.EnumType
import com.eggdigital.rpp.edcapp.domain.AccountType
import com.eggdigital.rpp.edcapp.enumuration.CampaignType
import com.eggdigital.rpp.edcapp.enumuration.TransactionChannel
import com.eggdigital.rpp.edcapp.validation.MobileNumberField
import com.eggdigital.rpp.edcapp.validation.constraints.RequiredField
import com.fasterxml.jackson.annotation.JsonProperty

class RedeemByCampaignRequestDto : CommonRequestDto() {

    @RequiredField(field = "Account type", message = "{validation.required_field}")
    @EnumType(enumClass = AccountType::class, ignoreCase = true, message = "{error.accountType.invalid}")
    @JsonProperty("acc_type")
    lateinit var accType: String

    @RequiredField(field = "Account value", message = "{validation.required_field}")
    @JsonProperty("acc_value")
    lateinit var accValue: String

    @RequiredField(field = "Campaign code", message = "{validation.required_field}")
    @JsonProperty("campaign_code")
    lateinit var campaignCode: String

    @JsonProperty("transaction_channel")
    @EnumType(enumClass = TransactionChannel::class, ignoreCase = true, message = "{error.transactionChannel.invalid}")
    var transactionChannel: String = ""

    @JsonProperty("campaign_type")
    @EnumType(enumClass = CampaignType::class, ignoreCase = true, message = "{error.campaign_type.invalid}")
    var campaignType: String = CampaignType.CUSTOMER.toString()

    @MobileNumberField
    @JsonProperty("mobile")
    var mobile: String = ""
}