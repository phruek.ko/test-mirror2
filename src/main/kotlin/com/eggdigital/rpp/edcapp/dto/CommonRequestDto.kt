package com.eggdigital.rpp.edcapp.dto

import com.eggdigital.rpp.edcapp.validation.constraints.RequiredField
import com.fasterxml.jackson.annotation.JsonProperty

open class CommonRequestDto {

    @RequiredField(field = "brand id", message = "{validation.required_field}")
    @JsonProperty("brand_id")
    lateinit var brandId: String

    @RequiredField(field = "outlet id", message = "{validation.required_field}")
    @JsonProperty("outlet_id")
    lateinit var outletId: String

    @RequiredField(field = "terminal id", message = "{validation.required_field}")
    @JsonProperty("terminal_id")
    lateinit var terminalId: String

}