package com.eggdigital.rpp.edcapp.dto

import com.eggdigital.rpp.edcapp.model.CustomerSlipChannel

data class MessagingResponseDto(

        var customer: CustomerSlipChannel
)