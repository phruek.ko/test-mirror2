package com.eggdigital.rpp.edcapp.dto

import com.fasterxml.jackson.annotation.JsonProperty

data class CardInfoResponseDto(
        @JsonProperty("card_no")
        var cardNo: String,

        @JsonProperty("card_type")
        var cardType: String,

        @JsonProperty("card_status")
        var cardStatus: String,

        @JsonProperty("card_expired")
        var cardExpired: String,

        @JsonProperty("customer_name")
        var customerName: String,

        @JsonProperty("point_balance")
        var pointBalance: String,

        @JsonProperty("point_pockets")
        var pointPockets: List<PointPocket>,

        @JsonProperty("slip_type_id")
        var slipTypeId: String,

        @JsonProperty("slip_type_description")
        var slipTypeDescription: String
) {
    data class PointPocket(

            @JsonProperty("description")
            var description: String,

            @JsonProperty("balance")
            var balance: String,

            @JsonProperty("expired")
            var expired: String)
}