package com.eggdigital.rpp.edcapp.dto

import com.fasterxml.jackson.annotation.JsonProperty
import java.time.LocalDateTime

class PaymentChargeResponseDto(
        brandId: String,
        outletId: String,
        terminalId: String,
        traceId: String,
        batchId: String,
        txRefId: String,
        transactionDate: LocalDateTime,
        trueyouId: String,

        @JvmField
        @JsonProperty("amount")
        val amount: String,

        @JvmField
        @JsonProperty("payment_code")
        val paymentCode: String,

        @JvmField
        @JsonProperty("payment_method")
        val paymentMethod: String,

        @JvmField
        @JsonProperty("currency")
        val currency: String

) : CommonResponseDto(brandId,
        outletId,
        terminalId,
        traceId,
        batchId,
        txRefId,
        transactionDate,
        trueyouId)