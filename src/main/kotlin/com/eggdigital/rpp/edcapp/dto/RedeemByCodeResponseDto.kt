package com.eggdigital.rpp.edcapp.dto

import com.fasterxml.jackson.annotation.JsonProperty
import java.time.LocalDateTime

class RedeemByCodeResponseDto(
        brandId: String,
        outletId: String,
        terminalId: String,
        traceId: String,
        batchId: String,
        txRefId: String,
        transactionDate: LocalDateTime,
        trueyouId: String,

        @JsonProperty("reward_code")
        val rewardCode: String,

        @JsonProperty("campaign_name")
        val campaignName: String

) : CommonResponseDto(brandId,
        outletId,
        terminalId,
        traceId,
        batchId,
        txRefId,
        transactionDate,
        trueyouId)