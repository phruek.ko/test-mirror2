package com.eggdigital.rpp.edcapp.dto

import com.fasterxml.jackson.annotation.JsonProperty

class CustomerMobileNumbersResponseDto {

    @JsonProperty("mobile_numbers")
    var mobileNumbers: List<String> = mutableListOf()
}