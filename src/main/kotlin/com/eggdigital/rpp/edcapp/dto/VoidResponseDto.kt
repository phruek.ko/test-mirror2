package com.eggdigital.rpp.edcapp.dto

import com.fasterxml.jackson.annotation.JsonProperty
import java.time.LocalDateTime

class VoidResponseDto(
        brandId: String,
        outletId: String,
        terminalId: String,
        traceId: String,
        batchId: String,
        txRefId: String,
        transactionDate: LocalDateTime,
        trueyouId: String,

        @JvmField
        @JsonProperty("amount")
        val amount: String,

        @JvmField
        @JsonProperty("payment_code")
        val paymentCode: String,

        @JvmField
        @JsonProperty("currency")
        val currency: String,

        @JvmField
        @JsonProperty("payment_method")
        val paymentMethod: String,

        @JvmField
        @JsonProperty("transaction_type")
        var transactionType: String? = "",

        @JvmField
        @JsonProperty("point")
        var point: String? = "",

        @JvmField
        @JsonProperty("acc_type")
        var accType: String? = "",

        @JvmField
        @JsonProperty("acc_value")
        var accValue: String? = "",

        @JvmField
        @JsonProperty("campaign_name")
        var campaignName: String? = ""

) : CommonResponseDto(brandId,
        outletId,
        terminalId,
        traceId,
        batchId,
        txRefId,
        transactionDate,
        trueyouId)
