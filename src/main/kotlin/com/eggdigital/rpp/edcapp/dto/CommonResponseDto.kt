package com.eggdigital.rpp.edcapp.dto

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.PropertyNamingStrategy
import com.fasterxml.jackson.databind.annotation.JsonNaming

import java.time.LocalDateTime

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy::class)
open class CommonResponseDto(

        @JvmField
        @JsonProperty("brand_id")
        val brandId: String,

        @JvmField
        @JsonProperty("outlet_id")
        val outletId: String,

        @JvmField
        @JsonProperty("terminal_id")
        val terminalId: String,

        @JvmField
        @JsonProperty("trance_id")
        val traceId: String,

        @JvmField
        @JsonProperty("batch_id")
        val batchId: String,

        @JvmField
        @JsonProperty("tx_ref_id")
        val txRefId: String,

        @JvmField
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:MM:ss")
        @JsonProperty("transaction_date")
        val transactionDate: LocalDateTime,

        @JvmField
        @JsonProperty("trueyou_id")
        val trueyouId: String
)

