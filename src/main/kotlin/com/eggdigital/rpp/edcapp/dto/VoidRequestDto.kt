package com.eggdigital.rpp.edcapp.dto

import com.eggdigital.rpp.edcapp.validation.constraints.RequiredField
import com.fasterxml.jackson.annotation.JsonProperty

class VoidRequestDto(

        @RequiredField(field = "trace id")
        @JsonProperty("trace_id")
        val traceId: String) : CommonRequestDto()