package com.eggdigital.rpp.edcapp.dto

import com.eggdigital.rpp.edcapp.model.CustomerSlipChannel
import com.fasterxml.jackson.annotation.JsonProperty

data class CustomerSlipChannelResponseDto(

        @JsonProperty("first_time")
        var firstTime: Boolean,
        var customer: CustomerSlipChannel)