package com.eggdigital.rpp.edcapp.dto

import com.eggdigital.common.validation.EnumType
import com.eggdigital.rpp.edcapp.domain.PaymentMethod
import com.fasterxml.jackson.annotation.JsonProperty
import com.eggdigital.rpp.edcapp.enumuration.TransactionChannel
import org.hibernate.validator.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Pattern

class PaymentChargeRequestDto : CommonRequestDto() {

    @NotNull
    @field:Pattern(regexp = "\\d{1,9}", message = "incorrect amount format.")
    @JsonProperty("amount")
    lateinit var amount: String

    @NotNull()
    @JsonProperty("currency")
    lateinit var currency: String

    @NotNull
    @JsonProperty("payment_code")
    lateinit var paymentCode: String

    @NotNull
    @JsonProperty("payment_method")
    @NotBlank(message = "{error.paymentMethod.notblank}")
    @EnumType(enumClass = PaymentMethod::class, ignoreCase = true, message = "{error.paymentMethod.invalid}")
    lateinit var paymentMethod: String

    @JsonProperty("description")
    val description: String? = ""

    @JsonProperty("transaction_channel")
    @EnumType(enumClass = TransactionChannel::class, ignoreCase = true, message = "{error.transactionChannel.invalid}")
    var transactionChannel: String = ""
}