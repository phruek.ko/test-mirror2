package com.eggdigital.rpp.edcapp.dto

import com.eggdigital.rpp.edcapp.validation.constraints.RequiredField
import com.fasterxml.jackson.annotation.JsonProperty

class VoidV2RequestDto(

        @RequiredField(field = "trace id")
        @JsonProperty("trace_id")
        val traceId: String,

        @RequiredField(field = "tx ref id")
        @JsonProperty("tx_ref_id")
        val txRefId: String) : CommonRequestDto()