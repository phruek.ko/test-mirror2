package com.eggdigital.rpp.edcapp.dto

import com.eggdigital.rpp.edcapp.model.SlipChannel
import com.fasterxml.jackson.annotation.JsonProperty
import javax.validation.constraints.NotNull

class MessagingRequestDto : CommonRequestDto() {

    @NotNull()
    @JsonProperty("trace_id")
    lateinit var traceId: String

    @NotNull()
    @JsonProperty("channel")
    lateinit var channel: SlipChannel

    @NotNull()
    @JsonProperty("value")
    lateinit var value: String

}