package com.eggdigital.rpp.edcapp.config

enum class ApiErrorCodes(val httpStatusCode: Int, val code: String, val message: String) {
    /**
     * Client Errors
     */
    BAD_REQUEST(400, "400", "Bad request"),
    AUTHENTICATION_FAILURE(401, "401", "Authentication failure"),
    NOT_AUTHORIZED(403, "403", "Insufficient permissions"),
    RESOURCE_NOT_FOUND(404, "404", "The specified resource does not exist"),
    METHOD_NOT_SUPPORTED(405, "405", "The server does not implement the requested HTTP method"),
    MEDIA_TYPE_NOT_ACCEPTABLE(406, "406", "The server does not implement the media type that would be acceptable to the client"),
    CONFLICT(409, "409", "The request could not be processed because of conflict in the request"),
    UNSUPPORTED_MEDIA_TYPE(415, "415", "The server does not support the request payload’s media type"),
    UNPROCESSABLE_ENTITY(422, "422", "Process failed"),
    TOO_MANY_REQUEST(429, "429", "Too many requests. Blocked due to rate limiting"),
    /**
     * Server Errors
     */
    INTERNAL_SERVER_ERROR(500, "500", "An internal server error has occurred"),
    SERVICE_UNAVAILABLE(503, "503", "Service Unavailable")
}