package com.eggdigital.rpp.edcapp.config

import org.slf4j.MDC
import org.springframework.stereotype.Component
import java.util.*
import javax.servlet.*
import javax.servlet.http.HttpServletRequest

@Component
class RequestFilter : Filter {

    override fun doFilter(request: ServletRequest?, response: ServletResponse?, chain: FilterChain?) {
        var request: HttpServletRequest = request as HttpServletRequest
        var uuid = UUID.randomUUID().toString()
        MDC.put("uuid", uuid)
        chain!!.doFilter(request, response)
    }

    override fun destroy() {
    }

    override fun init(filterConfig: FilterConfig?) {
    }
}