package com.eggdigital.rpp.edcapp.repository;

import com.eggdigital.rpp.edcapp.domain.Outlet;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigInteger;

/**
 * Created by bombay on 10/20/2017 AD.
 */
public interface OutletRepository extends JpaRepository<Outlet, BigInteger> {

    Outlet findByOutletId(String outletId);

    Outlet findByOutletIdAndMerchantId(String outletId, String merchantId);
}
