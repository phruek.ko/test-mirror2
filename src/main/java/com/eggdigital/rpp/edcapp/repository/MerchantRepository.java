package com.eggdigital.rpp.edcapp.repository;

import com.eggdigital.rpp.edcapp.domain.Merchant;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Created by bombay on 10/20/2017 AD.
 */
public interface MerchantRepository extends JpaRepository<Merchant, Long> {

    Optional<Merchant> findByMerchantId(String merchantId);

}
