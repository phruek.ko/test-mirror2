package com.eggdigital.rpp.edcapp.repository;

import com.eggdigital.rpp.edcapp.domain.Status;
import com.eggdigital.rpp.edcapp.domain.Transaction;
import com.eggdigital.rpp.edcapp.domain.TransactionType;
import com.eggdigital.rpp.edcapp.enumuration.CampaignType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Created by bombay on 10/16/2017 AD.
 */
public interface TransactionRepository extends JpaRepository<Transaction, BigInteger>, JpaSpecificationExecutor<Transaction> {

    List<Transaction> findByBrandIdAndOutletIdAndTerminalIdAndTraceIdAndSettleStatusIsNotAndStatus(String brandId, String outletId, String terminalId, String traceId, Status status1, Status status2);

    List<Transaction> findByBrandIdAndOutletIdAndTerminalIdAndTraceIdAndRefIdAndSettleStatusIsNotAndStatus(String brandId, String outletId, String terminalId, String traceId, String txRefId, Status status1, Status status2);

    Optional<Transaction> findByBrandIdAndOutletIdAndTerminalIdAndTraceIdAndSettleStatusIsNot(String brandId, String outletId, String terminalId, String traceId, Status status);

    Optional<Transaction> findByBrandIdAndOutletIdAndTerminalIdAndTraceIdAndSettleStatusIsNotAndStatusAndTransactionType(String brandId, String outletId, String terminalId, String traceId, Status status1, Status status2, TransactionType transactionType);

    List<Transaction> findByBrandIdAndOutletIdAndTerminalIdAndSettleStatusIsNot(String brandId, String outletId, String terminalId, Status status);

    @Query(value = "select t.* from (select * from transaction where brand_id = ?1 and outlet_id = ?2 and terminal_id = ?3 and settle_status = ?4 order by trace_id desc ) t limit 1", nativeQuery = true)
    Optional<Transaction> findFirstByBrandIdAndOutletIdAndTerminalIdAndSettleStatusOrderByTraceIdDesc(String brandId, String outletId, String terminalId, String status);

    List<Transaction> findByBrandIdAndOutletIdAndTerminalIdAndTransactionTypeInAndStatusAndSettleStatus(String brandId, String outletId, String terminalId, List<TransactionType> transactionTypes, Status status, Status settleStatus);

    @Query(value = "select distinct (campaign_code) as campaign_code from transaction where brand_id = ?1 and outlet_id = ?2 and terminal_id = ?3 and ( transaction_type = ?4 or transaction_type = ?5) and status = ?6 and settle_status = ?7 and campaign_code not like ?8 and campaign_code not like ?9 and campaign_type <> 'MERCHANT'", nativeQuery = true)
    List<String> getDistinctCampaignCodeByBrandIdAndOutletIdAndTerminalIdAndTransactionTypeAndStatusAndSettleStatus(String brandId, String outletId, String terminalId, String transactionType, String transactionType2, String status, String settleStatus, String campaignCode1, String campaignCode2);

    @Query(value = "select distinct (campaign_code) as campaign_code from transaction where brand_id = ?1 and outlet_id = ?2 and terminal_id = ?3 and ( transaction_type = ?4 or transaction_type = ?5) and status = ?6 and settle_status = ?7 and campaign_code like ?8 and campaign_type <> 'MERCHANT'", nativeQuery = true)
    List<String> getTransactionByBrandIdOutletIdTerminalIdSettleStatusPendingCampaign(String brandId, String outletId, String terminalId, String transactionType, String transactionType2, String status, String settleStatus, String campaignCode);

    @Query(value = "select ifnull(count(campaign_code), 0) as campaign_code from transaction where brand_id = ?1 and outlet_id = ?2 and terminal_id = ?3 and ( transaction_type = ?4 or transaction_type = ?5) and status = ?6 and settle_status = ?7 and campaign_code =?8 and campaign_type <> 'MERCHANT'", nativeQuery = true)
    int getCountCampaignCodeByBrandIdAndOutletIdAndTerminalIdAndTransactionTypeAndStatusAndSettleStatus(String brandId, String outletId, String terminalId, String transactionType, String transactionType2, String status, String settleStatus, String CAMPAIGN_CODE);

    @Query(value = "select ifnull(count(campaign_code), 0) as campaign_code from transaction where brand_id = ?1 and outlet_id = ?2 and terminal_id = ?3 and ( transaction_type = ?4 or transaction_type = ?5) and status = ?6 and settle_status = ?7 and campaign_code like ?8 and campaign_type <> 'MERCHANT'", nativeQuery = true)
    int getCountCampaignCodeEstampByBrandIdAndOutletIdAndTerminalIdAndTransactionTypeAndStatusAndSettleStatus(String brandId, String outletId, String terminalId, String transactionType, String transactionType2, String status, String settleStatus, String campaignCode);

    @Query(value = "select * from transaction where brand_id = ?1 and outlet_id = ?2 and terminal_id = ?3 and status = ?4 and transaction_type in('payment_charge', 'point_earn') and date(create_date)  = ?5", nativeQuery = true)
    List<Transaction> findByBrandIdAndOutletIdAndTerminalIdAndStatusAndTransactionTypeInAndCreateDate(String brandId, String outletId, String terminalId, String status, String transactionDate);

    @Query(value = "select distinct (campaign_code) as campaign_code from transaction where brand_id = ?1 and outlet_id = ?2 and terminal_id = ?3 and (transaction_type = ?4 or transaction_type = ?5) and status = ?6 and date(create_date)  = ?7 and campaign_type <> 'MERCHANT'", nativeQuery = true)
    List<String> getDistinctCampaignCodeByBrandIdAndOutletIdAndTerminalIdAndTransactionTypeAndStatusAndCreateDate(String brandId, String outletId, String terminalId, String transactionTypeRedeem, String transactionTypeMarkUse, String status, String transactionDate);

    @Query(value = "select distinct (campaign_code) as campaign_code from transaction where brand_id = ?1 and outlet_id = ?2 and terminal_id = ?3 and (transaction_type = ?4 or transaction_type = ?5) and status = ?6 and date(create_date)  = ?7 and campaign_code not like ?8 and campaign_code not like ?9 and campaign_type <> 'MERCHANT'", nativeQuery = true)
    List<String> getDistinctCampaignCodeByBrandIdAndOutletIdAndTerminalIdAndTransactionTypeAndStatusAndCreateDateNotInCampaignCode(String brandId, String outletId, String terminalId, String transactionTypeRedeem, String transactionTypeMarkUse, String status, String transactionDate, String campaignCode1, String campaignCode2);

    @Query(value = "select distinct (campaign_code) as campaign_code from transaction where brand_id = ?1 and outlet_id = ?2 and terminal_id = ?3 and (transaction_type = ?4 or transaction_type = ?5) and status = ?6 and date(create_date)  = ?7 and campaign_code like ?8 and campaign_type <> 'MERCHANT'", nativeQuery = true)
    List<String> getCampaignByBrandIdAndOutletIdAndTerminalIdAndTransactionTypeAndStatusAndCreateDateCampaignCode(String brandId, String outletId, String terminalId, String transactionTypeRedeem, String transactionTypeMarkUse, String status, String transactionDate, String campaignCode);

    @Query(value = "select ifnull(count(campaign_code), 0) as campaign_code from transaction where brand_id = ?1 and outlet_id = ?2 and terminal_id = ?3 and (transaction_type = ?4 or transaction_type = ?5) and status = ?6 and campaign_code =?7 and date(create_date)  = ?8 and campaign_type <> 'MERCHANT'", nativeQuery = true)
    int getCountCampaignCodeByBrandIdAndOutletIdAndTerminalIdAndTransactionTypeAndStatusAndCamapignCodeAndCreateDate(String brandId, String outletId, String terminalId, String transactionTypeRedeem, String transactionTypeMarkUse, String status, String campaignCode, String transactionDate);

    @Query(value = "select ifnull(sum(point), 0) as point from transaction where brand_id = ?1 and outlet_id = ?2 and terminal_id = ?3 and (transaction_type = ?4 or transaction_type = ?5) and status = ?6  and date(create_date)  = ?7 and campaign_type <> 'MERCHANT'", nativeQuery = true)
    int getSumPointRedeemByBrandIdAndOutletIdAndTerminalIdAndTransactionTypeAndStatusAndCreateDate(String brandId, String outletId, String terminalId, String transactionTypeRedeem, String transactionTypeMarkUse, String status, String transactionDate);

    @Query(value = "select ifnull(count(coupon_code), 0) as coupon_code from transaction where brand_id = ?1 and outlet_id = ?2 and terminal_id = ?3 and (transaction_type = ?4 or transaction_type = ?5) and status = ?6 and campaign_code like ?7 and date(create_date)  = ?8 and coupon_code like ?9 and campaign_type <> 'MERCHANT'", nativeQuery = true)
    int getCountCouponCodeByBrandIdAndOutletIdAndTerminalIdAndTransactionTypeAndStatusAndCamapignCodeAndCreateDate(String brandId, String outletId, String terminalId, String transactionTypeRedeem, String transactionTypeMarkUse, String status, String campaignCode, String transactionDate, String couponCode);

    @Query(value = "select ifnull(count(coupon_code), 0) as campaign_code from transaction where brand_id = ?1 and outlet_id = ?2 and terminal_id = ?3 and ( transaction_type = ?4 or transaction_type = ?5) and status = ?6 and settle_status = ?7 and campaign_code like ?8 and coupon_code like ?9 and campaign_type <> 'MERCHANT'", nativeQuery = true)
    int getCountCouponCodeEstampByBrandIdAndOutletIdAndTerminalIdAndTransactionTypeAndStatusAndSettleStatus(String brandId, String outletId, String terminalId, String transactionType, String transactionType2, String status, String settleStatus, String campaignCode, String couponCode);

    List<Transaction> findBySettleStatus(Status status);

    List<Transaction> findByBrandIdAndSettleStatus(String brandId, Status status);

    List<Transaction> findByBrandIdAndOutletIdAndTerminalIdAndStatusAndTransactionTypeInAndCreateDateBetweenAndCampaignCodeNotLikeAndCampaignCodeNotLikeAndCampaignTypeIsNot(String brandId, String outletId, String terminalId, Status status, List<TransactionType> transactionTypes, Date transactionDateStart, Date transactionDateEnd, String campaginCode1, String campaignCode2, CampaignType campaignType);

    List<Transaction> findByBrandIdAndOutletIdAndTerminalIdAndStatusAndTransactionTypeInAndCreateDateBetween(String brandId, String outletId, String terminalId, Status status, List<TransactionType> transactionTypes, Date transactionDateStart, Date transactionDateEnd);

    List<Transaction> findByBrandIdAndOutletIdAndTerminalIdAndStatusAndTransactionTypeInAndCreateDateBetweenAndCampaignCodeLikeAndCampaignTypeIsNot(String brandId, String outletId, String terminalId, Status status, List<TransactionType> transactionTypes, Date transactionDateStart, Date transactionDateEnd, String campaginCode, CampaignType campaignType);

    List<Transaction> findByBrandIdAndOutletIdAndTerminalIdAndStatusAndTransactionTypeInAndCreateDateBetweenAndCampaignCodeLikeAndCouponCodeLikeAndCampaignTypeIsNot(String brandId, String outletId, String terminalId, Status status, List<TransactionType> transactionTypes, Date transactionDateStart, Date transactionDateEnd, String campaginCode, String couponCode, CampaignType campaignType);

    @Query(value = "select distinct (brand_id) as brand_id from transaction where status = ?1", nativeQuery = true)
    List<String> findBrandIdDistinctByStatus(String status);
}
