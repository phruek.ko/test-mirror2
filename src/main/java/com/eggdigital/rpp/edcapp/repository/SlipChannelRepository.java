package com.eggdigital.rpp.edcapp.repository;

import com.eggdigital.rpp.edcapp.domain.SlipChannel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SlipChannelRepository extends JpaRepository<SlipChannel, String> {
}
