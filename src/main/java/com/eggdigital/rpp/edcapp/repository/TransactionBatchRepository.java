package com.eggdigital.rpp.edcapp.repository;

import com.eggdigital.rpp.edcapp.domain.Status;
import com.eggdigital.rpp.edcapp.domain.TransactionBatch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

/**
 * Created by bombay on 10/17/2017 AD.
 */
public interface TransactionBatchRepository extends JpaRepository<TransactionBatch, BigInteger> {

    @Query(value = "select t.* from (select * from transaction_batch where brand_id = ?1 and outlet_id = ?2 and terminal_id = ?3 and status = ?4 order by trace_id desc ) t limit 1", nativeQuery = true)
    Optional<TransactionBatch> findFirstByBrandIdAndOutletIdAndTerminalIdAndStatusOrderByTraceIdDesc(String brandId, String outletId, String terminalId, String status);

    @Query(value = "select t.* from (select * from transaction_batch where brand_id = ?1 and outlet_id = ?2 and terminal_id = ?3 and status = ?4 order by batch_id desc ) t limit 1", nativeQuery = true)
    Optional<TransactionBatch> findFirstByBrandIdAndOutletIdAndTerminalIdAndStatusOrderByBatchIdDesc(String brandId, String outletId, String terminalId, String status);

    List<TransactionBatch> findByBrandIdAndOutletIdAndTerminalIdAndStatus(String brandId, String outletId, String terminalId, Status status);

    List<TransactionBatch> findByStatus(Status status);

    List<TransactionBatch> findByBrandIdAndStatus(String brandId, Status status);
}
