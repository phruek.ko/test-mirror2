package com.eggdigital.rpp.edcapp;

import com.eggdigital.rpp.edcapp.config.DefaultProfileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;

@SpringBootApplication
public class Application {

    private static final Logger log = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) throws UnknownHostException {
        SpringApplication app = new SpringApplication(Application.class);
        DefaultProfileUtils.addDefaultProfile(app);
        Environment env = app.run(args).getEnvironment();
        log.info("\n----------------------------------------------------------\n\t" +
                        "Application: \t{}\n\t" +
                        "Version: \t{}\n\t" +
                        "Build Timestamp: {}\n\t" +
                        "Profiles: \t{}\n\t" +
                        "Local URL: \thttp://localhost:{}{}\n\t" +
                        "External URL: \thttp://{}:{}{}\n" +
                        "----------------------------------------------------------",
                env.getProperty("app.artifact.id"),
                env.getProperty("app.version"),
                env.getProperty("app.build.timestamp"),
                Arrays.toString(env.getActiveProfiles()),
                env.getProperty("server.port", "8080"),
                env.getProperty("server.context-path", "/"),
                InetAddress.getLocalHost().getHostAddress(),
                env.getProperty("server.port", "8080"),
                env.getProperty("server.context-path", "/")
        );
    }
}
