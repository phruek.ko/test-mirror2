package com.eggdigital.rpp.edcapp.merchant.dto;

import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Created by bombay on 16/5/2018 AD.
 */
public class MerchantSearchDto implements Serializable {

    public MerchantSearchDto() {
    }

    private Long id;

    private String merchantId;

    private String trueyouId;

    private String tmnMerchantId;

    private String categoryId;

    private String categoryName;

    private Long storeTypeId;

    private String storeTypeName;

    private String storeNameTh;

    private String storeNameEn;

    private String thaiId;

    private Long titleId;

    private String titleName;

    private String titleOtherName;

    private String firstName;

    private String lastName;

    private String contactNo;

    private String storeContactNo1;

    private String storeContactNo2;

    private String storeWebsite;

    private String storeFacebook;

    private String storeLineAt;

    private String storeTwitter;

    private String storeInstagram;

    private String email;

    private String lineId;

    private String lineAt;

    private String refId;

    private String taxId;

    private String deviceType;

    private String serviceType;

    private Status status;

    private int size;

//    private CampaignGroupDTO merchantGroup;

    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    private LocalDateTime createDateStart;

    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    private LocalDateTime createDateEnd;

    private String createBy;

    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    private LocalDateTime modifyDateStart;

    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    private LocalDateTime modifyDateEnd;

    private String modifyBy;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getTrueyouId() {
        return trueyouId;
    }

    public void setTrueyouId(String trueyouId) {
        this.trueyouId = trueyouId;
    }

    public String getTmnMerchantId() {
        return tmnMerchantId;
    }

    public void setTmnMerchantId(String tmnMerchantId) {
        this.tmnMerchantId = tmnMerchantId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Long getStoreTypeId() {
        return storeTypeId;
    }

    public void setStoreTypeId(Long storeTypeId) {
        this.storeTypeId = storeTypeId;
    }

    public String getStoreTypeName() {
        return storeTypeName;
    }

    public void setStoreTypeName(String storeTypeName) {
        this.storeTypeName = storeTypeName;
    }

    public String getStoreNameTh() {
        return storeNameTh;
    }

    public void setStoreNameTh(String storeNameTh) {
        this.storeNameTh = storeNameTh;
    }

    public String getStoreNameEn() {
        return storeNameEn;
    }

    public void setStoreNameEn(String storeNameEn) {
        this.storeNameEn = storeNameEn;
    }

    public String getThaiId() {
        return thaiId;
    }

    public void setThaiId(String thaiId) {
        this.thaiId = thaiId;
    }

    public Long getTitleId() {
        return titleId;
    }

    public void setTitleId(Long titleId) {
        this.titleId = titleId;
    }

    public String getTitleName() {
        return titleName;
    }

    public void setTitleName(String titleName) {
        this.titleName = titleName;
    }

    public String getTitleOtherName() {
        return titleOtherName;
    }

    public void setTitleOtherName(String titleOtherName) {
        this.titleOtherName = titleOtherName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getStoreContactNo1() {
        return storeContactNo1;
    }

    public void setStoreContactNo1(String storeContactNo1) {
        this.storeContactNo1 = storeContactNo1;
    }

    public String getStoreContactNo2() {
        return storeContactNo2;
    }

    public void setStoreContactNo2(String storeContactNo2) {
        this.storeContactNo2 = storeContactNo2;
    }

    public String getStoreWebsite() {
        return storeWebsite;
    }

    public void setStoreWebsite(String storeWebsite) {
        this.storeWebsite = storeWebsite;
    }

    public String getStoreFacebook() {
        return storeFacebook;
    }

    public void setStoreFacebook(String storeFacebook) {
        this.storeFacebook = storeFacebook;
    }

    public String getStoreLineAt() {
        return storeLineAt;
    }

    public void setStoreLineAt(String storeLineAt) {
        this.storeLineAt = storeLineAt;
    }

    public String getStoreTwitter() {
        return storeTwitter;
    }

    public void setStoreTwitter(String storeTwitter) {
        this.storeTwitter = storeTwitter;
    }

    public String getStoreInstagram() {
        return storeInstagram;
    }

    public void setStoreInstagram(String storeInstagram) {
        this.storeInstagram = storeInstagram;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLineId() {
        return lineId;
    }

    public void setLineId(String lineId) {
        this.lineId = lineId;
    }

    public String getLineAt() {
        return lineAt;
    }

    public void setLineAt(String lineAt) {
        this.lineAt = lineAt;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getTaxId() {
        return taxId;
    }

    public void setTaxId(String taxId) {
        this.taxId = taxId;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public LocalDateTime getModifyDateStart() {
        return modifyDateStart;
    }

    public void setModifyDateStart(LocalDateTime modifyDateStart) {
        this.modifyDateStart = modifyDateStart;
    }

    public LocalDateTime getModifyDateEnd() {
        return modifyDateEnd;
    }

    public void setModifyDateEnd(LocalDateTime modifyDateEnd) {
        this.modifyDateEnd = modifyDateEnd;
    }

    public LocalDateTime getCreateDateStart() {
        return createDateStart;
    }

    public void setCreateDateStart(LocalDateTime createDateStart) {
        this.createDateStart = createDateStart;
    }

    public LocalDateTime getCreateDateEnd() {
        return createDateEnd;
    }

    public void setCreateDateEnd(LocalDateTime createDateEnd) {
        this.createDateEnd = createDateEnd;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getModifyBy() {
        return modifyBy;
    }

    public void setModifyBy(String modifyBy) {
        this.modifyBy = modifyBy;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
