package com.eggdigital.rpp.edcapp.merchant.dto;

/**
 * Created by bombay on 6/14/2017 AD.
 */
public enum Status {
    WAITING, SUCCESS, FAIL, REVERSE, APPROVE, DRAFT, CLOSE, ACTIVATE, AVAILABLE, DELETE, REJECT
}
