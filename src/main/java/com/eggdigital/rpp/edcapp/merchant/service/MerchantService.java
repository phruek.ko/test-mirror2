package com.eggdigital.rpp.edcapp.merchant.service;

import com.eggdigital.rpp.edcapp.domain.Merchant;
import com.eggdigital.rpp.edcapp.merchant.dto.MerchantContentReponseDto;
import com.eggdigital.rpp.edcapp.merchant.dto.MerchantSearchDto;
import com.eggdigital.rpp.edcapp.merchant.dto.OutletContentReponseDto;
import com.eggdigital.rpp.edcapp.merchant.dto.OutletSearchDto;

import java.io.IOException;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;


public interface MerchantService {

    MerchantContentReponseDto searchMerchant(MerchantSearchDto merchantSearchDto) throws IOException;

    MerchantContentReponseDto searchMerchantByMerchantId(String  merchantId) throws IOException;

    Optional<Merchant> findByMerchantId(String merchantId) throws IOException;

    CompletableFuture<OutletContentReponseDto> searchOutlet(OutletSearchDto outletSearchDto) throws IOException;

    void saveMerchantFromMasterMerchant(Merchant merchant);

}
