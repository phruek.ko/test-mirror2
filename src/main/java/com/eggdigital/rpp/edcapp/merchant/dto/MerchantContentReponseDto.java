package com.eggdigital.rpp.edcapp.merchant.dto;

import java.util.List;

/**
 * Created by Patthara on 6/14/2018 AD.
 */
public class MerchantContentReponseDto {

    private List<MerchantReponseDto> content;

    public List<MerchantReponseDto> getContent() {
        return content;
    }

    public void setContent(List<MerchantReponseDto> content) {
        this.content = content;
    }
}
