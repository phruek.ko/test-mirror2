package com.eggdigital.rpp.edcapp.merchant.service.impl;

import com.eggdigital.rpp.edcapp.api.model.ApiStatus;
import com.eggdigital.rpp.edcapp.domain.Merchant;
import com.eggdigital.rpp.edcapp.exception.CustomThirdPartyExcepton;
import com.eggdigital.rpp.edcapp.merchant.dto.MerchantContentReponseDto;
import com.eggdigital.rpp.edcapp.merchant.dto.MerchantSearchDto;
import com.eggdigital.rpp.edcapp.merchant.dto.OutletContentReponseDto;
import com.eggdigital.rpp.edcapp.merchant.dto.OutletSearchDto;
import com.eggdigital.rpp.edcapp.merchant.service.MerchantService;
import com.eggdigital.rpp.edcapp.o2o.service.O2oService;
import com.eggdigital.rpp.edcapp.repository.MerchantRepository;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

@Service
public class MerchantServiceImpl implements MerchantService {

    private final Logger log = LoggerFactory.getLogger(MerchantServiceImpl.class);

    @Autowired
    private O2oService o2oService;

    private final RestTemplate restTemplate;

    @Value("${ms.master.merchant.merchantSearch}")
    private String urlMerchantSearch;

    @Value("${ms.master.merchant.outletSearch}")
    private String urlOutletSearch;

    private final MerchantRepository merchantRepository;

    public MerchantServiceImpl(RestTemplate restTemplate, MerchantRepository merchantRepository) {
        this.restTemplate = restTemplate;
        this.merchantRepository = merchantRepository;
    }

    @Override
    public MerchantContentReponseDto searchMerchant(MerchantSearchDto merchantSearchDto) throws IOException {
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(urlMerchantSearch).queryParam("merchantId", merchantSearchDto.getMerchantId());
        builder.queryParam("size", merchantSearchDto.getSize());
        String url = URLDecoder.decode(builder.toUriString(), "UTF-8");
        String token = o2oService.getToken();
        MultiValueMap<String, String> headers = getRequestHeader(token);
        MerchantContentReponseDto merchantContentReponseDto = restTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(headers), MerchantContentReponseDto.class).getBody();
        if (merchantContentReponseDto == null) {
            throw new CustomThirdPartyExcepton(new ApiStatus("0800", "No Response"));
        }
        return merchantContentReponseDto;
    }

    @Override
    public MerchantContentReponseDto searchMerchantByMerchantId(String merchantId) throws IOException {
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(urlMerchantSearch).queryParam("merchantId", merchantId);
        String url = URLDecoder.decode(builder.toUriString(), "UTF-8");
        String token = o2oService.getToken();
        MultiValueMap<String, String> headers = getRequestHeader(token);
        MerchantContentReponseDto merchantContentReponseDto = restTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(headers), MerchantContentReponseDto.class).getBody();
        if (merchantContentReponseDto == null) {
            throw new CustomThirdPartyExcepton(new ApiStatus("0800", "No Response"));
        }
        return merchantContentReponseDto;
    }

    @Override
    public CompletableFuture<OutletContentReponseDto> searchOutlet(OutletSearchDto outletSearchDto) throws IOException {
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(urlOutletSearch).queryParam("merchantId", outletSearchDto.getMerchantId());
        builder.queryParam("size", outletSearchDto.getSize());
        String url = URLDecoder.decode(builder.toUriString(), "UTF-8");
        String token = o2oService.getToken();
        MultiValueMap<String, String> headers = getRequestHeader(token);
        OutletContentReponseDto completableFuture = restTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(headers), OutletContentReponseDto.class).getBody();
        if (completableFuture == null) {
            throw new CustomThirdPartyExcepton(new ApiStatus("0800", "No Response"));
        }
        return CompletableFuture.completedFuture(completableFuture);
    }

    @NotNull
    private MultiValueMap<String, String> getRequestHeader(String token) {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
        headers.add(HttpHeaders.AUTHORIZATION, "bearer" + token);
        return headers;
    }

    @Override
    public Optional<Merchant> findByMerchantId(String merchantId) throws IOException {
        MerchantContentReponseDto merchantResult = this.searchMerchantByMerchantId(merchantId);
        return merchantResult.getContent()
                .stream().map(m -> new Merchant(m.getStoreNameTh(), m.getStoreNameEn(), m.getMerchantId(), m.getTrueyouId(), m.getTmnApiKey(), m.getTmnMerchantId()))
                .findFirst();
    }

    @Override
    public void saveMerchantFromMasterMerchant(Merchant merchant) {
        CompletableFuture<Void> future = CompletableFuture.runAsync(() -> {
            // Simulate a long-running Job
            //                TimeUnit.SECONDS.sleep(5);
            saveMerchant(merchant);
        });
    }

    private void saveMerchant(Merchant merchant) {
        log.info("Saving : {}", merchant.getMerchantId());
        Merchant merchantToSave = new Merchant();
        merchantToSave.setMerchantId(merchant.getMerchantId());
        merchantToSave.setTrueyouId(merchant.getTrueyouId());
        merchantToSave.setTmnMerchantId(merchant.getTmnMerchantId());
        merchantToSave.setTmnApiKey(merchant.getTmnApiKey());
        merchantToSave.setStoreNameTh(merchant.getStoreNameTh());
        merchantToSave.setStoreNameEn(merchant.getStoreNameEn());
        merchantRepository.save(merchantToSave);
    }
}
