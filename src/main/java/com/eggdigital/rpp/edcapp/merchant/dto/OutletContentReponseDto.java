package com.eggdigital.rpp.edcapp.merchant.dto;

import java.util.List;

/**
 * Created by Patthara on 6/14/2018 AD.
 */
public class OutletContentReponseDto {

    private List<OutletReponseDto> content;

    public List<OutletReponseDto> getContent() {
        return content;
    }

    public void setContent(List<OutletReponseDto> content) {
        this.content = content;
    }
}
