package com.eggdigital.rpp.edcapp.merchant.service.impl;

import com.eggdigital.rpp.edcapp.domain.Merchant;
import com.eggdigital.rpp.edcapp.domain.Outlet;
import com.eggdigital.rpp.edcapp.domain.Status;
import com.eggdigital.rpp.edcapp.merchant.dto.MerchantContentReponseDto;
import com.eggdigital.rpp.edcapp.merchant.dto.MerchantSearchDto;
import com.eggdigital.rpp.edcapp.merchant.dto.OutletContentReponseDto;
import com.eggdigital.rpp.edcapp.merchant.dto.OutletSearchDto;
import com.eggdigital.rpp.edcapp.merchant.service.MerchantService;
import com.eggdigital.rpp.edcapp.repository.MerchantRepository;
import com.eggdigital.rpp.edcapp.repository.OutletRepository;
import com.eggdigital.rpp.edcapp.repository.TransactionRepository;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

@Component
public class MerchantScheduledTasks {

    private final Logger log = LoggerFactory.getLogger(MerchantScheduledTasks.class);

    private final MerchantService merchantService;

    private final TransactionRepository transactionRepository;
    private final MerchantRepository merchantRepository;
    private final OutletRepository outletRepository;

    @Autowired
    public MerchantScheduledTasks(TransactionRepository transactionRepository, MerchantRepository merchantRepository, OutletRepository outletRepository, MerchantService merchantService) {
        this.transactionRepository = transactionRepository;
        this.merchantRepository = merchantRepository;
        this.outletRepository = outletRepository;
        this.merchantService = merchantService;
    }

    public void syncMerchantFromMerchantToEdc() {
        try {
            MerchantSearchDto merchantSearchDto = new MerchantSearchDto();
            merchantSearchDto.setMerchantId(getBrandId());
            merchantSearchDto.setSize(Integer.MAX_VALUE);
            MerchantContentReponseDto result = merchantService.searchMerchant(merchantSearchDto);
            List<Merchant> merchants = result.getContent().stream()
                    .map(m -> new Merchant(m.getStoreNameTh(), m.getStoreNameEn(), m.getMerchantId(), m.getTrueyouId(), m.getTmnApiKey(), m.getTmnMerchantId())).collect(Collectors.toList());
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            log.debug("sync merchant to EDC      start : {}", simpleDateFormat.format(new Date()));
            saveMerchant(merchants);
            log.debug("sync merchant to EDC      end : {}", simpleDateFormat.format(new Date()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void syncOutletFromMerchantToEdc() throws IOException, ExecutionException, InterruptedException {
        OutletSearchDto outletSearchDto = new OutletSearchDto();
        outletSearchDto.setMerchantId(getBrandId());
        outletSearchDto.setSize(Integer.MAX_VALUE);
        CompletableFuture<OutletContentReponseDto> result = merchantService.searchOutlet(outletSearchDto);
        List<Outlet> outlets = result.get().getContent().stream()
                .map(o -> new Outlet(o.getOutletId(), o.getOutletNameTh(), o.getOutletNameEn(), o.getMerchantId())).collect(Collectors.toList());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        log.debug("sync outlet to EDC      start : {}", simpleDateFormat.format(new Date()));
        saveOutlet(outlets);
        log.debug("sync outlet to EDC      end : {}", simpleDateFormat.format(new Date()));

    }

    private void saveOutlet(List<Outlet> outlets) {
        List<Outlet> outletEntityToUpdate = new ArrayList<>();
        List<Outlet> outletEntityToInsert = new ArrayList<>();
        for (Outlet outlet : outlets) {
            Outlet o = outletRepository.findByOutletIdAndMerchantId(outlet.getOutletId(), outlet.getMerchantId());
            if (o != null) {
                o.setOutletId(outlet.getOutletId());
                o.setOutletNameEn(outlet.getOutletNameEn());
                o.setOutletNameTh(outlet.getOutletNameTh());
                o.setMerchantId(outlet.getMerchantId());
                outletEntityToUpdate.add(o);
            } else {
                outletEntityToInsert.add(outlet);
            }
        }
        outletRepository.save(outletEntityToUpdate);
        outletRepository.save(outletEntityToInsert);

    }

    private void saveMerchant(List<Merchant> merchants) {
        List<Merchant> merchantEntityToUpdate = new ArrayList<>();
        List<Merchant> merchantEntityToInsert = new ArrayList<>();
        merchants.forEach(merchant -> {
            Optional<Merchant> merchantOptional = merchantRepository.findByMerchantId(merchant.getMerchantId());
            if (merchantOptional.isPresent()) {
                Merchant m = new Merchant();
                m.setId(merchantOptional.get().getId());
                m.setStoreNameEn(merchant.getStoreNameEn());
                m.setStoreNameTh(merchant.getStoreNameTh());
                m.setTmnApiKey(merchant.getTmnApiKey());
                m.setTmnMerchantId(merchant.getTmnMerchantId());
                m.setTrueyouId(merchant.getTrueyouId());
                m.setMerchantId(merchant.getMerchantId());
                merchantEntityToUpdate.add(m);
            } else {
                merchantEntityToInsert.add(merchant);
            }
        });
        merchantRepository.save(merchantEntityToInsert);
        merchantRepository.save(merchantEntityToUpdate);

    }

    @NotNull
    private String getBrandId() {
        List<String> brandId = transactionRepository.findBrandIdDistinctByStatus(String.valueOf(Status.SUCCESS));
        return Arrays.toString(brandId.toArray()).replace("[", "").replace("]", "");
    }

}
