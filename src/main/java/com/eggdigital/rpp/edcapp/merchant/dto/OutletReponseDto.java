package com.eggdigital.rpp.edcapp.merchant.dto;

import java.math.BigInteger;

/**
 * Created by Patthara on 6/14/2018 AD.
 */
public class OutletReponseDto {

    private BigInteger id;

    /**
     * OUTLET_ID
     */
    private String outletId;

    /**
     * ชื่อร้านภาษาไทย (TH)
     */
    private String outletNameTh;
    /**
     * ชื่อร้านภาษาอังกฤษ (EN)
     */
    private String outletNameEn;

    private String merchantId;

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public String getOutletId() {
        return outletId;
    }

    public void setOutletId(String outletId) {
        this.outletId = outletId;
    }

    public String getOutletNameTh() {
        return outletNameTh;
    }

    public void setOutletNameTh(String outletNameTh) {
        this.outletNameTh = outletNameTh;
    }

    public String getOutletNameEn() {
        return outletNameEn;
    }

    public void setOutletNameEn(String outletNameEn) {
        this.outletNameEn = outletNameEn;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }
}
