package com.eggdigital.rpp.edcapp.merchant.dto;

import java.util.Collections;
import java.util.Set;

public class MerchantReponseDto {

    private Long id;

    private String merchantId;

    private String trueyouId;

    private String tmnMerchantId;

    private String tmnApiKey;

    private String businessType;

    private String categoryId;

    private String categoryName;

    private Long storeTypeId;

    private String storeTypeName;

    private String registerChannel;

    private String storeNameTh;

    private String storeNameEn;

    private String thaiId;

    private Long titleId;

    private String titleName;

    private String firstName;

    private String lastName;

    private String contactNo;

    private String storeDetail;

    private String storeContactNo1;

    private String storeContactNo2;

    private String storeWebsite;

    private String storeFacebook;

    private String storeLineAt;

    private String storeTwitter;

    private String storeInstagram;

    private String email;

    private String lineId;

    private String lineAt;

    private String refId;

    private String titleOtherName;

    private String taxId;

    private String deviceType;

    private String status;

    private Set<String> serviceTypes = Collections.emptySet();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getTrueyouId() {
        return trueyouId;
    }

    public void setTrueyouId(String trueyouId) {
        this.trueyouId = trueyouId;
    }

    public String getTmnMerchantId() {
        return tmnMerchantId;
    }

    public void setTmnMerchantId(String tmnMerchantId) {
        this.tmnMerchantId = tmnMerchantId;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Long getStoreTypeId() {
        return storeTypeId;
    }

    public void setStoreTypeId(Long storeTypeId) {
        this.storeTypeId = storeTypeId;
    }

    public String getStoreTypeName() {
        return storeTypeName;
    }

    public void setStoreTypeName(String storeTypeName) {
        this.storeTypeName = storeTypeName;
    }

    public String getRegisterChannel() {
        return registerChannel;
    }

    public void setRegisterChannel(String registerChannel) {
        this.registerChannel = registerChannel;
    }

    public String getStoreNameTh() {
        return storeNameTh;
    }

    public void setStoreNameTh(String storeNameTh) {
        this.storeNameTh = storeNameTh;
    }

    public String getStoreNameEn() {
        return storeNameEn;
    }

    public void setStoreNameEn(String storeNameEn) {
        this.storeNameEn = storeNameEn;
    }

    public String getThaiId() {
        return thaiId;
    }

    public void setThaiId(String thaiId) {
        this.thaiId = thaiId;
    }

    public Long getTitleId() {
        return titleId;
    }

    public void setTitleId(Long titleId) {
        this.titleId = titleId;
    }

    public String getTitleName() {
        return titleName;
    }

    public void setTitleName(String titleName) {
        this.titleName = titleName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getStoreDetail() {
        return storeDetail;
    }

    public void setStoreDetail(String storeDetail) {
        this.storeDetail = storeDetail;
    }

    public String getStoreContactNo1() {
        return storeContactNo1;
    }

    public void setStoreContactNo1(String storeContactNo1) {
        this.storeContactNo1 = storeContactNo1;
    }

    public String getStoreContactNo2() {
        return storeContactNo2;
    }

    public void setStoreContactNo2(String storeContactNo2) {
        this.storeContactNo2 = storeContactNo2;
    }

    public String getStoreWebsite() {
        return storeWebsite;
    }

    public void setStoreWebsite(String storeWebsite) {
        this.storeWebsite = storeWebsite;
    }

    public String getStoreFacebook() {
        return storeFacebook;
    }

    public void setStoreFacebook(String storeFacebook) {
        this.storeFacebook = storeFacebook;
    }

    public String getStoreLineAt() {
        return storeLineAt;
    }

    public void setStoreLineAt(String storeLineAt) {
        this.storeLineAt = storeLineAt;
    }

    public String getStoreTwitter() {
        return storeTwitter;
    }

    public void setStoreTwitter(String storeTwitter) {
        this.storeTwitter = storeTwitter;
    }

    public String getStoreInstagram() {
        return storeInstagram;
    }

    public void setStoreInstagram(String storeInstagram) {
        this.storeInstagram = storeInstagram;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLineId() {
        return lineId;
    }

    public void setLineId(String lineId) {
        this.lineId = lineId;
    }

    public String getLineAt() {
        return lineAt;
    }

    public void setLineAt(String lineAt) {
        this.lineAt = lineAt;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getTitleOtherName() {
        return titleOtherName;
    }

    public void setTitleOtherName(String titleOtherName) {
        this.titleOtherName = titleOtherName;
    }

    public String getTaxId() {
        return taxId;
    }

    public void setTaxId(String taxId) {
        this.taxId = taxId;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Set<String> getServiceTypes() {
        return serviceTypes;
    }

    public void setServiceTypes(Set<String> serviceTypes) {
        this.serviceTypes = serviceTypes;
    }

    public String getTmnApiKey() {
        return tmnApiKey;
    }

    public void setTmnApiKey(String tmnApiKey) {
        this.tmnApiKey = tmnApiKey;
    }
}
