package com.eggdigital.rpp.edcapp.validation;

import com.eggdigital.rpp.edcapp.validation.constraints.RequiredField;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class RequiredFieldValidator implements ConstraintValidator<RequiredField, Object> {

    private String field;

    @Override
    public void initialize(RequiredField constraintAnnotation) {
        field = constraintAnnotation.field();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        if (value == null) {
            return false;
        }

        if (value instanceof String && ((String) value).trim().isEmpty()) {
            return false;
        }

        return true;
    }
}
