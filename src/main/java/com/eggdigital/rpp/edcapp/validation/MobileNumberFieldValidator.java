package com.eggdigital.rpp.edcapp.validation;

import com.google.common.base.Strings;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Added by Patthara L. 30 AUG 2018
 */
public class MobileNumberFieldValidator implements ConstraintValidator<MobileNumberField, String> {

    @Override
    public void initialize(MobileNumberField constraintAnnotation) {
    }

    @Override
    public boolean isValid(String mobileNumber, ConstraintValidatorContext context) {
        if(Strings.isNullOrEmpty(mobileNumber)) {
            return true;
        }

        //validate phone numbers of format "1234567890"
        if (mobileNumber.matches("\\d{10}")) return true;
        //validating phone number with -, . or spaces
        else if(mobileNumber.matches("\\d{3}[-\\.\\s]\\d{3}[-\\.\\s]\\d{4}")) return true;
        //validating phone number with extension length from 3 to 5
        else if(mobileNumber.matches("\\d{3}-\\d{3}-\\d{4}\\s(x|(ext))\\d{3,5}")) return true;
        //validating phone number where area code is in braces ()
        else if(mobileNumber.matches("\\(\\d{3}\\)-\\d{3}-\\d{4}")) return true;
        //return false if nothing matches the input
        else return false;
    }
}
