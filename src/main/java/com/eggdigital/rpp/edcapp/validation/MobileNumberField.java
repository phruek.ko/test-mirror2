package com.eggdigital.rpp.edcapp.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Added by Patthara L. 30 AUG 2018
 */
@Target({FIELD})
@Retention(RUNTIME)
@Constraint(validatedBy = {MobileNumberFieldValidator.class})
public @interface MobileNumberField {

    String message() default "{error.mobile_number_field}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String field() default "";

}