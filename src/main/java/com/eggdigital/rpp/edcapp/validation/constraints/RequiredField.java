package com.eggdigital.rpp.edcapp.validation.constraints;

import com.eggdigital.rpp.edcapp.validation.RequiredFieldValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({FIELD})
@Retention(RUNTIME)
@Constraint(validatedBy = {RequiredFieldValidator.class})
public @interface RequiredField {

    String message() default "{validation.required_field}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String field() default "";

    @Target({FIELD})
    @Retention(RUNTIME)
    @interface List {
        RequiredField[] value();
    }
}
