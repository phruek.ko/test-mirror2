package com.eggdigital.rpp.edcapp.validation;

/**
 * Created by jaruwan.yen on 10/12/2017 AD.
 */
public interface ValidateGroups {
    interface RedeemByCode {
    }

    interface RedeemByCampaign {
    }

    interface RedeemReversal {
    }
}
