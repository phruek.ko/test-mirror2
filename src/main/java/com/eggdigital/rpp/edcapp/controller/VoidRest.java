package com.eggdigital.rpp.edcapp.controller;

import com.eggdigital.rpp.edcapp.dto.VoidRequestDto;
import com.eggdigital.rpp.edcapp.dto.VoidResponseDto;
import com.eggdigital.rpp.edcapp.service.VoidService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class VoidRest {

    @Autowired
    private VoidService voidService;

    @PostMapping("/voids")
    public VoidResponseDto voidTransaction(@Valid @RequestBody VoidRequestDto body) {
        return voidService.voidTransaction(body.getBrandId(), body.getOutletId(), body.getTerminalId(), body.getTraceId());
    }

    @GetMapping("/pre-voids")
    public VoidResponseDto preVoidTransaction(@RequestParam("brand_id") String brandId,
                                              @RequestParam("outlet_id") String outletId,
                                              @RequestParam("terminal_id") String terminalId,
                                              @RequestParam("trace_id") String traceId) {
        return voidService.preVoidTransaction(brandId, outletId, terminalId, traceId);
    }
}
