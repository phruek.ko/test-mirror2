package com.eggdigital.rpp.edcapp.controller;

import com.eggdigital.rpp.edcapp.dto.RedeemByCampaignRequestDto;
import com.eggdigital.rpp.edcapp.dto.RedeemByCampaignResponseDto;
import com.eggdigital.rpp.edcapp.dto.RedeemByCodeRequestDto;
import com.eggdigital.rpp.edcapp.dto.RedeemByCodeResponseDto;
import com.eggdigital.rpp.edcapp.handler.ApiErrorException;
import com.eggdigital.rpp.edcapp.service.RedeemService;
import com.eggdigital.rpp.edcapp.validation.ValidateGroups;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.groups.Default;
import java.io.IOException;

@RestController
@RequestMapping("/redeems")
public class RedeemRest {

    @Autowired
    private RedeemService redeemService;

    @PostMapping("/campaign")
    public RedeemByCampaignResponseDto redeemByCampaign(@Validated({Default.class, ValidateGroups.RedeemByCampaign.class}) @RequestBody RedeemByCampaignRequestDto requestDto) throws ApiErrorException, IOException {
        return redeemService.redeemByCampaign(requestDto);
    }

    @PostMapping("/code")
    public RedeemByCodeResponseDto redeemByCode(@Validated({Default.class, ValidateGroups.RedeemByCode.class}) @RequestBody RedeemByCodeRequestDto requestDto) throws ApiErrorException, IOException {
        return redeemService.redeemByCode(requestDto);
    }

}
