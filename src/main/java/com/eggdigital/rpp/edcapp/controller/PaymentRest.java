package com.eggdigital.rpp.edcapp.controller;

import com.eggdigital.rpp.edcapp.dto.PaymentChargeRequestDto;
import com.eggdigital.rpp.edcapp.dto.PaymentChargeResponseDto;
import com.eggdigital.rpp.edcapp.service.PaymentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RequestMapping("/payments")
@RestController
public class PaymentRest {

    private Logger log = LoggerFactory.getLogger(PaymentRest.class);

    @Autowired
    private PaymentService paymentService;

    @PostMapping("/charge")
    public PaymentChargeResponseDto charge(@Valid @RequestBody PaymentChargeRequestDto request) {
        PaymentChargeResponseDto responseDto = paymentService.charge(request);
        return responseDto;
    }
}
