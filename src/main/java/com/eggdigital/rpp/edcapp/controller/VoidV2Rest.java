package com.eggdigital.rpp.edcapp.controller;

import com.eggdigital.rpp.edcapp.dto.VoidResponseDto;
import com.eggdigital.rpp.edcapp.dto.VoidV2RequestDto;
import com.eggdigital.rpp.edcapp.service.VoidService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/v2")
public class VoidV2Rest {

    @Autowired
    private VoidService voidService;

    @GetMapping("/pre-voids")
    public VoidResponseDto preVoidTransaction(@RequestParam("brand_id") String brandId,
                                              @RequestParam("outlet_id") String outletId,
                                              @RequestParam("terminal_id") String terminalId,
                                              @RequestParam("trace_id") String traceId,
                                              @RequestParam("tx_ref_id") String txRefId) {
        return voidService.preVoidTransactionByTxRefId(brandId, outletId, terminalId, traceId, txRefId);
    }

    @PostMapping("/voids")
    public VoidResponseDto voidTransaction(@Valid @RequestBody VoidV2RequestDto body) {
        return voidService.voidTransactionByTxRefId(body.getBrandId(), body.getOutletId(), body.getTerminalId(), body.getTraceId(), body.getTxRefId());
    }


}
