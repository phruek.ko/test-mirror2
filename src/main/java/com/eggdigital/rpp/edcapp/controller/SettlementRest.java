package com.eggdigital.rpp.edcapp.controller;

import com.eggdigital.rpp.edcapp.dto.SettlementTransaction;
import com.eggdigital.rpp.edcapp.dto.SettlementTransactionByBrand;
import com.eggdigital.rpp.edcapp.service.SettlementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by bombay on 11/14/2017 AD.
 */
@RestController
@RequestMapping("/settlements")
public class SettlementRest {

    @Autowired
    private SettlementService settlementService;

    @GetMapping
    public ResponseEntity<SettlementTransaction> settlements() {
        return ResponseEntity.ok(settlementService.settlements());
    }

    @GetMapping("/{brandId}")
    public ResponseEntity<SettlementTransactionByBrand> settlementsByBrandId(@PathVariable("brandId") String brandId) {
        return ResponseEntity.ok(settlementService.settlementsByBrandId(brandId));
    }
}
