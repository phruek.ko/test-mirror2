package com.eggdigital.rpp.edcapp.controller;

import com.eggdigital.rpp.edcapp.domain.AccountType;
import com.eggdigital.rpp.edcapp.dto.CustomerSlipChannelCreateOrUpdateRequestDto;
import com.eggdigital.rpp.edcapp.dto.CustomerSlipChannelResponseDto;
import com.eggdigital.rpp.edcapp.service.SlipChannelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/customers")
@RestController
public class SlipChannelRest {

    @Autowired
    private SlipChannelService slipChannelService;

    @GetMapping("/slip-channel")
    public CustomerSlipChannelResponseDto getCustomerSlipChannel(@RequestParam("brand_id") String brandId,
                                                                 @RequestParam("outlet_id") String outletId,
                                                                 @RequestParam("terminal_id") String terminalId,
                                                                 @RequestParam("acc_type") AccountType accType,
                                                                 @RequestParam("acc_value") String accValue) {

        return slipChannelService.checkFirstTime(terminalId, accType, accValue);
    }

    @PostMapping("/slip-channel")
    public CustomerSlipChannelResponseDto createOrUpdateCustomerSlipChannel(@Valid @RequestBody CustomerSlipChannelCreateOrUpdateRequestDto request) {

        CustomerSlipChannelResponseDto response = slipChannelService.createOrUpdateSlipChannel(request.terminalId, request.thaiId, request.channel, request.getValue());
        return response;

    }

}
