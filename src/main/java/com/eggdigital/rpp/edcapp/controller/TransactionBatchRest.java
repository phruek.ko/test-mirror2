package com.eggdigital.rpp.edcapp.controller;

import com.eggdigital.rpp.edcapp.domain.Status;
import com.eggdigital.rpp.edcapp.domain.TransactionBatch;
import com.eggdigital.rpp.edcapp.service.TransactionBatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by bombay on 10/18/2017 AD.
 */
@RestController
@RequestMapping("/transaction/batch")
public class TransactionBatchRest {

    @Autowired
    private TransactionBatchService transactionService;

    @GetMapping
    public ResponseEntity<TransactionBatch> genBatch(@RequestParam("brand_id") String brandId, @RequestParam("outlet_id") String outletId, @RequestParam("terminal_id") String terminalId) {
        return ResponseEntity.ok(transactionService.genTransactionBatch(brandId, outletId, terminalId));
    }

    @PutMapping
    public ResponseEntity<List<TransactionBatch>> clearBatchByTerminalId(@RequestParam("brand_id") String brandId, @RequestParam("outlet_id") String outletId, @RequestParam("terminal_id") String terminalId) {
        return ResponseEntity.ok(transactionService.updateByBrandIdOutletIdTerminalIdStatusSettle(brandId, outletId, terminalId, Status.PENDING));
    }
}
