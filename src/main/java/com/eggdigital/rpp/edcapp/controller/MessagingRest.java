package com.eggdigital.rpp.edcapp.controller;

import com.eggdigital.rpp.edcapp.dto.MessagingRequestDto;
import com.eggdigital.rpp.edcapp.dto.MessagingResponseDto;
import com.eggdigital.rpp.edcapp.model.CustomerSlipChannel;
import com.eggdigital.rpp.edcapp.service.MessagingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/messaging")
public class MessagingRest {

    @Autowired
    private MessagingService messagingService;

    @PostMapping
    public MessagingResponseDto charge(@Valid @RequestBody MessagingRequestDto request) {

        messagingService.sendByTraceId(request);

        return new MessagingResponseDto(new CustomerSlipChannel("", request.channel, request.value));
    }
}
