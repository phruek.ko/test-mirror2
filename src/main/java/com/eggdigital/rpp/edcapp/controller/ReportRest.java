package com.eggdigital.rpp.edcapp.controller;

import com.eggdigital.rpp.edcapp.api.edc.ApiEdcService;
import com.eggdigital.rpp.edcapp.api.model.ApiStatus;
import com.eggdigital.rpp.edcapp.dto.*;
import com.eggdigital.rpp.edcapp.exception.CustomThirdPartyExcepton;
import com.eggdigital.rpp.edcapp.exception.NotFoundException;
import com.eggdigital.rpp.edcapp.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * Created by bombay on 10/31/2017 AD.
 */
@RestController
@RequestMapping("/reports")
public class ReportRest {

    @Autowired
    private ReportService reportService;

    @Autowired
    private ApiEdcService apiEdcService;

    @GetMapping("/receipts")
    public ResponseEntity<ReceiptDto> getReceiptByBrandIdAndOutletAndTerminalIdAndTraceId(@RequestParam(value = "brand_id", required = false) String brandId, @RequestParam(value = "outlet_id", required = false) String outletId, @RequestParam(value = "terminal_id", required = false) String terminalId, @RequestParam(value = "trace_id", required = false) String traceId) throws NotFoundException {
        ReceiptDto receiptDto = reportService.getReceiptByBrandIdAndOutletAndTerminalIdAndTraceId(brandId, outletId, terminalId, traceId);
        if (receiptDto == null) {
            throw new NotFoundException("Data not founds");
        }
        return ResponseEntity.ok(receiptDto);
    }

    @GetMapping("/receipts/last")
    public ResponseEntity<ReceiptDto> getReceiptLastByBrandIdAndOutletAndTerminalId(@RequestParam(value = "brand_id", required = false) String brandId, @RequestParam(value = "outlet_id", required = false) String outletId, @RequestParam(value = "terminal_id", required = false) String terminalId) throws NotFoundException {
        ReceiptDto receiptDto = reportService.getReceiptLastByBrandIdAndOutletAndTerminalId(brandId, outletId, terminalId);
        if (receiptDto == null) {
            throw new NotFoundException("Data not found");
        }
        return ResponseEntity.ok(receiptDto);
    }

    @PostMapping("/preview-settlement")
    public ResponseEntity<SettlementReport> previewSettlement(@RequestBody @Validated VerifyUserDto dto) {
        if (!apiEdcService.verifyLogin(dto.getUsername(), dto.getPassword())) {
            throw new CustomThirdPartyExcepton(new ApiStatus("EDCLOGIN00", "Unauthorized"));
        }
        SettlementReport settlementReport = reportService.previewSettlementByTerminalInfo(dto);
        if (settlementReport == null) {
            throw new CustomThirdPartyExcepton(new ApiStatus("411", "Not found data for preview settlement"));
        }
        return ResponseEntity.ok(settlementReport);
    }

    @PostMapping("/settlement")
    public ResponseEntity<SettlementReport> settlement(@RequestBody @Validated VerifyUserDto dto) {
        if (!apiEdcService.verifyLogin(dto.getUsername(), dto.getPassword())) {
            throw new CustomThirdPartyExcepton(new ApiStatus("EDCLOGIN00", "Unauthorized"));
        }
        SettlementReport settlementReport = reportService.settlementByTerminalInfo(dto);
        if (settlementReport == null) {
            throw new CustomThirdPartyExcepton(new ApiStatus("411", "Not found data for settlement"));
        }
        return ResponseEntity.ok(settlementReport);
    }

    @GetMapping("/summary")
    public ResponseEntity<SummaryReport> summary(@RequestParam(value = "brand_id", required = false) String brandId, @RequestParam(value = "outlet_id", required = false) String outletId, @RequestParam(value = "terminal_id", required = false) String terminalId, @RequestParam(value = "transaction_date", required = false) String transactionDate) {
        SummaryReport summaryReport = reportService.summaryByBrandIdOutletIdTerminalId(brandId, outletId, terminalId, transactionDate);
        if (summaryReport == null) {
            throw new NotFoundException("Data not found");
        }
        return ResponseEntity.ok(summaryReport);
    }

    @GetMapping("/detail-summary")
    public ResponseEntity<DetailSummaryReportNew> detailSummary(@RequestParam(value = "brand_id", required = false) String brandId, @RequestParam(value = "outlet_id", required = false) String outletId, @RequestParam(value = "terminal_id", required = false) String terminalId, @RequestParam(value = "transaction_date", required = false) String transactionDate) {
        DetailSummaryReportNew detailSummaryReport = reportService.detailSummaryByBrandIdOutletIdTerminalIdTransactionDate(brandId, outletId, terminalId, transactionDate);
        if (detailSummaryReport == null) {
            throw new NotFoundException("Data not found");
        }
        return ResponseEntity.ok(detailSummaryReport);
    }

}
