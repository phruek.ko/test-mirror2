package com.eggdigital.rpp.edcapp.controller;

import com.eggdigital.rpp.edcapp.dto.EarnPointRequestDto;
import com.eggdigital.rpp.edcapp.dto.EarnPointResponseDto;
import com.eggdigital.rpp.edcapp.handler.ApiErrorException;
import com.eggdigital.rpp.edcapp.service.PointService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping("/points")
public class PointRest {

    private final Logger log = LoggerFactory.getLogger(PointRest.class);

    @Autowired
    private PointService pointService;

    @PostMapping(value = "/issue")
    public EarnPointResponseDto issue(@Validated @RequestBody EarnPointRequestDto requestDto) throws ApiErrorException, IOException {
        return pointService.issue(requestDto);
    }

}
