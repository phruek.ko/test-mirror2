package com.eggdigital.rpp.edcapp.controller;

import com.eggdigital.rpp.edcapp.domain.AccountType;
import com.eggdigital.rpp.edcapp.dto.*;
import com.eggdigital.rpp.edcapp.handler.ApiErrorException;
import com.eggdigital.rpp.edcapp.ms.RppBsApi;
import com.eggdigital.rpp.edcapp.ms.dto.BsCheckPointResponse;
import com.eggdigital.rpp.edcapp.service.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/customers")
public class CustomerRest {

    private final Logger log = LoggerFactory.getLogger(CustomerRest.class);

    @Autowired
    private RppBsApi rppBsApi;

    @Autowired
    private CustomerService customerService;


    @GetMapping
    public CustomerMobileNumbersResponseDto getCustomerInfo(@RequestParam("brand_id") String brandId,
                                                            @RequestParam("outlet_id") String outletId,
                                                            @RequestParam("terminal_id") String terminalId,
                                                            @RequestParam("acc_type") AccountType accType,
                                                            @RequestParam("acc_value") String accValue) {

        List<String> mobileNumbers = customerService.getCustomerInfo(terminalId, accType, accValue);

        CustomerMobileNumbersResponseDto response = new CustomerMobileNumbersResponseDto();
        response.setMobileNumbers(mobileNumbers);

        return response;
    }


    @GetMapping("/card")
    public CardInfoResponseDto getCardInfo(@Validated CardInfoRequest dto) throws ApiErrorException {

        if (dto.getAcc_type().equals("TRUECARD")) {
            dto.setAcc_type("CARD");
        }

        CardInfo bsCardInfo = rppBsApi.getCustomerCardInfo(dto).getData();

        List<CardInfoResponseDto.PointPocket> pointPockets = new ArrayList<>();
        if (bsCardInfo.getPointPockets() != null) {
            bsCardInfo.getPointPockets().forEach(pp ->
                    {
                        CardInfoResponseDto.PointPocket pointPocket = new CardInfoResponseDto.PointPocket(pp.getDescription(),
                                String.valueOf(pp.getBalance()),
                                pp.getExpire());

                        pointPockets.add(pointPocket);
                    }
            );
        }

        CardInfoResponseDto response = new CardInfoResponseDto(
                bsCardInfo.getCardNo(),
                bsCardInfo.getCardType(),
                bsCardInfo.getCardStatus(),
                bsCardInfo.getCardExpire(),
                bsCardInfo.getCustomerName(),
                String.valueOf(bsCardInfo.getPointBalance()),
                pointPockets,
                String.valueOf(bsCardInfo.getSlipTypeId()),
                bsCardInfo.getSlipTypeDescription()
        );
        return response;
    }

    @PostMapping("/card")
    public CardInfoSlip saveCardInfo(@Validated CardInfoRequest dto) throws ApiErrorException {
        return customerService.saveCardSlipTx(dto);
    }

    @GetMapping("/check-point")
    public Map checkPoints(@Validated CheckPointRequest dto) throws ApiErrorException {
        BsCheckPointResponse result = rppBsApi.checkPoints(dto);
        return result.getData();
    }
}
