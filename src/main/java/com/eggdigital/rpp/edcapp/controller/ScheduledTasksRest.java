package com.eggdigital.rpp.edcapp.controller;

import com.eggdigital.rpp.edcapp.merchant.service.impl.MerchantScheduledTasks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

/**
 * Created by bombay on 14/6/2018 AD.
 */
@RestController
@RequestMapping("/schedule")
public class ScheduledTasksRest {

    private final MerchantScheduledTasks merchantScheduledTasks;

    @Autowired
    public ScheduledTasksRest(MerchantScheduledTasks merchantScheduledTasks) {
        this.merchantScheduledTasks = merchantScheduledTasks;
    }

    @GetMapping("/merchants/sync")
    public void syncMerchants() {
        merchantScheduledTasks.syncMerchantFromMerchantToEdc();
    }

    @GetMapping("/outlets/sync")
    public void syncOutlets() throws IOException, ExecutionException, InterruptedException {
        merchantScheduledTasks.syncOutletFromMerchantToEdc();
    }

}
