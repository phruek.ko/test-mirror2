package com.eggdigital.rpp.edcapp.security;

import org.springframework.security.core.AuthenticationException;

public class ApiSignatureAuthenticationException extends AuthenticationException {

    public ApiSignatureAuthenticationException(String msg) {
        super(msg);
    }
}
