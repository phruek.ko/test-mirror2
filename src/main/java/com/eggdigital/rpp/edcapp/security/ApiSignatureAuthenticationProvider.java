package com.eggdigital.rpp.edcapp.security;

import com.eggdigital.rpp.edcapp.service.CryptoService;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

public class ApiSignatureAuthenticationProvider implements AuthenticationProvider {

    private final Logger log = LoggerFactory.getLogger(ApiSignatureAuthenticationProvider.class);

    private final CryptoService cryptoService;

    public ApiSignatureAuthenticationProvider(CryptoService cryptoService) {
        this.cryptoService = cryptoService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        ApiSignatureAuthentication apiSignatureAuthentication = (ApiSignatureAuthentication) authentication;
        String signature = apiSignatureAuthentication.getSignature();
        String authorization = apiSignatureAuthentication.getAuthorization();

        if (Strings.isNullOrEmpty(signature) && Strings.isNullOrEmpty(authorization)) {
            throw new ApiSignatureAuthenticationException("Authentication failed");
        }

        boolean isXSignatureValid = false, isAuthorizationValid = false;
        try {

            if (!Strings.isNullOrEmpty(signature)) {
                isXSignatureValid = cryptoService.validate(signature);
            }

            if (!isXSignatureValid) {
                if (!Strings.isNullOrEmpty(authorization)) {
                    isAuthorizationValid = true;
                }
            }
        } catch (Exception ex) {
            throw new ApiSignatureAuthenticationException("Authentication failed");
        }

        if (!isXSignatureValid && !isAuthorizationValid) {
            throw new ApiSignatureAuthenticationException("Authentication failed");
        }

        return new ApiSignatureAuthentication(signature, authorization);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return ApiSignatureAuthentication.class.isAssignableFrom(authentication);
    }
}
