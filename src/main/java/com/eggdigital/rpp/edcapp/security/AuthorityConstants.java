package com.eggdigital.rpp.edcapp.security;

/**
 * Created by jaruwan.yen on 10/16/2017 AD.
 */
public final class AuthorityConstants {
    public static final String ANONYMOUS = "ROLE_ANONYMOUS";
    public static final String ADMIN = "ROLE_ADMIN";
    public static final String USER = "ROLE_USER";

    private AuthorityConstants() {
    }
}
