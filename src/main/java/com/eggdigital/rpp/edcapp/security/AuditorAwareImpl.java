package com.eggdigital.rpp.edcapp.security;

import com.eggdigital.rpp.edcapp.config.constant.SecurityConstants;
import org.springframework.data.domain.AuditorAware;

/**
 * Created by wanatchapong on 12/15/2016 AD.
 */
public class AuditorAwareImpl implements AuditorAware<String> {

    @Override
    public String getCurrentAuditor() {
//        String userName = SecurityUtils.getCurrentUserLogin();
//        return userName != null ? userName : SecurityConstants.ANONYMOUS_USER;
        return SecurityConstants.ANONYMOUS_USER;
    }
}
