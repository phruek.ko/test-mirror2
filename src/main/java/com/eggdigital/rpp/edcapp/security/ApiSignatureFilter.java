package com.eggdigital.rpp.edcapp.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ApiSignatureFilter extends GenericFilterBean {

    private final static String X_SIGNATURE = "X-Signature";
    private final Logger log = LoggerFactory.getLogger(ApiSignatureFilter.class);
    private final AuthenticationManager authenticationManager;
    private final AuthenticationFailureHandler failureHandler;

    public ApiSignatureFilter(AuthenticationManager authenticationManager, AuthenticationFailureHandler failureHandler) {
        this.authenticationManager = authenticationManager;
        this.failureHandler = failureHandler;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        final String signature = httpRequest.getHeader(X_SIGNATURE);
        final String authorization = httpRequest.getHeader(HttpHeaders.AUTHORIZATION);

        try {
            Authentication authResult = authenticationManager.authenticate(new ApiSignatureAuthentication(signature, authorization));
            SecurityContextHolder.getContext().setAuthentication(authResult);
            chain.doFilter(request, response);

        } catch (AuthenticationException ex) {
            log.error("exception : {}", ex.getMessage());
            failureHandler.onAuthenticationFailure(httpRequest, httpResponse, ex);
        }
    }
}
