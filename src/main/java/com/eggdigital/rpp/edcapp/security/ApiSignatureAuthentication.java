package com.eggdigital.rpp.edcapp.security;

import org.springframework.security.authentication.AbstractAuthenticationToken;

public class ApiSignatureAuthentication extends AbstractAuthenticationToken {

    private String signature;
    private String authorization;

    public ApiSignatureAuthentication(String signature, String authorization) {
        super(null);
        setAuthenticated(true);
        this.signature = signature;
        this.authorization = authorization;
    }

    @Override
    public Object getCredentials() {
        return "";
    }

    @Override
    public Object getPrincipal() {
        return signature;
    }

    public String getSignature() {
        return signature;
    }

    public String getAuthorization() {
        return authorization;
    }

}
