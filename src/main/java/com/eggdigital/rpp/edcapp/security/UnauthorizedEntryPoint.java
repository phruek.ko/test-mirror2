package com.eggdigital.rpp.edcapp.security;

import com.eggdigital.rpp.edcapp.dto.base.ResponseStatus;
import com.eggdigital.rpp.edcapp.handler.ApiError;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class UnauthorizedEntryPoint implements AuthenticationEntryPoint {

    private final Logger log = LoggerFactory.getLogger(UnauthorizedEntryPoint.class);

    private final ObjectMapper mapper;

    public UnauthorizedEntryPoint(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        log.debug("UnauthorizedEntryPoint -> exception : {}", exception.getClass().getName());
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.setCharacterEncoding(StandardCharsets.UTF_8.toString());
        mapper.writeValue(response.getOutputStream(),
                ApiError.builder()
                        .status(new ResponseStatus(HttpStatus.UNAUTHORIZED, exception.getMessage()))
                        .build());
    }
}
