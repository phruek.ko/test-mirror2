package com.eggdigital.rpp.edcapp.ms.dto;

import com.eggdigital.rpp.edcapp.dto.base.AbstractTransactionResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.math.BigDecimal;

public class BsPointIssueResponse extends AbstractMsResponse<BsPointIssueResponse.Data> {

    public static class Data extends AbstractTransactionResponse implements Serializable {

        @JsonProperty("point")
        private Integer point;

        @JsonProperty("sale_amt")
        private BigDecimal saleAmt;

        @JsonProperty("sale_cal_point")
        private BigDecimal saleCalPoint;

        public Integer getPoint() {
            return point;
        }

        public void setPoint(Integer point) {
            this.point = point;
        }

        public BigDecimal getSaleAmt() {
            return saleAmt;
        }

        public void setSaleAmt(BigDecimal saleAmt) {
            this.saleAmt = saleAmt;
        }

        public BigDecimal getSaleCalPoint() {
            return saleCalPoint;
        }

        public void setSaleCalPoint(BigDecimal saleCalPoint) {
            this.saleCalPoint = saleCalPoint;
        }
    }
}
