package com.eggdigital.rpp.edcapp.ms.impl;

import com.eggdigital.rpp.edcapp.api.ApiSender;
import com.eggdigital.rpp.edcapp.dto.CardInfoRequest;
import com.eggdigital.rpp.edcapp.dto.CheckPointRequest;
import com.eggdigital.rpp.edcapp.dto.PointIssueRequest;
import com.eggdigital.rpp.edcapp.dto.RedeemRequest;
import com.eggdigital.rpp.edcapp.ms.RppBsApi;
import com.eggdigital.rpp.edcapp.ms.dto.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static com.eggdigital.rpp.edcapp.api.ApiSender.Microservice.*;

/**
 * Created by jaruwan.yen on 10/16/2017 AD.
 */
@Component
public class RppBsApiImpl implements RppBsApi {

    private final Logger log = LoggerFactory.getLogger(RppBsApiImpl.class);

    @Autowired
    private ApiSender apiSender;

    @Override
    public BsRedeemByCampaignResponse redeemByCampaign(RedeemRequest dto) {
        return apiSender.post(REDEEM_BY_CAMPAIGN, BsRedeemByCampaignResponse.class, dto, null, true);
    }

    @Override
    public BsRedeemByCodeResponse redeemByCode(RedeemRequest dto) {
        return apiSender.post(REDEEM_BY_CODE, BsRedeemByCodeResponse.class, dto, null, true);
    }

    @Override
    public BsRedeemReversalResponse reversal(RedeemRequest dto) {
        return apiSender.post(REDEEM_REVERSAL, BsRedeemReversalResponse.class, dto, null, true);
    }

    @Override
    public BsCustomerCardInfoResponse getCustomerCardInfo(CardInfoRequest dto) {
        Map<String, String> params = new HashMap<>();
        params.put("brand_id", dto.getBrand_id());
        params.put("outlet_id", dto.getOutlet_id());
        params.put("terminal_id", dto.getTerminal_id());
        params.put("acc_type", dto.getAcc_type());
        params.put("acc_value", dto.getAcc_value());
        return apiSender.get(CUSTOMER_CARD_INFO, BsCustomerCardInfoResponse.class, params, null, true);
    }

    @Override
    public BsCheckPointResponse checkPoints(CheckPointRequest dto) {
        Map<String, String> params = new HashMap<>();
        params.put("brand_id", dto.getBrand_id());
        params.put("outlet_id", dto.getOutlet_id());
        params.put("terminal_id", dto.getTerminal_id());
        params.put("acc_type", dto.getAcc_type());
        params.put("acc_value", dto.getAcc_value());
        params.put("point", Integer.toString(dto.getPoint()));
        return apiSender.get(CUSTOMER_CHECK_POINT, BsCheckPointResponse.class, params, null, true);
    }

    @Override
    public BsPointIssueResponse issuePoint(PointIssueRequest dto) {
        return apiSender.post(POINT_ISSUE, BsPointIssueResponse.class, dto, null, true);
    }
}
