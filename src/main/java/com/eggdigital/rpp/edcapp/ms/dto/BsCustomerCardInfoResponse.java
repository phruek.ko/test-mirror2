package com.eggdigital.rpp.edcapp.ms.dto;

import com.eggdigital.rpp.edcapp.dto.CardInfo;

public class BsCustomerCardInfoResponse extends AbstractMsResponse<CardInfo> {
}
