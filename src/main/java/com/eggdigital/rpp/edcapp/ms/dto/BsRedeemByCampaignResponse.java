package com.eggdigital.rpp.edcapp.ms.dto;

import com.eggdigital.rpp.edcapp.dto.RedeemByCampaignData;

public class BsRedeemByCampaignResponse extends AbstractMsResponse<RedeemByCampaignData> {
}
