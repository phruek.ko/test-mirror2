package com.eggdigital.rpp.edcapp.ms.dto;

import com.eggdigital.rpp.edcapp.dto.base.ResponseStatus;
import com.eggdigital.rpp.edcapp.handler.PropertyError;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by jaruwan.yen on 10/16/2017 AD.
 */
public class AbstractMsResponse<T> {

    @JsonProperty("status")
    private ResponseStatus status;

    @JsonProperty("data")
    private T data;

    // move json mapping annotation to getter and setter method
    private List<PropertyError> errors;

    public ResponseStatus getStatus() {
        return status;
    }

    public void setStatus(ResponseStatus status) {
        this.status = status;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @JsonGetter("errors")
    public List<PropertyError> getErrors() {
        return errors;
    }

    @JsonSetter("errors")
    public void setErrors(List<PropertyError> errors) {
        this.errors = errors;
    }

    @JsonSetter("error")
    public void setError(PropertyError error) {
        if (errors == null) {
            errors = new ArrayList<>();
        }
        errors.add(error);
    }

    public boolean isSuccess() {
        return Objects.equals(HttpStatus.OK.toString(), getStatusCode());
    }

    public boolean hasErrors() {
        return errors != null && !errors.isEmpty();
    }

    public String getStatusCode() {
        return status != null ? status.getCode() : null;
    }

    public String getStatusMessage() {
        return status != null ? status.getMessage() : null;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("status", status)
                .append("data", data)
                .append("errors", errors)
                .toString();
    }
}
