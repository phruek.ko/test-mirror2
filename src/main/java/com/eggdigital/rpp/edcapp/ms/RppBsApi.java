package com.eggdigital.rpp.edcapp.ms;

import com.eggdigital.rpp.edcapp.dto.CardInfoRequest;
import com.eggdigital.rpp.edcapp.dto.CheckPointRequest;
import com.eggdigital.rpp.edcapp.dto.PointIssueRequest;
import com.eggdigital.rpp.edcapp.dto.RedeemRequest;
import com.eggdigital.rpp.edcapp.handler.ApiErrorException;
import com.eggdigital.rpp.edcapp.ms.dto.*;

/**
 * Created by jaruwan.yen on 10/16/2017 AD.
 */
public interface RppBsApi {

    BsRedeemByCampaignResponse redeemByCampaign(RedeemRequest dto) throws ApiErrorException;

    BsRedeemByCodeResponse redeemByCode(RedeemRequest dto) throws ApiErrorException;

    BsRedeemReversalResponse reversal(RedeemRequest dto) throws ApiErrorException;

    BsCustomerCardInfoResponse getCustomerCardInfo(CardInfoRequest dto) throws ApiErrorException;

    BsCheckPointResponse checkPoints(CheckPointRequest dto) throws ApiErrorException;

    BsPointIssueResponse issuePoint(PointIssueRequest dto) throws ApiErrorException;
}
