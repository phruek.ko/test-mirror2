package com.eggdigital.rpp.edcapp.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by bombay on 11/14/2017 AD.
 */
public class SettlementTransactionByBrand extends SettlementTransaction {

    @JsonProperty("brand_id")
    private String brandId;

    public SettlementTransactionByBrand(String message, String brandId) {
        super(message);
        this.brandId = brandId;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

}
