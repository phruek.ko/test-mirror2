package com.eggdigital.rpp.edcapp.dto;

import com.eggdigital.rpp.edcapp.validation.constraints.RequiredField;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;

/**
 * Created by wanatchapong on 4/7/2017 AD.
 */
public class CardInfoRequest implements Serializable {

    private String tx_ref_id;

    @RequiredField(field = "account type")
    private String acc_type;

    @RequiredField(field = "account value")
    private String acc_value;

    @RequiredField(field = "brand id")
    private String brand_id;

    @RequiredField(field = "outlet id")
    private String outlet_id;

    @RequiredField(field = "terminal id")
    private String terminal_id;

    public String getTx_ref_id() {
        return tx_ref_id;
    }

    public void setTx_ref_id(String tx_ref_id) {
        this.tx_ref_id = tx_ref_id;
    }

    public String getAcc_type() {
        return acc_type;
    }

    public void setAcc_type(String acc_type) {
        this.acc_type = acc_type;
    }

    public String getAcc_value() {
        return acc_value;
    }

    public void setAcc_value(String acc_value) {
        this.acc_value = acc_value;
    }

    public String getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(String brand_id) {
        this.brand_id = brand_id;
    }

    public String getOutlet_id() {
        return outlet_id;
    }

    public void setOutlet_id(String outlet_id) {
        this.outlet_id = outlet_id;
    }

    public String getTerminal_id() {
        return terminal_id;
    }

    public void setTerminal_id(String terminal_id) {
        this.terminal_id = terminal_id;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("tx_ref_id", tx_ref_id)
                .append("acc_type", acc_type)
                .append("acc_value", acc_value)
                .append("brand_id", brand_id)
                .append("outlet_id", outlet_id)
                .append("terminal_id", terminal_id)
                .toString();
    }
}
