package com.eggdigital.rpp.edcapp.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class VoidV2Dto extends VoidDto {

    @JsonProperty("tx_ref_id")
    private String txRefId;

    public String getTxRefId() {
        return txRefId;
    }

    public void setTxRefId(String txRefId) {
        this.txRefId = txRefId;
    }
}
