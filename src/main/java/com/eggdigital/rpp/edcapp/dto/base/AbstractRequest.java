package com.eggdigital.rpp.edcapp.dto.base;

import com.eggdigital.rpp.edcapp.validation.constraints.RequiredField;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Created by jaruwan.yen on 10/12/2017 AD.
 */
public abstract class AbstractRequest {

    @JsonProperty("brand_id")
    @RequiredField(field = "brand id")
    private String brandId;

    @JsonProperty("outlet_id")
    @RequiredField(field = "outlet id")
    private String outletId;

    @JsonProperty("terminal_id")
    @RequiredField(field = "terminal id")
    private String terminalId;

    public AbstractRequest() {
    }

    public AbstractRequest(String brandId, String outletId, String terminalId) {
        this.brandId = brandId;
        this.outletId = outletId;
        this.terminalId = terminalId;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getOutletId() {
        return outletId;
    }

    public void setOutletId(String outletId) {
        this.outletId = outletId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("brandId", brandId)
                .append("outletId", outletId)
                .append("terminalId", terminalId)
                .toString();
    }
}
