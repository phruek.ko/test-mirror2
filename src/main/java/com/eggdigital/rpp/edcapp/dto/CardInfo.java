package com.eggdigital.rpp.edcapp.dto;

import com.eggdigital.rpp.edcapp.trueyou.TrueCardStatus;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.util.List;

/**
 * Created by bombay on 11/14/2016 AD.
 */
public class CardInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("card_no")
    private String cardNo;

    @JsonProperty("card_type")
    private String cardType;

    @JsonProperty("card_status")
    private String cardStatus;

    @JsonProperty("card_expire")
    private String cardExpire;

    @JsonProperty("customer_name")
    private String customerName;
    /**
     * Default = 0 for Sycapt suggestion
     */
    @JsonProperty("point_balance")
    private int pointBalance;

    @JsonProperty("point_pockets")
    private List<PointPocket> pointPockets;
    /**
     * Default = 0 for Sycapt suggestion
     */
    @JsonProperty("slip_type_id")
    private int slipTypeId;

    @JsonProperty("slip_type_description")
    private String slipTypeDescription;

    public CardInfo() {
        /**
         * Default value for Sycapt suggestion
         */
        this.cardNo = "";
        this.cardType = "";
        this.cardStatus = TrueCardStatus.UNDEFINED.getStatus();
        this.cardExpire = "12/00";
        this.customerName = "";
        this.slipTypeDescription = "";
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getCardStatus() {
        return cardStatus;
    }

    public void setCardStatus(String cardStatus) {
        this.cardStatus = cardStatus;
    }

    public String getCardExpire() {
        return cardExpire;
    }

    public void setCardExpire(String cardExpire) {
        this.cardExpire = cardExpire;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public Integer getPointBalance() {
        return pointBalance;
    }

    public void setPointBalance(Integer pointBalance) {
        this.pointBalance = pointBalance;
    }

    public List<PointPocket> getPointPockets() {
        return pointPockets;
    }

    public void setPointPockets(List<PointPocket> pointPockets) {
        this.pointPockets = pointPockets;
    }

    public int getSlipTypeId() {
        return slipTypeId;
    }

    public void setSlipTypeId(int slipTypeId) {
        this.slipTypeId = slipTypeId;
    }

    public String getSlipTypeDescription() {
        return slipTypeDescription;
    }

    public void setSlipTypeDescription(String slipTypeDescription) {
        this.slipTypeDescription = slipTypeDescription;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("cardNo", cardNo)
                .append("cardType", cardType)
                .append("cardStatus", cardStatus)
                .append("cardExpire", cardExpire)
                .append("customerName", customerName)
                .append("pointBalance", pointBalance)
                .append("pointPockets", pointPockets)
                .append("slipTypeId", slipTypeId)
                .append("slipTypeDescription", slipTypeDescription)
                .toString();
    }

    public static class PointPocket implements Serializable {

        @JsonProperty("description")
        private String description;

        @JsonProperty("balance")
        private Integer balance;

        @JsonProperty("expire")
        private String expire;

        public PointPocket() {
        }

        public PointPocket(String description, Integer balance, String expire) {
            this.description = description;
            this.balance = balance;
            this.expire = expire;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public Integer getBalance() {
            return balance;
        }

        public void setBalance(Integer balance) {
            this.balance = balance;
        }

        public String getExpire() {
            return expire;
        }

        public void setExpire(String expire) {
            this.expire = expire;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                    .append("description", description)
                    .append("balance", balance)
                    .append("expire", expire)
                    .toString();
        }
    }
}
