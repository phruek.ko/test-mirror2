package com.eggdigital.rpp.edcapp.dto.base;

import com.eggdigital.rpp.edcapp.domain.Transaction;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public abstract class AbstractTransactionResponse implements Serializable {

    @JsonProperty("trace_id")
    private String traceId;

    @JsonProperty("batch_id")
    private String batchId;

    @JsonProperty("tx_ref_id")
    private String txRefId;

    protected String getTraceId() {
        return traceId;
    }

    protected void setTraceId(String traceId) {
        this.traceId = traceId;
    }

    protected String getBatchId() {
        return batchId;
    }

    protected void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    protected String getTxRefId() {
        return txRefId;
    }

    protected void setTxRefId(String txRefId) {
        this.txRefId = txRefId;
    }

    /**
     * Set transaction info (traceId, batchId, txRefId) response for client print slip
     *
     * @param tx
     */
    public void setTransaction(Transaction tx) {
        setTraceId(tx.getTraceId());
        setBatchId(tx.getBatchId());
        setTxRefId(tx.getRefId());
    }
}
