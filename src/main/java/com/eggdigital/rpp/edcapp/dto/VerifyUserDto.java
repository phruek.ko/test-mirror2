package com.eggdigital.rpp.edcapp.dto;

import com.eggdigital.rpp.edcapp.dto.base.AbstractRequest;
import com.eggdigital.rpp.edcapp.validation.constraints.RequiredField;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by bombay on 10/27/2017 AD.
 */
public class VerifyUserDto extends AbstractRequest {

    @JsonProperty("username")
    @RequiredField(field = "username")
    private String username;

    @JsonProperty("password")
    @RequiredField(field = "password")
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
