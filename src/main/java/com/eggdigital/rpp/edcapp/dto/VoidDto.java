package com.eggdigital.rpp.edcapp.dto;

import com.eggdigital.rpp.edcapp.domain.TransactionType;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

/**
 * Created by bombay on 11/10/2017 AD.
 */
public class VoidDto {

    @JsonProperty("trace_id")
    private String traceId;

    @JsonProperty("void_method")
    private TransactionType voidMethod;

    @JsonProperty("ref_value")
    private String refValue;

    @JsonProperty("ref_type")
    private String refType;

    @JsonProperty("amount")
    private String amount;

    @JsonProperty("point")
    private String point;

    @JsonProperty("transaction_date")
    private Date transactionDate;

    @JsonProperty("transaction_name")
    private String transactionName;

    public String getTraceId() {
        return traceId;
    }

    public void setTraceId(String traceId) {
        this.traceId = traceId;
    }

    public TransactionType getVoidMethod() {
        return voidMethod;
    }

    public void setVoidMethod(TransactionType voidMethod) {
        this.voidMethod = voidMethod;
    }

    public String getRefValue() {
        return refValue;
    }

    public void setRefValue(String refValue) {
        this.refValue = refValue;
    }

    public String getRefType() {
        return refType;
    }

    public void setRefType(String refType) {
        this.refType = refType;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionName() {
        return transactionName;
    }

    public void setTransactionName(String transactionName) {
        this.transactionName = transactionName;
    }
}
