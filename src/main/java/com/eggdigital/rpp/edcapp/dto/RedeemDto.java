package com.eggdigital.rpp.edcapp.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by bombay on 11/8/2017 AD.
 */
public class RedeemDto {

    @JsonProperty("total")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String total;

    @JsonProperty("point")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String point;

    @JsonProperty("campaigns")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<CampaignDto> campaigns;

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public List<CampaignDto> getCampaigns() {
        return campaigns;
    }

    public void setCampaigns(List<CampaignDto> campaigns) {
        this.campaigns = campaigns;
    }
}
