package com.eggdigital.rpp.edcapp.dto;

import java.util.List;

/**
 * Created by bombay on 12/1/2017 AD.
 */
public class DetailSummaryObject {

    private int total;

    private List<DetailSummaryTransaction> transactions;

    public DetailSummaryObject() {
    }

    public DetailSummaryObject(int total, List<DetailSummaryTransaction> transactions) {
        this.total = total;
        this.transactions = transactions;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<DetailSummaryTransaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<DetailSummaryTransaction> transactions) {
        this.transactions = transactions;
    }
}
