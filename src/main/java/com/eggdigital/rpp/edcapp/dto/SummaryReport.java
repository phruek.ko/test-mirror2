package com.eggdigital.rpp.edcapp.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by bombay on 11/9/2017 AD.
 */
public class SummaryReport {

    @JsonProperty("payments")
    private List<PaymentDto> payments;

    @JsonProperty("redeem")
    private RedeemDto redeem;

    @JsonProperty("e_stamp")
    private EstampDto eStamp;

    @JsonProperty("eCouponTotal")
    private String eCoupon;

    @JsonProperty("earns")
    private List<EarnDto> earns;

    public SummaryReport() {
    }

    public SummaryReport(List<PaymentDto> payments, RedeemDto redeem, List<EarnDto> earns, EstampDto eStamp, String eCoupon) {
        this.payments = payments;
        this.redeem = redeem;
        this.earns = earns;
        this.eStamp = eStamp;
        this.eCoupon = eCoupon;
    }

    public List<PaymentDto> getPayments() {
        return payments;
    }

    public void setPayments(List<PaymentDto> payments) {
        this.payments = payments;
    }

    public RedeemDto getRedeem() {
        return redeem;
    }

    public void setRedeem(RedeemDto redeem) {
        this.redeem = redeem;
    }

    public List<EarnDto> getEarns() {
        return earns;
    }

    public void setEarns(List<EarnDto> earns) {
        this.earns = earns;
    }

    public EstampDto geteStamp() {
        return eStamp;
    }

    public void seteStamp(EstampDto eStamp) {
        this.eStamp = eStamp;
    }

    public String geteCoupon() {
        return eCoupon;
    }

    public void seteCoupon(String eCoupon) {
        this.eCoupon = eCoupon;
    }
}
