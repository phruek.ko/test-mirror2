package com.eggdigital.rpp.edcapp.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by bombay on 11/10/2017 AD.
 */
public class DetailSummaryReport {

    @JsonProperty("redeem")
    private RedeemDto redeem;

    @JsonProperty("earns")
    private List<EarnDto> earns;

    @JsonProperty("payments")
    private List<PaymentDto> payments;

    @JsonProperty("voids")
    private List<VoidDto> voids;

    public DetailSummaryReport() {
    }

    public DetailSummaryReport(RedeemDto redeem, List<EarnDto> earns, List<PaymentDto> payments, List<VoidDto> voids) {
        this.redeem = redeem;
        this.earns = earns;
        this.payments = payments;
        this.voids = voids;
    }

    public RedeemDto getRedeem() {
        return redeem;
    }

    public void setRedeem(RedeemDto redeem) {
        this.redeem = redeem;
    }

    public List<EarnDto> getEarns() {
        return earns;
    }

    public void setEarns(List<EarnDto> earns) {
        this.earns = earns;
    }

    public List<PaymentDto> getPayments() {
        return payments;
    }

    public void setPayments(List<PaymentDto> payments) {
        this.payments = payments;
    }

    public List<VoidDto> getVoids() {
        return voids;
    }

    public void setVoids(List<VoidDto> voids) {
        this.voids = voids;
    }
}
