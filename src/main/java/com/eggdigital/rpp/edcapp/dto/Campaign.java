package com.eggdigital.rpp.edcapp.dto;

public enum Campaign {

    E_COUPON("E-55%"), E_STAMP("E-66%"), SHORT_REDEEM("SHORT_REDEEM");

    private String parameterLike;

    private Campaign(String parameterLike) {
        this.parameterLike = parameterLike;
    }

    public String getParameterLike() {
        return parameterLike;
    }

    public void setParameterLike(String parameterLike) {
        this.parameterLike = parameterLike;
    }
}
