package com.eggdigital.rpp.edcapp.dto;

import com.eggdigital.rpp.edcapp.dto.base.AbstractRequest;
import com.eggdigital.rpp.edcapp.validation.constraints.RequiredField;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Created by wanatchapong on 2/9/2017 AD.
 */
public class PointIssueRequest extends AbstractRequest {

    private static final long serialVersionUID = 1L;

    @JsonProperty("tx_ref_id")
    private String txRefId;

    @JsonProperty("acc_type")
    @RequiredField(field = "account type")
    private String accType;

    @JsonProperty("acc_value")
    @RequiredField(field = "account value")
    private String accValue;

    @JsonProperty("sale_amt")
    @RequiredField(field = "sale amount")
    private String saleAmt;

    @JsonProperty("discount_amt")
    @RequiredField(field = "discount amount")
    private String discountAmt;

    @JsonProperty("payment_amt")
    @RequiredField(field = "payment amount")
    private String paymentAmt;

    public String getTxRefId() {
        return txRefId;
    }

    public void setTxRefId(String txRefId) {
        this.txRefId = txRefId;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getAccType() {
        return accType;
    }

    public void setAccType(String accType) {
        this.accType = accType;
    }

    public String getAccValue() {
        return accValue;
    }

    public void setAccValue(String accValue) {
        this.accValue = accValue;
    }

    public String getSaleAmt() {
        return saleAmt;
    }

    public void setSaleAmt(String saleAmt) {
        this.saleAmt = saleAmt;
    }

    public String getDiscountAmt() {
        return discountAmt;
    }

    public void setDiscountAmt(String discountAmt) {
        this.discountAmt = discountAmt;
    }

    public String getPaymentAmt() {
        return paymentAmt;
    }

    public void setPaymentAmt(String paymentAmt) {
        this.paymentAmt = paymentAmt;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("txRefId", txRefId)
                .append("accType", accType)
                .append("accValue", accValue)
                .append("saleAmt", saleAmt)
                .append("discountAmt", discountAmt)
                .append("paymentAmt", paymentAmt)
                .toString();
    }
}
