package com.eggdigital.rpp.edcapp.dto;

import com.eggdigital.rpp.edcapp.domain.EarnMethod;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by bombay on 11/8/2017 AD.
 */
public class EarnDto extends BaseReportDto {

    @JsonProperty("point")
    private String point;

    @JsonProperty("earn_method")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private EarnMethod earnMethod;

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public EarnMethod getEarnMethod() {
        return earnMethod;
    }

    public void setEarnMethod(EarnMethod earnMethod) {
        this.earnMethod = earnMethod;
    }
}
