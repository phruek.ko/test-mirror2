package com.eggdigital.rpp.edcapp.dto;

import com.eggdigital.rpp.edcapp.dto.base.AbstractTransactionResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by jaruwan.yen on 10/12/2017 AD.
 */
public class RedeemByCampaignData extends AbstractTransactionResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("truecard")
    private String trueCardNumber;

    @JsonProperty("mastercard")
    private String masterCardNumber;

    @JsonProperty("remain_privilege")
    private Integer remainPrivilege;

    @JsonProperty("total_privilege")
    private Integer totalPrivilege;

    @JsonProperty("point_balance")
    private Integer pointBalance;

    @JsonProperty("points_used")
    private Integer pointsUsed;

    @JsonProperty("marketing_message")
    private List<String> marketingMessages;

    @JsonProperty("reward_code")
    private String rewardCode;

    @JsonProperty("trace")
    private String trace;

    @JsonProperty("reward")
    private String reward;

    public RedeemByCampaignData() {
    }

    public String getTrueCardNumber() {
        return trueCardNumber;
    }

    public void setTrueCardNumber(String trueCardNumber) {
        this.trueCardNumber = trueCardNumber;
    }

    public String getMasterCardNumber() {
        return masterCardNumber;
    }

    public void setMasterCardNumber(String masterCardNumber) {
        this.masterCardNumber = masterCardNumber;
    }

    public Integer getRemainPrivilege() {
        return remainPrivilege;
    }

    public void setRemainPrivilege(Integer remainPrivilege) {
        this.remainPrivilege = remainPrivilege;
    }

    public Integer getTotalPrivilege() {
        return totalPrivilege;
    }

    public void setTotalPrivilege(Integer totalPrivilege) {
        this.totalPrivilege = totalPrivilege;
    }

    public Integer getPointBalance() {
        return pointBalance;
    }

    public void setPointBalance(Integer pointBalance) {
        this.pointBalance = pointBalance;
    }

    public Integer getPointsUsed() {
        return pointsUsed;
    }

    public void setPointsUsed(Integer pointsUsed) {
        this.pointsUsed = pointsUsed;
    }

    public List<String> getMarketingMessages() {
        if (marketingMessages == null) {
            marketingMessages = new LinkedList<>();
        }
        return marketingMessages;
    }

    public void setMarketingMessages(List<String> marketingMessages) {
        this.marketingMessages = marketingMessages;
    }

    public void addMarketingMessage(String message) {
        getMarketingMessages().add(message);
    }

    public String getRewardCode() {
        return rewardCode;
    }

    public void setRewardCode(String rewardCode) {
        this.rewardCode = rewardCode;
    }

    public String getTrace() {
        return trace;
    }

    public void setTrace(String trace) {
        this.trace = trace;
    }

    public String getReward() {
        return reward;
    }

    public void setReward(String reward) {
        this.reward = reward;
    }
}
