package com.eggdigital.rpp.edcapp.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by bombay on 11/8/2017 AD.
 */
public class CampaignDto {

    @JsonProperty("total")
    private String total;

    @JsonProperty("campaign_code")
    private String campaignCode;

    @JsonProperty("campaign_name")
    private String campaignName;

    public CampaignDto() {
    }

    public CampaignDto(String total, String campaignCode, String campaignName) {
        this.total = total;
        this.campaignCode = campaignCode;
        this.campaignName = campaignName;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getCampaignCode() {
        return campaignCode;
    }

    public void setCampaignCode(String campaignCode) {
        this.campaignCode = campaignCode;
    }

    public String getCampaignName() {
        return campaignName;
    }

    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }
}
