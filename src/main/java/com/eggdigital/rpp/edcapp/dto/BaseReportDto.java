package com.eggdigital.rpp.edcapp.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by bombay on 11/8/2017 AD.
 */
public abstract class BaseReportDto {

    @JsonProperty("total")
    private String total;

    @JsonProperty("amount")
    private String amount;

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
