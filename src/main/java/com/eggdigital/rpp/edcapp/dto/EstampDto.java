package com.eggdigital.rpp.edcapp.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EstampDto {

    @JsonProperty("earnTotal")
    private String totalEarnEstamp;

    @JsonProperty("markuseTotal")
    private String totalMarkuseEstamp;

    public EstampDto(String totalEarnEstamp, String totalMarkuseEstamp) {
        this.totalEarnEstamp = totalEarnEstamp;
        this.totalMarkuseEstamp = totalMarkuseEstamp;
    }

    public EstampDto(){

    }

    public String getTotalEarnEstamp() {
        return totalEarnEstamp;
    }

    public void setTotalEarnEstamp(String totalEarnEstamp) {
        this.totalEarnEstamp = totalEarnEstamp;
    }

    public String getTotalMarkuseEstamp() {
        return totalMarkuseEstamp;
    }

    public void setTotalMarkuseEstamp(String totalMarkuseEstamp) {
        this.totalMarkuseEstamp = totalMarkuseEstamp;
    }
}