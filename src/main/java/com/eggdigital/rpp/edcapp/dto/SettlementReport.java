package com.eggdigital.rpp.edcapp.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

/**
 * Created by bombay on 11/1/2017 AD.
 */
public class SettlementReport {

    @JsonProperty("brand_id")
    private String brandId;

    @JsonProperty("brand_name")
    private String brandName;

    @JsonProperty("outlet_id")
    private String outletId;

    @JsonProperty("terminal_id")
    private String terminalId;

    @JsonProperty("batch_id")
    private String batchId;

    @JsonProperty("payment")
    private PaymentDto paymentDto;

    @JsonProperty("earn")
    private EarnDto earnDto;

    @JsonProperty("redeem")
    private RedeemDto redeemDto;

    @JsonProperty("e_stamp")
    private EstampDto estampDto;

    @JsonProperty("eCouponTotal")
    private String eCoupon;

    @JsonProperty("settlement_date")
    private Date settlementDate = new Date();

    @JsonProperty("trueyou_id")
    private String trueyouId;

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getOutletId() {
        return outletId;
    }

    public void setOutletId(String outletId) {
        this.outletId = outletId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public PaymentDto getPaymentDto() {
        return paymentDto;
    }

    public void setPaymentDto(PaymentDto paymentDto) {
        this.paymentDto = paymentDto;
    }

    public EarnDto getEarnDto() {
        return earnDto;
    }

    public void setEarnDto(EarnDto earnDto) {
        this.earnDto = earnDto;
    }

    public RedeemDto getRedeemDto() {
        return redeemDto;
    }

    public void setRedeemDto(RedeemDto redeemDto) {
        this.redeemDto = redeemDto;
    }

    public Date getSettlementDate() {
        return settlementDate;
    }

    public void setSettlementDate(Date settlementDate) {
        this.settlementDate = settlementDate;
    }

    public String getTrueyouId() {
        return trueyouId;
    }

    public void setTrueyouId(String trueyouId) {
        this.trueyouId = trueyouId;
    }

    public EstampDto getEstampDto() {
        return estampDto;
    }

    public void setEstampDto(EstampDto estampDto) {
        this.estampDto = estampDto;
    }

    public String geteCoupon() {
        return eCoupon;
    }

    public void seteCoupon(String eCoupon) {
        this.eCoupon = eCoupon;
    }
}
