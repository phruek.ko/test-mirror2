package com.eggdigital.rpp.edcapp.dto;

import com.eggdigital.rpp.edcapp.domain.Currency;
import com.eggdigital.rpp.edcapp.domain.TransactionType;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

/**
 * Created by bombay on 10/20/2017 AD.
 */
public class ReceiptDto {

    @JsonProperty("terminal_id")
    private String terminalId;

    @JsonProperty("tx_ref_id")
    private String txRefId;

    @JsonProperty("trace_id")
    private String traceId;

    @JsonProperty("batch_id")
    private String batchId;

    @JsonProperty("transaction_date")
    private Date transactionDate;

    @JsonProperty("brand_id")
    private String brandId;

    @JsonProperty("brand_name_en")
    private String brandNameEn;

    @JsonProperty("brand_name_th")
    private String brandNameTh;

    @JsonProperty("outlet_id")
    private String outletId;

    @JsonProperty("outlet_name_th")
    private String outletNameTh;

    @JsonProperty("outlet_name_en")
    private String outletNameEn;

    @JsonProperty("transaction_type")
    private TransactionType transactionType;

    private String amount;

    @JsonProperty("payment_code")
    private String paymentCode;

    @JsonProperty("customer_ref")
    private String customerRef;

    @JsonProperty("campaign_code")
    private String campaignCode;

    @JsonProperty("coupon_Code")
    private String couponCode;

    private Currency currency;

    private Integer point;

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public String getTxRefId() {
        return txRefId;
    }

    public void setTxRefId(String txRefId) {
        this.txRefId = txRefId;
    }

    public String getTraceId() {
        return traceId;
    }

    public void setTraceId(String traceId) {
        this.traceId = traceId;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getBrandNameEn() {
        return brandNameEn;
    }

    public void setBrandNameEn(String brandNameEn) {
        this.brandNameEn = brandNameEn;
    }

    public String getBrandNameTh() {
        return brandNameTh;
    }

    public void setBrandNameTh(String brandNameTh) {
        this.brandNameTh = brandNameTh;
    }

    public String getOutletId() {
        return outletId;
    }

    public void setOutletId(String outletId) {
        this.outletId = outletId;
    }

    public String getOutletNameTh() {
        return outletNameTh;
    }

    public void setOutletNameTh(String outletNameTh) {
        this.outletNameTh = outletNameTh;
    }

    public String getOutletNameEn() {
        return outletNameEn;
    }

    public void setOutletNameEn(String outletNameEn) {
        this.outletNameEn = outletNameEn;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPaymentCode() {
        return paymentCode;
    }

    public void setPaymentCode(String paymentCode) {
        this.paymentCode = paymentCode;
    }

    public String getCustomerRef() {
        return customerRef;
    }

    public void setCustomerRef(String customerRef) {
        this.customerRef = customerRef;
    }

    public String getCampaignCode() {
        return campaignCode;
    }

    public void setCampaignCode(String campaignCode) {
        this.campaignCode = campaignCode;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Integer getPoint() {
        return point;
    }

    public void setPoint(Integer point) {
        this.point = point;
    }
}
