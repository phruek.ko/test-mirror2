package com.eggdigital.rpp.edcapp.dto.base;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Created by wanatchapong
 */
public class ResponseBody<T> {

    @JsonProperty("status")
    private ResponseStatus status;

    @JsonProperty("data")
    private T data;

    public ResponseBody() {
    }

    public ResponseBody(Builder<T> builder) {
        this.status = builder.status;
        this.data = builder.data;
    }

    public ResponseBody(ResponseStatus status, T data) {
        this.status = status;
        this.data = data;
    }

    public static <T> Builder<T> getBuilder() {
        return new Builder<>();
    }

    public ResponseStatus getStatus() {
        return status;
    }

    public void setStatus(ResponseStatus status) {
        this.status = status;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("status", status)
                .append("data", data)
                .toString();
    }

    public static class Builder<T> {

        private ResponseStatus status;
        private T data;

        private Builder() {
        }

        public Builder<T> status(ResponseStatus status) {
            this.status = status;
            return this;
        }

        public Builder<T> status(String code, String message) {
            this.status = new ResponseStatus(code, message);
            return this;
        }

        public Builder<T> data(T data) {
            this.data = data;
            return this;
        }

        public ResponseBody<T> build() {
            return new ResponseBody<>(this);
        }
    }
}
