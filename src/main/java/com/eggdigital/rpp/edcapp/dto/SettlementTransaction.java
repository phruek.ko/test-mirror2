package com.eggdigital.rpp.edcapp.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by bombay on 11/14/2017 AD.
 */
public class SettlementTransaction {

    @JsonProperty("message")
    private String message;

    public SettlementTransaction() {
    }

    public SettlementTransaction(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
