package com.eggdigital.rpp.edcapp.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by bombay on 12/1/2017 AD.
 */
public class DetailSummaryEstamp {

    @JsonProperty("earn")
    private DetailSummaryObject earn;

    @JsonProperty("markuse")
    private DetailSummaryObject markuse;

    public DetailSummaryEstamp() {
    }

    public DetailSummaryEstamp(DetailSummaryObject earn, DetailSummaryObject markuse) {
        this.earn = earn;
        this.markuse = markuse;
    }

    public DetailSummaryObject getEarn() {
        return earn;
    }

    public void setEarn(DetailSummaryObject earn) {
        this.earn = earn;
    }

    public DetailSummaryObject getMarkuse() {
        return markuse;
    }

    public void setMarkuse(DetailSummaryObject markuse) {
        this.markuse = markuse;
    }
}
