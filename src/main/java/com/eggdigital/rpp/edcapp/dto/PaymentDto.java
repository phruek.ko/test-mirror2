package com.eggdigital.rpp.edcapp.dto;

import com.eggdigital.rpp.edcapp.domain.PaymentMethod;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by bombay on 11/8/2017 AD.
 */
public class PaymentDto extends BaseReportDto {

    @JsonProperty("payment_method")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private PaymentMethod paymentMethod;

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }
}
