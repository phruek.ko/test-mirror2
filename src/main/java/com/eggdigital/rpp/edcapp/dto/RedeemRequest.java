package com.eggdigital.rpp.edcapp.dto;

import com.eggdigital.rpp.edcapp.dto.base.AbstractRequest;
import com.eggdigital.rpp.edcapp.validation.MobileNumberField;
import com.eggdigital.rpp.edcapp.validation.ValidateGroups;
import com.eggdigital.rpp.edcapp.validation.constraints.RequiredField;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by jaruwan.yen on 10/12/2017 AD.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RedeemRequest extends AbstractRequest {

    @JsonProperty("tx_ref_id")
    private String txRefId;

    @JsonProperty("acc_type")
    @RequiredField(field = "account type", groups = {ValidateGroups.RedeemByCampaign.class})
    private String accType;

    @JsonProperty("acc_value")
    @RequiredField(field = "account value", groups = {ValidateGroups.RedeemByCampaign.class})
    private String accValue;

    @JsonProperty("campaign_code")
    @RequiredField(field = "campaign code", groups = {ValidateGroups.RedeemByCampaign.class, ValidateGroups.RedeemByCode.class})
    private String campaignCode;

    @JsonProperty("reward_code")
    @RequiredField(field = "reward code", groups = {ValidateGroups.RedeemByCode.class})
    private String rewardCode;

    @JsonProperty("action")
    private String action;

    @JsonProperty("campaign_type")
    private String campaignType;

    @JsonProperty("mobile")
    @MobileNumberField(field = "mobile", groups = {ValidateGroups.RedeemByCampaign.class})
    private String mobile;

    public String getTxRefId() {
        return txRefId;
    }

    public void setTxRefId(String txRefId) {
        this.txRefId = txRefId;
    }

    public String getAccType() {
        return accType;
    }

    public void setAccType(String accType) {
        this.accType = accType;
    }

    public String getAccValue() {
        return accValue;
    }

    public void setAccValue(String accValue) {
        this.accValue = accValue;
    }

    public String getCampaignCode() {
        return campaignCode;
    }

    public void setCampaignCode(String campaignCode) {
        this.campaignCode = campaignCode;
    }

    public String getRewardCode() {
        return rewardCode;
    }

    public void setRewardCode(String rewardCode) {
        this.rewardCode = rewardCode;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getCampaignType() {
        return campaignType;
    }

    public void setCampaignType(String campaignType) {
        this.campaignType = campaignType;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @Override
    public String toString() {
        return "RedeemRequest{" +
                "txRefId='" + txRefId + '\'' +
                ", accType='" + accType + '\'' +
                ", accValue='" + accValue + '\'' +
                ", campaignCode='" + campaignCode + '\'' +
                ", rewardCode='" + rewardCode + '\'' +
                ", action='" + action + '\'' +
                ", campaignType='" + campaignType + '\'' +
                ", mobile='" + mobile + '\'' +
                '}';
    }
}
