package com.eggdigital.rpp.edcapp.dto;

import com.eggdigital.rpp.edcapp.domain.Transaction;
import com.eggdigital.rpp.edcapp.dto.base.AbstractTransactionResponse;
import com.eggdigital.rpp.edcapp.trueyou.TrueCardStatus;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

public class CardInfoSlip extends AbstractTransactionResponse implements Serializable {

    @JsonProperty("card_no")
    private String cardNo;

    @JsonProperty("card_type")
    private String cardType;

    @JsonProperty("card_status")
    private String cardStatus;

    @JsonProperty("card_expire")
    private String cardExpire;

    @JsonProperty("customer_name")
    private String customerName;
    /**
     * Default = 0 for Sycapt suggestion
     */
    @JsonProperty("point_balance")
    private int pointBalance;

    @JsonProperty("point_pockets")
    private List<CardInfo.PointPocket> pointPockets;
    /**
     * Default = 0 for Sycapt suggestion
     */
    @JsonProperty("slip_type_id")
    private int slipTypeId;

    @JsonProperty("slip_type_description")
    private String slipTypeDescription;

    public CardInfoSlip() {
        /**
         * Default value for Sycapt suggestion
         */
        setTraceId("");
        setBatchId("");
        setTxRefId("");
        this.cardNo = "";
        this.cardType = "";
        this.cardStatus = TrueCardStatus.UNDEFINED.getStatus();
        this.cardExpire = "12/00";
        this.customerName = "";
        this.slipTypeDescription = "";
    }

    public CardInfoSlip(Transaction tx, CardInfo decorated) {
        setTransaction(tx);
        this.cardNo = decorated.getCardNo();
        this.cardType = decorated.getCardType();
        this.cardStatus = decorated.getCardStatus();
        this.cardExpire = decorated.getCardExpire();
        this.customerName = decorated.getCustomerName();
        this.pointBalance = decorated.getPointBalance();
        this.pointPockets = decorated.getPointPockets();
        this.slipTypeId = decorated.getSlipTypeId();
        this.slipTypeDescription = decorated.getSlipTypeDescription();
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getCardStatus() {
        return cardStatus;
    }

    public void setCardStatus(String cardStatus) {
        this.cardStatus = cardStatus;
    }

    public String getCardExpire() {
        return cardExpire;
    }

    public void setCardExpire(String cardExpire) {
        this.cardExpire = cardExpire;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public int getPointBalance() {
        return pointBalance;
    }

    public void setPointBalance(int pointBalance) {
        this.pointBalance = pointBalance;
    }

    public List<CardInfo.PointPocket> getPointPockets() {
        return pointPockets;
    }

    public void setPointPockets(List<CardInfo.PointPocket> pointPockets) {
        this.pointPockets = pointPockets;
    }

    public int getSlipTypeId() {
        return slipTypeId;
    }

    public void setSlipTypeId(int slipTypeId) {
        this.slipTypeId = slipTypeId;
    }

    public String getSlipTypeDescription() {
        return slipTypeDescription;
    }

    public void setSlipTypeDescription(String slipTypeDescription) {
        this.slipTypeDescription = slipTypeDescription;
    }
}
