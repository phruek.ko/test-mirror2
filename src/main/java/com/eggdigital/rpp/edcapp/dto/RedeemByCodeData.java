package com.eggdigital.rpp.edcapp.dto;

import com.eggdigital.rpp.edcapp.dto.base.AbstractTransactionResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by jaruwan.yen on 10/16/2017 AD.
 */
public class RedeemByCodeData extends AbstractTransactionResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("card_no")
    private String cardNo;

    @JsonProperty("remark1")
    private String remark1;

    @JsonProperty("remark2")
    private String remark2;

    @JsonProperty("remark3")
    private String remark3;

    @JsonProperty("remain_privilege")
    private Integer remainPrivilege;

    @JsonProperty("point_balance")
    private Integer pointBalance;

    @JsonProperty("discount_amt")
    private BigDecimal discountAmount;

    @JsonProperty("reward_code")
    private String rewardCode;

    @JsonProperty("trace")
    private String trace;

    @JsonProperty("reward")
    private String reward;

    public RedeemByCodeData() {
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getRemark1() {
        return remark1;
    }

    public void setRemark1(String remark1) {
        this.remark1 = remark1;
    }

    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2;
    }

    public String getRemark3() {
        return remark3;
    }

    public void setRemark3(String remark3) {
        this.remark3 = remark3;
    }

    public Integer getRemainPrivilege() {
        return remainPrivilege;
    }

    public void setRemainPrivilege(Integer remainPrivilege) {
        this.remainPrivilege = remainPrivilege;
    }

    public Integer getPointBalance() {
        return pointBalance;
    }

    public void setPointBalance(Integer pointBalance) {
        this.pointBalance = pointBalance;
    }

    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getRewardCode() {
        return rewardCode;
    }

    public void setRewardCode(String rewardCode) {
        this.rewardCode = rewardCode;
    }

    public String getTrace() {
        return trace;
    }

    public void setTrace(String trace) {
        this.trace = trace;
    }

    public String getReward() {
        return reward;
    }

    public void setReward(String reward) {
        this.reward = reward;
    }
}
