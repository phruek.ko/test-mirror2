package com.eggdigital.rpp.edcapp.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by bombay on 12/1/2017 AD.
 */
public class DetailSummaryReportNew {

    @JsonProperty("trueyou_id")
    private String trueyouId;

    @JsonProperty("terminal_id")
    private String terminalId;

    @JsonProperty("redeem")
    private DetailSummaryObject redeem;

    @JsonProperty("earn")
    private DetailSummaryObject earn;

    @JsonProperty("payment")
    private DetailSummaryObject payment;

    @JsonProperty("void")
    private DetailSummaryVoid voidDto;

    @JsonProperty("e_stamp")
    private DetailSummaryEstamp estamp;

    @JsonProperty("eCoupon")
    private DetailSummaryObject eCoupon;

    public DetailSummaryReportNew() {
    }

    public DetailSummaryReportNew(String trueyouId, String terminalId, DetailSummaryObject eCoupon, DetailSummaryObject redeem, DetailSummaryEstamp estamp, DetailSummaryObject earn, DetailSummaryObject payment, DetailSummaryVoid voidDto) {
        this.trueyouId = trueyouId;
        this.terminalId = terminalId;
        this.redeem = redeem;
        this.earn = earn;
        this.payment = payment;
        this.voidDto = voidDto;
        this.estamp = estamp;
        this.eCoupon = eCoupon;
    }

    public String getTrueyouId() {
        return trueyouId;
    }

    public void setTrueyouId(String trueyouId) {
        this.trueyouId = trueyouId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public DetailSummaryObject getRedeem() {
        return redeem;
    }

    public void setRedeem(DetailSummaryObject redeem) {
        this.redeem = redeem;
    }

    public DetailSummaryObject getEarn() {
        return earn;
    }

    public void setEarn(DetailSummaryObject earn) {
        this.earn = earn;
    }

    public DetailSummaryObject getPayment() {
        return payment;
    }

    public void setPayment(DetailSummaryObject payment) {
        this.payment = payment;
    }

    public DetailSummaryVoid getVoidDto() {
        return voidDto;
    }

    public void setVoidDto(DetailSummaryVoid voidDto) {
        this.voidDto = voidDto;
    }

    public DetailSummaryEstamp getEstamp() {
        return estamp;
    }

    public void setEstamp(DetailSummaryEstamp estamp) {
        this.estamp = estamp;
    }

    public DetailSummaryObject geteCoupon() {
        return eCoupon;
    }

    public void seteCoupon(DetailSummaryObject eCoupon) {
        this.eCoupon = eCoupon;
    }
}
