package com.eggdigital.rpp.edcapp.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by bombay on 12/1/2017 AD.
 */
public class DetailSummaryVoid extends DetailSummaryObject {

    @JsonProperty("redeem")
    private DetailSummaryObject redeem;

    @JsonProperty("earn")
    private DetailSummaryObject earn;

    @JsonProperty("payment")
    private DetailSummaryObject payment;

    public DetailSummaryVoid() {
    }

    public DetailSummaryVoid(DetailSummaryObject redeem, DetailSummaryObject earn, DetailSummaryObject payment) {
        this.redeem = redeem;
        this.earn = earn;
        this.payment = payment;
    }

    public DetailSummaryObject getRedeem() {
        return redeem;
    }

    public void setRedeem(DetailSummaryObject redeem) {
        this.redeem = redeem;
    }

    public DetailSummaryObject getEarn() {
        return earn;
    }

    public void setEarn(DetailSummaryObject earn) {
        this.earn = earn;
    }

    public DetailSummaryObject getPayment() {
        return payment;
    }

    public void setPayment(DetailSummaryObject payment) {
        this.payment = payment;
    }
}
