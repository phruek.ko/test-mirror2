package com.eggdigital.rpp.edcapp.helper;

import com.eggdigital.rpp.edcapp.dto.base.ResponseStatus;
import com.eggdigital.rpp.edcapp.handler.ApiError;
import com.eggdigital.rpp.edcapp.handler.ApiErrorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.UnknownHttpStatusCodeException;

/**
 * Created by jaruwan.yen on 10/12/2017 AD.
 */
@Component
public class RestTemplateExceptionHelper {
    private final Logger log = LoggerFactory.getLogger(RestTemplateExceptionHelper.class);

    @Autowired
    private ApiErrorHelper apiErrorHelper;

    public ResponseStatus getResponseStatus(Exception ex) {
        if (ex instanceof ResourceAccessException) {
            ResourceAccessException resourceAccessEx = (ResourceAccessException) ex;
            log.error("ResourceAccessException -> message : {}", resourceAccessEx.getMessage());
            return new ResponseStatus(HttpStatus.REQUEST_TIMEOUT, ex.getMessage());

        } else if (ex instanceof HttpStatusCodeException) {
            HttpStatusCodeException statusCodeEx = (HttpStatusCodeException) ex;
            log.error("HttpStatusCodeException -> status code : {}, status text : {}, body : {}", statusCodeEx.getRawStatusCode(), statusCodeEx.getStatusText(), statusCodeEx.getResponseBodyAsString());
            return new ResponseStatus(statusCodeEx.getStatusCode().toString(), ex.getMessage());

        } else if (ex instanceof UnknownHttpStatusCodeException) {
            UnknownHttpStatusCodeException unknownStatusEx = (UnknownHttpStatusCodeException) ex;
            log.error("UnknownHttpStatusCodeException -> status code : {}, body : {}", unknownStatusEx.getRawStatusCode(), unknownStatusEx.getResponseBodyAsString());
            return new ResponseStatus(Integer.toString(unknownStatusEx.getRawStatusCode()), unknownStatusEx.getMessage());

        } else if (ex instanceof ApiErrorException) {
            ApiErrorException apiErrorException = (ApiErrorException) ex;
            ResponseStatus status = apiErrorException.getApiError().getStatus();
            log.error("ApiErrorException -> status code : {}, body : {}", status.getCode(), status.getMessage());
            return status;
        }
        log.error(null, ex);
        return new ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
    }

    public ApiError getApiError(Exception ex) {
        ApiError.Builder builder = ApiError.builder();

        if (ex instanceof ResourceAccessException) {
            ResourceAccessException resourceAccessEx = (ResourceAccessException) ex;
            log.error("ResourceAccessException -> message : {}", resourceAccessEx.getMessage());
            builder.status(HttpStatus.REQUEST_TIMEOUT);

        } else if (ex instanceof HttpStatusCodeException) {
            HttpStatusCodeException statusCodeEx = (HttpStatusCodeException) ex;
            log.error("HttpStatusCodeException -> status code : {}, status text : {}, body : {}", statusCodeEx.getRawStatusCode(), statusCodeEx.getStatusText(), statusCodeEx.getResponseBodyAsString());
            builder.status(statusCodeEx.getStatusCode())
                    .errors(apiErrorHelper.getPropertyErrorsFromJsonResponse(statusCodeEx.getResponseBodyAsString()));

        } else if (ex instanceof UnknownHttpStatusCodeException) {
            UnknownHttpStatusCodeException unknownStatusEx = (UnknownHttpStatusCodeException) ex;
            log.error("UnknownHttpStatusCodeException -> status code : {}, body : {}", unknownStatusEx.getRawStatusCode(), unknownStatusEx.getResponseBodyAsString());
            builder.status(Integer.toString(unknownStatusEx.getRawStatusCode()), unknownStatusEx.getStatusText())
                    .errors(apiErrorHelper.getPropertyErrorsFromJsonResponse(unknownStatusEx.getResponseBodyAsString()));

        } else if (ex instanceof ApiErrorException) {
            ApiErrorException apiErrorException = (ApiErrorException) ex;
            ResponseStatus status = apiErrorException.getApiError().getStatus();
            log.error("ApiErrorException -> status code : {}, body : {}", status.getCode(), status.getMessage());
            return apiErrorException.getApiError();

        } else {
            log.error(null, ex);
            builder.status(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return builder.build();
    }

    public ApiErrorException getApiErrorException(Exception ex) {
        String message = ex.getMessage();

        if (ex instanceof ResourceAccessException) {
            ResourceAccessException resourceAccessEx = (ResourceAccessException) ex;
            log.error("ResourceAccessException -> message : {}", resourceAccessEx.getMessage());
            return new ApiErrorException(message,
                    ApiError.builder()
                            .status(HttpStatus.REQUEST_TIMEOUT)
                            .build()
            );

        } else if (ex instanceof HttpStatusCodeException) {
            HttpStatusCodeException statusCodeEx = (HttpStatusCodeException) ex;
            log.error("HttpStatusCodeException -> status code : {}, status text : {}, body : {}", statusCodeEx.getRawStatusCode(), statusCodeEx.getStatusText(), statusCodeEx.getResponseBodyAsString());
            return new ApiErrorException(message,
                    ApiError.builder()
                            .status(statusCodeEx.getStatusCode())
                            .errors(apiErrorHelper.getPropertyErrorsFromJsonResponse(statusCodeEx.getResponseBodyAsString()))
                            .build()
            );

        } else if (ex instanceof UnknownHttpStatusCodeException) {
            UnknownHttpStatusCodeException unknownStatusEx = (UnknownHttpStatusCodeException) ex;
            log.error("UnknownHttpStatusCodeException -> status code : {}, body : {}", unknownStatusEx.getRawStatusCode(), unknownStatusEx.getResponseBodyAsString());
            return new ApiErrorException(message,
                    ApiError.builder()
                            .status(Integer.toString(unknownStatusEx.getRawStatusCode()), unknownStatusEx.getStatusText())
                            .errors(apiErrorHelper.getPropertyErrorsFromJsonResponse(unknownStatusEx.getResponseBodyAsString()))
                            .build()
            );

        } else if (ex instanceof ApiErrorException) {
            return (ApiErrorException) ex;

        } else {
            log.error(null, ex);
            return new ApiErrorException(message,
                    ApiError.builder()
                            .status(HttpStatus.INTERNAL_SERVER_ERROR)
                            .build()
            );
        }
    }
}
