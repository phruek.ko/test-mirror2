package com.eggdigital.rpp.edcapp.helper;

import com.eggdigital.common.util.StreamUtils;
import com.eggdigital.rpp.edcapp.handler.ApiError;
import com.eggdigital.rpp.edcapp.handler.PropertyError;
import com.eggdigital.rpp.edcapp.ms.dto.AbstractMsResponse;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by jaruwan.yen on 10/12/2017 AD.
 */
@Component
public class ApiErrorHelper {
    private final Logger log = LoggerFactory.getLogger(ApiErrorHelper.class);

    @Autowired
    private ObjectMapper mapper;

    public List<PropertyError> getPropertyErrorsFromJsonResponse(String json) {
        try {
            JsonNode root = mapper.readTree(json);
            JsonNode multiErrors = root.path("errors");
            if (!multiErrors.isMissingNode()) {
                return getPropertyErrors(multiErrors);
            }
            JsonNode singleError = root.path("error");
            if (!singleError.isMissingNode()) {
                return getPropertyErrors(singleError);
            }
        } catch (IOException ex) {
            log.error(null, ex);
        }

        return Collections.emptyList();
    }

    public <T extends AbstractMsResponse<?>> ApiError getApiError(T responseClass) {
        return new ApiError(responseClass.getStatus(), responseClass.getErrors());
    }

    private List<PropertyError> getPropertyErrors(JsonNode node) {
        return StreamUtils.iteratorToStream(node.elements(), false)
                .map(n -> {
                    String property = n.path("property").asText();
                    String message = n.path("message").asText();
                    return new PropertyError(property, message);
                })
                .collect(Collectors.toList());
    }
}
