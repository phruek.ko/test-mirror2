package com.eggdigital.rpp.edcapp.config;

import com.eggdigital.rpp.edcapp.config.prop.SecurityProps;
import com.eggdigital.rpp.edcapp.service.CryptoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class ApplicationStartup implements ApplicationListener<ApplicationReadyEvent> {

    private final Logger log = LoggerFactory.getLogger(ApplicationStartup.class);

    @Autowired
    private CryptoService cryptoService;

    @Autowired
    private SecurityProps securityProperties;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        log.info("on application ready load security private key");

//        BeanDefinitionRegistry beanFactory = (BeanDefinitionRegistry) event.getApplicationContext().getBeanFactory();
//        beanFactory.registerBeanDefinition("securityProperties",
//                BeanDefinitionBuilder.genericBeanDefinition(SecurityProperties.class)
//                        .setScope(BeanDefinition.SCOPE_SINGLETON)
//                        .setAutowireMode(AUTOWIRE_BY_TYPE)
//                        .addPropertyValue("privateKey", cryptoService.getPrivateKey())
//                        .getBeanDefinition()
//        );

        securityProperties.setPrivateKey(cryptoService.getPrivateKey());
    }
}
