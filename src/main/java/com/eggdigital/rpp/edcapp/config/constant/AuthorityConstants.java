package com.eggdigital.rpp.edcapp.config.constant;

/**
 * Constants for Spring Security authorities.
 */
public final class AuthorityConstants {

    public static final String ANONYMOUS = "ROLE_ANONYMOUS";

    private AuthorityConstants() {
    }
}
