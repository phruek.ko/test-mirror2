package com.eggdigital.rpp.edcapp.config;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.TimeUnit;

/**
 * Created by wanatchapong
 */
@Configuration
public class RestTemplateConfig {

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate(customHttpRequestFactory());
    }

    private ClientHttpRequestFactory customHttpRequestFactory() {
        RequestConfig config = RequestConfig.custom()
                // (http.connection.timeout) – the time to establish the connection with the remote host
                .setConnectTimeout((int) TimeUnit.SECONDS.toMillis(15))
                // (http.socket.timeout) – the time waiting for data – after the connection was established; maximum time of inactivity between two data packets
                .setSocketTimeout((int) TimeUnit.SECONDS.toMillis(60))
                // (http.connection-manager.timeout) – the time to wait for a connection from the connection manager/pool
                .setConnectionRequestTimeout((int) TimeUnit.SECONDS.toMillis(10))
                .build();

        CloseableHttpClient httpClient = HttpClients
                .custom()
                .setDefaultRequestConfig(config)
                .setSSLHostnameVerifier(new NoopHostnameVerifier())
                .build();

        return new HttpComponentsClientHttpRequestFactory(httpClient);
    }
}
