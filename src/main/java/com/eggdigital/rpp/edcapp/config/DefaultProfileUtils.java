package com.eggdigital.rpp.edcapp.config;

import com.eggdigital.rpp.edcapp.config.constant.ProfileConstants;
import org.springframework.boot.SpringApplication;
import org.springframework.core.env.Environment;

import java.util.HashMap;
import java.util.Map;

public final class DefaultProfileUtils {

    private static final String SPRING_PROFILES_DEFAULT = "spring.profiles.default";

    private DefaultProfileUtils() {
    }

    /**
     * Set a default to use when no profile is configured.
     *
     * @param app the spring application
     */
    public static void addDefaultProfile(SpringApplication app) {
        Map<String, Object> defProperties = new HashMap<>();
        /*
        * The default profile to use when no other profiles are defined
        * This cannot be set in the <code>application.yml</code> file.
        * See https://github.com/spring-projects/spring-boot/issues/1219
        */
        defProperties.put(SPRING_PROFILES_DEFAULT, ProfileConstants.DEVELOPMENT);
        app.setDefaultProperties(defProperties);
    }

    /**
     * Get the profiles that are applied else get default profiles.
     */
    public static String[] getActiveProfiles(Environment env) {
        String[] profiles = env.getActiveProfiles();
        if (profiles.length == 0) {
            return env.getDefaultProfiles();
        }
        return profiles;
    }
}
