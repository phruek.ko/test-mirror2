package com.eggdigital.rpp.edcapp.config.constant;

public final class SecurityConstants {

    public static final String ANONYMOUS_ROLE = "ROLE_ANONYMOUS";
    public static final String ANONYMOUS_USER = "anonymous";

}
