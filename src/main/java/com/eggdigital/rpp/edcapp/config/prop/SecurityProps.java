package com.eggdigital.rpp.edcapp.config.prop;

import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.ApplicationScope;

import java.security.PrivateKey;

@ApplicationScope
@Component
public class SecurityProps {

    private PrivateKey privateKey;

    public void setPrivateKey(PrivateKey privateKey) {
        this.privateKey = privateKey;
    }

    public PrivateKey getPrivateKey() {
        return privateKey;
    }
}
