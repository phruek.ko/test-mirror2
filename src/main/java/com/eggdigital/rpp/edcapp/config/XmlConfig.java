package com.eggdigital.rpp.edcapp.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

/**
 * Created by wanatchapong on 12/17/2016 AD.
 */
@Configuration
public class XmlConfig {

    @Bean
    public Jaxb2Marshaller jaxb2Marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setPackagesToScan("com.eggdigital.rpp.bs");
        return marshaller;
    }
}
