package com.eggdigital.rpp.edcapp.config.prop;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by wanatchapong
 */
@Component
@ConfigurationProperties(prefix = "rpp")
public class RppProperties {

    private Application application;

    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    public static class Application {

        private String version;

        private String buildTimestamp;

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }

        public String getBuildTimestamp() {
            return buildTimestamp;
        }

        public void setBuildTimestamp(String buildTimestamp) {
            this.buildTimestamp = buildTimestamp;
        }
    }

}
