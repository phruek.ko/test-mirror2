package com.eggdigital.rpp.edcapp.config.aop;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class ControllerAspect {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ObjectMapper objectMapper;

    @Before("execution(* com.eggdigital.rpp.edcapp.controller.*.*(..))")
    public void logBeforePaymentController(JoinPoint joinPoint) {
        log.info("request [{}] : {}",
                joinPoint.getSignature().getName(), getJson(joinPoint.getArgs()));
    }

    @AfterReturning(
            value = "execution(* com.eggdigital.rpp.edcapp.controller.*.*(..))",
            returning = "response")
    public void logAfterPaymentController(JoinPoint joinPoint, Object response) {

        log.info("response [{}] : {}",
                joinPoint.getSignature().getName(), getJson(response));
    }

    public String getJson(Object o) {
        String json = null;
        try {
            json = objectMapper.writeValueAsString(o);
        } catch (JsonProcessingException e) {
            log.error("{}", e);
        }
        return json;
    }
}
