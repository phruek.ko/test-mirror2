package com.eggdigital.rpp.edcapp.config;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
public class EdcMessageConfig {

    public static class EdcDisplay {

        private String code;
        private String message;

        public EdcDisplay(String code, String message) {
            this.code = code;
            this.message = message;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }


    @Bean
    public HashMap<String, EdcDisplay> getEdcDisplayMessages() {
        HashMap<String, EdcDisplay> mappingMessage = new HashMap<>();
        mappingMessage.put("500", new EdcDisplay("0500", "ข้อมูลไม่ถูกต้อง หรือ ระบบไม่สามารถดำเนินการได้ขณะนี้"));
        mappingMessage.put("800", new EdcDisplay("0800", "ระบบไม่สามารถดำเนินการณ์ได้ในขณะนี้"));
        mappingMessage.put("734", new EdcDisplay("0734", "บัตรทรูการ์ดของท่านหมดอายุแล้ว"));
        mappingMessage.put("732", new EdcDisplay("0732", "ใช้สิทธิครบตามจำนวนที่กำหนด"));
        mappingMessage.put("729", new EdcDisplay("0729", "บัตรทรูการ์ดของท่านหมดอายุแล้ว"));
        mappingMessage.put("727", new EdcDisplay("0727", "สิทธินี้สำหรับทรูเรดการ์ดเท่านั้น"));
        mappingMessage.put("726", new EdcDisplay("0726", "สิทธินี้สำหรับทรูแบล็คการ์ดเท่านั้น"));
        mappingMessage.put("725", new EdcDisplay("0725", "ระบบไม่สามารถดำเนินการณ์ได้ในขณะนี้"));
        mappingMessage.put("724", new EdcDisplay("0724", "ไม่พบสิทธิพิเศษที่กำหนด"));
        mappingMessage.put("723", new EdcDisplay("0723", "หมายเลขบัตรไม่ถูกต้อง"));
        mappingMessage.put("722", new EdcDisplay("0722", "TID ยังไม่ได้ถูกสร้าง"));
        mappingMessage.put("718", new EdcDisplay("0718", "ระบบไม่สามารถดำเนินการณ์ได้ในขณะนี้"));
        mappingMessage.put("715", new EdcDisplay("0715", "ไม่พบข้อมูลลูกค้าในระบบทรู"));
        mappingMessage.put("714", new EdcDisplay("0714", "ใช้สิทธิครบตามจำนวนที่กำหนด"));
        mappingMessage.put("713", new EdcDisplay("0713", "ไม่เข้าเงื่อนไขในการรับสิทธิ์"));
        mappingMessage.put("712", new EdcDisplay("0712", "ใช้สิทธิครบตามจำนวนที่กำหนด"));
        mappingMessage.put("711", new EdcDisplay("0711", "รหัสถูกใช้ไปแล้ว"));
        mappingMessage.put("709", new EdcDisplay("0709", "ไม่พบข้อมูลบัตรของท่าน"));
        mappingMessage.put("708", new EdcDisplay("0708", "ไม่พบสิทธิพิเศษที่กำหนด"));
        mappingMessage.put("706", new EdcDisplay("0706", "ไม่พบสิทธิพิเศษที่กำหนด"));
        mappingMessage.put("705", new EdcDisplay("0705", "ระบบไม่สามารถดำเนินการณ์ได้ในขณะนี้"));
        mappingMessage.put("702", new EdcDisplay("0702", "คะแนนคงเหลือไม่เพียงพอค่ะ"));
        mappingMessage.put("701", new EdcDisplay("0701", "ไม่สามารถส่ง SMS ได้"));
        mappingMessage.put("501", new EdcDisplay("0501", "หมายเลขโทรศัพท์ไม่ถูกต้อง"));
        mappingMessage.put("402", new EdcDisplay("0402", "ระบบไม่สามารถดำเนินการณ์ได้ในขณะนี้"));
        mappingMessage.put("401", new EdcDisplay("0401", "ระบบไม่สามารถดำเนินการณ์ได้ในขณะนี้"));
        mappingMessage.put("409", new EdcDisplay("0409", "ไม่สามารถยกเลิกรายการซ้ำได้"));
        mappingMessage.put("410", new EdcDisplay("0410", "ไม่พบ Campaign นี้ในระบบ"));
        mappingMessage.put("411", new EdcDisplay("0411", "ไม่พบข้อมูล Settlement"));
        mappingMessage.put("412", new EdcDisplay("412", "ไม่พบ Merchant นี้ในระบบ"));
        mappingMessage.put("400", new EdcDisplay("0400", "ระบบไม่สามารถดำเนินการณ์ได้ในขณะนี้"));
        mappingMessage.put("300", new EdcDisplay("0300", "ระบบไม่สามารถดำเนินการณ์ได้ในขณะนี้"));
        mappingMessage.put("250", new EdcDisplay("0250", "ไม่พบสิทธิพิเศษที่กำหนด"));
        mappingMessage.put("105", new EdcDisplay("0105", "คุณเข้าร่วมกิจกรรมแล้ว"));
        mappingMessage.put("104", new EdcDisplay("0104", "ไม่พบข้อมูลลูกค้าในระบบทรู"));
        mappingMessage.put("100", new EdcDisplay("0100", "ระบบไม่สามารถดำเนินการณ์ได้ในขณะนี้"));
        mappingMessage.put("-2002", new EdcDisplay("2002", "ระบบไม่สามารถดำเนินการณ์ได้ในขณะนี้"));
        mappingMessage.put("-2008", new EdcDisplay("2008", "ระบบไม่สามารถดำเนินการณ์ได้ในขณะนี้"));
        mappingMessage.put("-2009", new EdcDisplay("2009", "อยู่นอกช่วงเวลาการรับสิทธิ์"));
        mappingMessage.put("-2010", new EdcDisplay("2010", "ใช้สิทธิครบตามจำนวนที่กำหนด"));
        mappingMessage.put("-2011", new EdcDisplay("2011", "ใช้สิทธิครบตามจำนวนที่กำหนด"));
        mappingMessage.put("-2012", new EdcDisplay("2012", "ไม่พบสิทธิพิเศษที่กำหนด"));
        mappingMessage.put("-2042", new EdcDisplay("2042", "ระบบไม่สามารถดำเนินการณ์ได้ในขณะนี้"));
        mappingMessage.put("-2043", new EdcDisplay("2043", "กรุณาตรวจสอบหมายเลขบัตรประชาชนอีกครั้งค่ะ"));
        mappingMessage.put("-2044", new EdcDisplay("2044", "ระบบไม่สามารถดำเนินการณ์ได้ในขณะนี้"));
        mappingMessage.put("-2045", new EdcDisplay("2045", "ระบบไม่สามารถดำเนินการณ์ได้ในขณะนี้"));
        mappingMessage.put("-2050", new EdcDisplay("2050", "ระบบไม่สามารถดำเนินการณ์ได้ในขณะนี้"));
        mappingMessage.put("-2055", new EdcDisplay("2055", "ระบบไม่สามารถดำเนินการณ์ได้ในขณะนี้"));
        mappingMessage.put("-2056", new EdcDisplay("2056", "ระบบยังไม่ได้สร้างโปรโมชั่นร้านค้านี้"));
        mappingMessage.put("-2061", new EdcDisplay("2061", "ไม่พบรหัสส่วนลดในระบบ"));
        mappingMessage.put("-2062", new EdcDisplay("2062", "รหัสส่วนลดนี้ถูกใช้แล้ว"));
        mappingMessage.put("-2063", new EdcDisplay("2063", "รหัสส่วนลดนี้อยู่นอกระยะเวลารับสิทธิ์"));
        mappingMessage.put("-2064", new EdcDisplay("2064", "ระบบไม่สามารถดำเนินการณ์ได้ในขณะนี้"));
        mappingMessage.put("-2065", new EdcDisplay("2065", "เลขร้านค้านี้มีอยู่แล้วในระบบ"));
        mappingMessage.put("-2066", new EdcDisplay("2066", "เลขสาขาร้านค้านี้มีอยู่แล้วในระบบ"));
        mappingMessage.put("-2067", new EdcDisplay("2067", "ยังไม่ได้สร้างร้านค้าในระบบ"));
        mappingMessage.put("-2068", new EdcDisplay("2068", "ระบบไม่สามารถดำเนินการณ์ได้ในขณะนี้"));
        mappingMessage.put("-2069", new EdcDisplay("2069", "ระบบไม่สามารถดำเนินการณ์ได้ในขณะนี้"));
        mappingMessage.put("-2071", new EdcDisplay("2071", "กรุณาตรวจสอบหมายเลขบัตรประชาชนอีกครั้งค่ะ"));
        mappingMessage.put("-2072", new EdcDisplay("2072", "ระบบไม่สามารถดำเนินการณ์ได้ในขณะนี้"));
        mappingMessage.put("-2074", new EdcDisplay("2074", "ระบบไม่สามารถดำเนินการณ์ได้ในขณะนี้"));
        mappingMessage.put("-2075", new EdcDisplay("2075", "ระบบไม่สามารถดำเนินการณ์ได้ในขณะนี้"));
        mappingMessage.put("-2076", new EdcDisplay("2076", "ระบบไม่สามารถดำเนินการณ์ได้ในขณะนี้"));
        mappingMessage.put("-2077", new EdcDisplay("2077", "ระบบไม่สามารถดำเนินการณ์ได้ในขณะนี้"));
        mappingMessage.put("-2078", new EdcDisplay("2078", "ระบบไม่สามารถดำเนินการณ์ได้ในขณะนี้"));
        mappingMessage.put("-2079", new EdcDisplay("2079", "ใช้สิทธิครบตามจำนวนที่กำหนด"));
        mappingMessage.put("-2080", new EdcDisplay("2080", "ระบบไม่สามารถดำเนินการณ์ได้ในขณะนี้"));
        mappingMessage.put("-2081", new EdcDisplay("2081", "ระบบไม่สามารถดำเนินการณ์ได้ในขณะนี้"));
        mappingMessage.put("-2083", new EdcDisplay("2083", "คะแนนคงเหลือไม่พอค่ะ"));
        mappingMessage.put("-2088", new EdcDisplay("2088", "ท่านไม่สามารถรับสิทธิพิเศษนี้ได้ เนื่องจากไม่เข้าเงื่อนไขการรับสิทธิ์"));
        mappingMessage.put("-2089", new EdcDisplay("2089", "ระบบไม่สามารถดำเนินการณ์ได้ในขณะนี้"));
        mappingMessage.put("-2090", new EdcDisplay("2090", "ระบบไม่สามารถดำเนินการณ์ได้ในขณะนี้"));
        mappingMessage.put("-2091", new EdcDisplay("2091", "ระบบไม่สามารถดำเนินการณ์ได้ในขณะนี้"));
        mappingMessage.put("-2099", new EdcDisplay("2099", "ระบบไม่สามารถดำเนินการณ์ได้ในขณะนี้"));
        mappingMessage.put("-3006", new EdcDisplay("3006", "ระบบไม่สามารถดำเนินการณ์ได้ในขณะนี้"));
        mappingMessage.put("-3009", new EdcDisplay("3009", "ระบบไม่สามารถดำเนินการณ์ได้ในขณะนี้"));
        mappingMessage.put("-3011", new EdcDisplay("3011", "ระบบไม่สามารถดำเนินการณ์ได้ในขณะนี้"));
        mappingMessage.put("-3999", new EdcDisplay("3999", "ระบบไม่สามารถดำเนินการณ์ได้ในขณะนี้"));
        mappingMessage.put("4101", new EdcDisplay("4101", "ระบบไม่สามารถดำเนินการได้ในขณะนี้"));
        mappingMessage.put("4102", new EdcDisplay("4102", "ข้อมูลร้านค้าไม่ถูกต้อง ไม่สามารถดำเนินการได้"));
        mappingMessage.put("4103", new EdcDisplay("4103", "ระบบไม่สามารถดำเนินการได้ในขณะนี้"));
        mappingMessage.put("4104", new EdcDisplay("4104", "Barcode ถูกใช้แล้ว/ไม่ถูกต้องหรือหมดอายุ"));
        mappingMessage.put("4105", new EdcDisplay("4105", "เงินในกระเป๋าของท่านมีไม่เพียงพอ"));
        mappingMessage.put("4106", new EdcDisplay("4106", "ไม่สามารถดำเนินการได้ กรุณาชำระช่องทางอื่น"));
        mappingMessage.put("4107", new EdcDisplay("4107", "ระบบไม่สามารถดำเนินการได้ในขณะนี้"));
        mappingMessage.put("4109", new EdcDisplay("4109", "ไม่สามารถยกเลิกข้ามวันได้ กรุณาใช้ Refund"));
        mappingMessage.put("4110", new EdcDisplay("4110", "กรุณาทำรายการยกเลิกแทนการ Refund"));
        mappingMessage.put("4111", new EdcDisplay("4111", "ไม่สามารถดำเนินการได้ Refund เกินจำนวน"));
        mappingMessage.put("4112", new EdcDisplay("4112", "ไม่สามารถดำเนินการได้ ติดต่อ ***********ascend/tmn"));
        mappingMessage.put("4113", new EdcDisplay("4113", "ไม่สามารถดำเนินการได้ ติดต่อ ***********ascend/tmn"));
        mappingMessage.put("4114", new EdcDisplay("4114", "ไม่สามารถดำเนินการได้ กรุณา Refund เต็มจำนวน"));
        mappingMessage.put("4115", new EdcDisplay("4115", "ไม่สามารถดำเนินการได้ รายการถูกยกเลิกแล้ว"));
        mappingMessage.put("4017", new EdcDisplay("4117", "ข้อมูลไม่ถูกต้อง กรุณาชำระช่องทางอื่น"));
        mappingMessage.put("-9991", new EdcDisplay("9991", "ระบบไม่สามารถดำเนินการณ์ได้ในขณะนี้"));
        mappingMessage.put("408", new EdcDisplay("0408", "ไม่สามารถทำรายการได้ กรุณาติดต่อ Call Center"));
        mappingMessage.put("-9999", new EdcDisplay("9999", "ขออภัยค่ะ ระบบไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้งค่ะ"));
        mappingMessage.put("-9998", new EdcDisplay("9998", "ขออภัยค่ะ ระบบไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้งค่ะ"));
        mappingMessage.put("-4444", new EdcDisplay("4444", "ขออภัยค่ะ ระบบไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้ง"));
        mappingMessage.put("-4004", new EdcDisplay("4004", "ขออภัยค่ะคุณรับสิทธิพิเศษนี้ไปแล้ว ติดตามความสุขอื่นที่ www.trueyou.co.th"));
        mappingMessage.put("-8002", new EdcDisplay("8002", "ขออภัยค่ะ คุณไม่สามารถใช้สิทธิพิเศษนี้ได้ค่ะ เพิ่มเติมที่ www.trueyou.co.th"));
        mappingMessage.put("-3011", new EdcDisplay("3011", "ขออภัยค่ะ ระบบไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้งค่ะ"));
        mappingMessage.put("-3010", new EdcDisplay("3010", "ขออภัยค่ะ ระบบไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้งค่ะ"));
        mappingMessage.put("-3009", new EdcDisplay("3009", "ขออภัยค่ะ ระบบไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้งค่ะ"));
        mappingMessage.put("-3008", new EdcDisplay("3008", "ขออภัยค่ะ ข้อมูลของคุณไม่ถูกต้องค่ะ"));
        mappingMessage.put("-3006", new EdcDisplay("3006", "ขออภัยค่ะ ระบบไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้งค่ะ"));
        mappingMessage.put("-3005", new EdcDisplay("3005", "ขออภัยค่ะ ระบบไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้งค่ะ"));
        mappingMessage.put("-3004", new EdcDisplay("3004", "ขออภัยค่ะ ระบบไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้งค่ะ"));
        mappingMessage.put("-2998", new EdcDisplay("2998", "ขออภัยค่ะ ระบบไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้งค่ะ"));
        mappingMessage.put("-2087", new EdcDisplay("2087", "ขออภัยค่ะ ระบบไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้งค่ะ"));
        mappingMessage.put("-2085", new EdcDisplay("2085", "ขออภัยค่ะ ระบบไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้งค่ะ"));
        mappingMessage.put("-2084", new EdcDisplay("2085", "ขออภัยค่ะ ระบบไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้งค่ะ"));
        mappingMessage.put("-2082", new EdcDisplay("2082", "ขออภัยค่ะ ระบบไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้งค่ะ"));
        mappingMessage.put("-2073", new EdcDisplay("2073", "ขออภัยค่ะ ท่านไม่สามารถรับสิทธิพิเศษได้เนื่องจากหมายเลขนี้ไม่อยู่ในเงื่อนไขค่ะ"));
        mappingMessage.put("-2070", new EdcDisplay("2070", "ขออภัยค่ะ ระบบไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้งค่ะ"));
        mappingMessage.put("-2060", new EdcDisplay("2060", "ขออภัยค่ะ ระบบไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้งค่ะ"));
        mappingMessage.put("-2059", new EdcDisplay("2059", "ขออภัยค่ะ ระบบไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้งค่ะ"));
        mappingMessage.put("-2058", new EdcDisplay("2058", "ขออภัยค่ะ ระบบไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้งค่ะ"));
        mappingMessage.put("-2057", new EdcDisplay("2057", "ขออภัยค่ะ ระบบไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้งค่ะ"));
        mappingMessage.put("-2055", new EdcDisplay("2055", "ขออภัยค่ะ ระบบไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้งค่ะ"));
        mappingMessage.put("-2054", new EdcDisplay("2054", "ขออภัยค่ะ ระบบไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้งค่ะ"));
        mappingMessage.put("-2053", new EdcDisplay("2053", "ขออภัยค่ะ ระบบไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้งค่ะ"));
        mappingMessage.put("-2052", new EdcDisplay("2052", "ขออภัยค่ะ ระบบไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้งค่ะ"));
        mappingMessage.put("-2051", new EdcDisplay("2051", "ขออภัยค่ะ ระบบไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้งค่ะ"));
        mappingMessage.put("-2050", new EdcDisplay("2050", "ขออภัยค่ะ ระบบไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้งค่ะ"));
        mappingMessage.put("-2049", new EdcDisplay("2049", "ขออภัยค่ะ ระบบไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้งค่ะ"));
        mappingMessage.put("-2048", new EdcDisplay("2048", "ขออภัยค่ะ ระบบไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้งค่ะ"));
        mappingMessage.put("-2047", new EdcDisplay("2047", "ขออภัยค่ะ ระบบไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้ง"));
        mappingMessage.put("-2046", new EdcDisplay("2046", "ขออภัยค่ะ ระบบไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้งค่ะ"));
        mappingMessage.put("-2045", new EdcDisplay("2045", "ขออภัยค่ะ ระบบไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้งค่ะ"));
        mappingMessage.put("-2044", new EdcDisplay("2044", "ขออภัยค่ะ ระบบไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้งค่ะ"));
        mappingMessage.put("-2043", new EdcDisplay("2043", "ขออภัยค่ะ ระบบไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้งค่ะ"));
        mappingMessage.put("-2042", new EdcDisplay("2042", "ขออภัยค่ะ ระบบไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้งค่ะ"));
        mappingMessage.put("-2041", new EdcDisplay("2041", "ขออภัยค่ะ รหัสข้อมูลของคุณไม่ถูกต้องค่ะ"));
        mappingMessage.put("-2040", new EdcDisplay("2040", "ขออภัยค่ะ กรุณาส่งหมายเลขบัตรประชาชนใหม่อีกครั้ง"));
        mappingMessage.put("-2039", new EdcDisplay("2039", "ขออภัยค่ะ ระบบไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้งค่ะ"));
        mappingMessage.put("-2038", new EdcDisplay("2038", "ขออภัยค่ะ ระบบไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้งค่ะ"));
        mappingMessage.put("-2037", new EdcDisplay("2037", "ขออภัยค่ะ ระบบไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้งค่ะ"));
        mappingMessage.put("-2019", new EdcDisplay("2019", "ขออภัยค่ะ ไม่พบข้อมูลที่จะลบในระบบ"));
        mappingMessage.put("-2014", new EdcDisplay("2014", "ขออภัยค่ะ ระบบไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้งค่ะ"));
        mappingMessage.put("-2013", new EdcDisplay("2013", "ขณะนี้มีผู้ใช้สิทธิพิเศษครบตามรอบการใช้สิทธิ์แล้ว ติดตามสิทธินี้ในโอกาสต่อไปค่ะ"));
        mappingMessage.put("-2007", new EdcDisplay("2007", "ขออภัยค่ะ ระบบไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้งค่ะ"));
        mappingMessage.put("-2001", new EdcDisplay("2001", "ขออภัยค่ะ ระบบไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้งค่ะ"));
        mappingMessage.put("-99", new EdcDisplay("0099", "ขออภัยค่ะ ระบบไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้ง"));
        mappingMessage.put("-98", new EdcDisplay("0098", "ขออภัยค่ะ ระบบไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้ง"));
        mappingMessage.put("-97", new EdcDisplay("0097", "แพคเกจปัจจุบันของคุณ ไม่สามารถเข้าร่วมรับสิทธิพิเศษนี้ได้ค่ะ"));
        mappingMessage.put("-96", new EdcDisplay("0096", "ขออภัยค่ะ ระบบไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้ง"));
        mappingMessage.put("-95", new EdcDisplay("0095", "ขออภัยค่ะ คุณได้ใช้สิทธิพิเศษนี้แล้วค่ะ"));
        mappingMessage.put("0", new EdcDisplay("0000", "Accept"));
        mappingMessage.put("1", new EdcDisplay("0001", "Success"));
        mappingMessage.put("10", new EdcDisplay("0010", "คะแนนสะสม TrueReward ไม่เพียงพอค่ะ ตรวจสอบคะแนน *878*3*1# โทรออก(ฟรี)"));
        mappingMessage.put("99", new EdcDisplay("0099", "ขออภัยค่ะ ระบบไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้งค่ะขออภัยค่ะ คุณไม่สามารถใช้สิทธิพิเศษนี้ได้ค่ะ เพิ่มเติมที่ www.trueyou.co.th"));
        mappingMessage.put("4134", new EdcDisplay("4134", "ไม่สามารถชำระเงินผ่าน Alipay ในเวลา 22:40 - 23:10"));
        mappingMessage.put("4135", new EdcDisplay("4135", "ไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้ง"));
        mappingMessage.put("4136", new EdcDisplay("4136", "ระบบไม่สามารถดำเนินการได้ในขณะนี้"));
        mappingMessage.put("4137", new EdcDisplay("4137", "ระบบไม่สามารถดำเนินการได้ในขณะนี้"));
        mappingMessage.put("4138", new EdcDisplay("4138", "ระบบไม่สามารถดำเนินการได้ในขณะนี้"));
        mappingMessage.put("4139", new EdcDisplay("4139", "ข้อมูลร้านค้าไม่ถูกต้อง ไม่สามารถดำเนินการได้"));
        mappingMessage.put("4140", new EdcDisplay("4140", "ระบบไม่สามารถดำเนินการได้ในขณะนี้"));
        mappingMessage.put("4141", new EdcDisplay("4141", "ระบบไม่สามารถดำเนินการได้ในขณะนี้"));
        mappingMessage.put("4142", new EdcDisplay("4142", "ระบบไม่สามารถดำเนินการได้ในขณะนี้"));
        mappingMessage.put("414032", new EdcDisplay("4142", "ระบบไม่สามารถดำเนินการได้ในขณะนี้"));
        mappingMessage.put("4143", new EdcDisplay("4143", "ไม่พบสิทธิพิเศษที่กำหนด"));
        mappingMessage.put("4144", new EdcDisplay("4144", "เงินในกระเป๋าของท่านมีไม่เพียงพอ"));
        mappingMessage.put("4145", new EdcDisplay("4145", "ไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้ง"));
        mappingMessage.put("4146", new EdcDisplay("4146", "ไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้ง"));
        mappingMessage.put("4147", new EdcDisplay("4147", "ระบบไม่สามารถดำเนินการได้ในขณะนี้"));
        mappingMessage.put("4148", new EdcDisplay("4148", "ระบบไม่สามารถดำเนินการได้ในขณะนี้"));
        mappingMessage.put("4149", new EdcDisplay("4149", "ข้อมูลไม่ถูกต้อง กรุณาชำระช่องทางอื่น"));
        mappingMessage.put("4150", new EdcDisplay("4150", "บัญชีลูกค้าไม่สามารถทำรายการได้"));
        mappingMessage.put("4151", new EdcDisplay("4151", "กรุณาอัพเดท Application เวอร์ชั่นล่าสุด"));
        mappingMessage.put("4152", new EdcDisplay("4152", "บาร์โค้ดชำระเงินไม่ถูกต้อง"));
        mappingMessage.put("4153", new EdcDisplay("4153", "ระบบไม่สามารถดำเนินการได้ในขณะนี้"));
        mappingMessage.put("4154", new EdcDisplay("4154", "ระบบไม่สามารถดำเนินการได้ในขณะนี้"));
        mappingMessage.put("4155", new EdcDisplay("4155", "ระบบไม่สามารถดำเนินการได้ในขณะนี้"));
        mappingMessage.put("4156", new EdcDisplay("4156", "ไม่สามารถดำเนินการได้ในขณะนี้ กรุณาลองใหม่อีกครั้ง"));
        mappingMessage.put("403", new EdcDisplay("0403", "ไม่พบรายการที่สรุปยอด"));
        mappingMessage.put("404", new EdcDisplay("0404", "ไม่พบรายการ"));
        mappingMessage.put("EDCLOGIN00", new EdcDisplay("401", "ข้อมูลผู้ใช้งานไม่ถูกต้อง"));
        mappingMessage.put("407", new EdcDisplay("407", "ขออภัยไม่สามารถทำรายการได้ กรุณาติดต่อ TrueYou call center"));
        mappingMessage.put("0413", new EdcDisplay("0413", "ขออภัยไม่สามารถเชื่อมต่อกับระบบ Security ได้ขณะนี้"));
        mappingMessage.put("1014", new EdcDisplay("1014", "ไม่พบรหัสของรางวัลในระบบ"));
        return mappingMessage;
    }


}
