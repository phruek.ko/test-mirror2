package com.eggdigital.rpp.edcapp.config;

import com.eggdigital.rpp.edcapp.security.ApiAuthenticationFailureHandler;
import com.eggdigital.rpp.edcapp.security.ApiSignatureAuthenticationProvider;
import com.eggdigital.rpp.edcapp.security.ApiSignatureFilter;
import com.eggdigital.rpp.edcapp.security.UnauthorizedEntryPoint;
import com.eggdigital.rpp.edcapp.service.AuthorizationService;
import com.eggdigital.rpp.edcapp.service.CryptoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private CryptoService cryptoService;

    @Autowired
    private AuthorizationService authorizationService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.headers().frameOptions().sameOrigin()
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .exceptionHandling().authenticationEntryPoint(unauthorizedEntryPoint())
                .and()
                .authorizeRequests().anyRequest().authenticated()
                .and()
                .anonymous().disable();

        http.addFilterBefore(new ApiSignatureFilter(authenticationManager(), apiAuthenticationFailureHandler()), UsernamePasswordAuthenticationFilter.class);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(apiSignatureAuthenticationProvider());
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/signature/**", "/h2-console/**", "/swagger-ui.html", "/swagger-ui.html/**", "/settlements/**");
    }

    @Bean
    public AuthenticationProvider apiSignatureAuthenticationProvider() {
        return new ApiSignatureAuthenticationProvider(cryptoService);
    }

    @Bean
    public AuthenticationFailureHandler apiAuthenticationFailureHandler() {
        return new ApiAuthenticationFailureHandler(mapper);
    }

    @Bean
    public AuthenticationEntryPoint unauthorizedEntryPoint() {
        return new UnauthorizedEntryPoint(mapper);
    }
}
