package com.eggdigital.rpp.edcapp.config.constant;

public final class ProfileConstants {

    public static final String DEVELOPMENT = "dev";

    private ProfileConstants() {
    }
}
