package com.eggdigital.rpp.edcapp.config;

import org.springframework.cloud.sleuth.Sampler;
import org.springframework.cloud.sleuth.sampler.AlwaysSampler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by bombay on 3/10/2018 AD.
 */
@Configuration
public class ZipKinConfig {
    @Bean
    public Sampler defaultSampler() { return new AlwaysSampler();}
}
