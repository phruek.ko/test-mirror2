package com.eggdigital.rpp.edcapp.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public final class NumberUtils {

    private static final String AMOUNT_FORMAT_PATTERN = "#0.00";

    private NumberUtils() {
    }

    public static String amountFormat(BigDecimal value) {
        DecimalFormat format = new DecimalFormat(AMOUNT_FORMAT_PATTERN);
        return format.format(value);
    }
}
