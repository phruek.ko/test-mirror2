package com.eggdigital.rpp.edcapp;

import com.eggdigital.common.util.SMSUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CommonUtilConfig {

    @Bean
    public SMSUtils getSmsUtils() {
        return new SMSUtils();
    }

}
