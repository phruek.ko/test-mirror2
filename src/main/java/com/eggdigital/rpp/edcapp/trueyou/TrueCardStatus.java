package com.eggdigital.rpp.edcapp.trueyou;

/**
 * Created by wanatchapong on 2/23/2017 AD.
 */
public enum TrueCardStatus {

    ACTIVE("A"), CANCEL("N"), UNDEFINED("Z");

    private String status;

    private TrueCardStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
