package com.eggdigital.rpp.edcapp;

import com.eggdigital.rpp.edcapp.config.DefaultProfileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 * Bootstrap process used in Servlet 3.0+ applications with no web.xml
 */
public class ApplicationWebXml extends SpringBootServletInitializer {

    private final Logger log = LoggerFactory.getLogger(ApplicationWebXml.class);

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        log.info("Producing a deployable war file");
        /**
         * set a default to use when no profile is configured.
         */
        DefaultProfileUtils.addDefaultProfile(builder.application());
        return builder.sources(Application.class);
    }

}
