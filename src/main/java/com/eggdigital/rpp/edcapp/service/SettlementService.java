package com.eggdigital.rpp.edcapp.service;

import com.eggdigital.rpp.edcapp.dto.SettlementTransaction;
import com.eggdigital.rpp.edcapp.dto.SettlementTransactionByBrand;

/**
 * Created by bombay on 11/14/2017 AD.
 */
public interface SettlementService {

    SettlementTransaction settlements();

    SettlementTransactionByBrand settlementsByBrandId(String brandId);
}
