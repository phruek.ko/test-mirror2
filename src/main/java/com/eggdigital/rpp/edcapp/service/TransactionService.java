package com.eggdigital.rpp.edcapp.service;


import com.eggdigital.rpp.edcapp.domain.Transaction;

import java.math.BigInteger;

/**
 * Created by bombay on 10/16/2017 AD.
 */
public interface TransactionService {

    Transaction saveTx(Transaction transaction);

    void updateTxFailureByException(BigInteger id, Exception ex);
}
