package com.eggdigital.rpp.edcapp.service;

import com.eggdigital.rpp.edcapp.dto.*;

/**
 * Created by bombay on 11/8/2017 AD.
 */
public interface ReportService {

    ReceiptDto getReceiptByBrandIdAndOutletAndTerminalIdAndTraceId(String brandId, String outletId, String terminalId, String traceId);

    ReceiptDto getReceiptLastByBrandIdAndOutletAndTerminalId(String brandId, String outletId, String terminalId);

    SettlementReport settlementByTerminalInfo(VerifyUserDto dto);

    SummaryReport summaryByBrandIdOutletIdTerminalId(String brandId, String outletId, String terminalId, String createDate);

    DetailSummaryReportNew detailSummaryByBrandIdOutletIdTerminalIdTransactionDate(String brandId, String outletId, String terminalId, String transactionDate);

    SettlementReport previewSettlementByTerminalInfo(VerifyUserDto dto);

}
