package com.eggdigital.rpp.edcapp.service;

import com.eggdigital.rpp.edcapp.domain.Status;
import com.eggdigital.rpp.edcapp.domain.TransactionBatch;

import java.util.List;

/**
 * Created by bombay on 10/19/2017 AD.
 */
public interface TransactionBatchService {

    TransactionBatch genTransactionBatch(String brandId, String outletId, String terminalId);

    List<TransactionBatch> updateByBrandIdOutletIdTerminalIdStatusSettle(String brandId, String outletId, String terminalId, Status status);
}
