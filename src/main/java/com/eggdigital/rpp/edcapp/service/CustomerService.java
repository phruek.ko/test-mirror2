package com.eggdigital.rpp.edcapp.service;

import com.eggdigital.rpp.edcapp.domain.AccountType;
import com.eggdigital.rpp.edcapp.dto.CardInfoRequest;
import com.eggdigital.rpp.edcapp.dto.CardInfoSlip;
import com.eggdigital.rpp.edcapp.handler.ApiErrorException;

import java.util.List;

public interface CustomerService {

    CardInfoSlip saveCardSlipTx(CardInfoRequest requestDto) throws ApiErrorException;

    List<String> getCustomerInfo(String terminalId, AccountType accType, String accValue);
}
