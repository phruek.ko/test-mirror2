package com.eggdigital.rpp.edcapp.service.impl;

import com.eggdigital.common.util.DateUtils;
import com.eggdigital.rpp.edcapp.api.edccampaign.ApiEdcCampaignService;
import com.eggdigital.rpp.edcapp.api.edccampaign.dto.ApiEdcCampaignDetailResponseDto;
import com.eggdigital.rpp.edcapp.api.model.ApiStatus;
import com.eggdigital.rpp.edcapp.domain.*;
import com.eggdigital.rpp.edcapp.dto.*;
import com.eggdigital.rpp.edcapp.enumuration.CampaignType;
import com.eggdigital.rpp.edcapp.exception.CustomThirdPartyExcepton;
import com.eggdigital.rpp.edcapp.repository.MerchantRepository;
import com.eggdigital.rpp.edcapp.repository.OutletRepository;
import com.eggdigital.rpp.edcapp.repository.TransactionRepository;
import com.eggdigital.rpp.edcapp.service.ReportService;
import com.eggdigital.rpp.edcapp.service.TransactionBatchService;
import com.google.common.base.Strings;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.schedulers.Schedulers;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.util.UriComponentsBuilder;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by bombay on 11/8/2017 AD.
 */
@Service
@Transactional
public class ReportServiceImpl implements ReportService {

    private final Logger log = LoggerFactory.getLogger(ReportServiceImpl.class);

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private MerchantRepository merchantRepository;

    @Autowired
    private OutletRepository outletRepository;

    @Autowired
    private TransactionBatchService transactionBatchService;

    @Autowired
    private ApiEdcCampaignService apiEdcCampaignService;

    private Set<Map> campaignInfo = new HashSet<>();

    @Override
    public ReceiptDto getReceiptByBrandIdAndOutletAndTerminalIdAndTraceId(String brandId, String outletId, String terminalId, String traceId) {
        Optional<Transaction> transactionOp = transactionRepository.findByBrandIdAndOutletIdAndTerminalIdAndTraceIdAndSettleStatusIsNot(brandId, outletId, terminalId, traceId, Status.SETTLE);
        return setReceiptDto(transactionOp);
    }

    @Override
    public ReceiptDto getReceiptLastByBrandIdAndOutletAndTerminalId(String brandId, String outletId, String terminalId) {
        Optional<Transaction> transactionOp = transactionRepository.findFirstByBrandIdAndOutletIdAndTerminalIdAndSettleStatusOrderByTraceIdDesc(brandId, outletId, terminalId, Status.PENDING.name());
        return setReceiptDto(transactionOp);
    }

    @Override
    public SettlementReport previewSettlementByTerminalInfo(VerifyUserDto dto) {
        String brandId = dto.getBrandId();
        String outletId = dto.getOutletId();
        String terminalId = dto.getTerminalId();

        //find transaction by brandId outletId terminalId settleStatus is not settle
        List<Transaction> transactions = transactionRepository.findByBrandIdAndOutletIdAndTerminalIdAndSettleStatusIsNot(brandId, outletId, terminalId, Status.SETTLE);
        if (transactions.isEmpty()) {
            return null;
        }
        return getSettlementByTerminalInfo(dto);
    }

    @Override
    public SettlementReport settlementByTerminalInfo(VerifyUserDto dto) {
        String brandId = dto.getBrandId();
        String outletId = dto.getOutletId();
        String terminalId = dto.getTerminalId();

        //find transaction by brandId outletId terminalId settleStatus is not settle
        List<Transaction> transactions = transactionRepository.findByBrandIdAndOutletIdAndTerminalIdAndSettleStatusIsNot(brandId, outletId, terminalId, Status.SETTLE);
        if (transactions.isEmpty()) {
            return null;
        }

        //update transaction settle status to settle
        transactions = transactions.stream().map(tx -> {
            tx.setSettleDate(new Date());
            tx.setSettleStatus(Status.SETTLE);
            return tx;
        }).collect(Collectors.toList());
        transactionRepository.save(transactions);

        //update status settlement by brandId and terminalId and terminalId and status settle of transaction batch
        transactionBatchService.updateByBrandIdOutletIdTerminalIdStatusSettle(brandId, outletId, terminalId, Status.PENDING);

        return getSettlementByTerminalInfo(dto);
    }

    @Override
    public SummaryReport summaryByBrandIdOutletIdTerminalId(String brandId, String outletId, String terminalId, String createDate) {
        if (Strings.isNullOrEmpty(createDate)) {
            createDate = DateUtils.formatDate(new Date(), "yyyy-MM-dd");
        }

        List<Transaction> transactions = getTransactionByBrandIdAndOutletIdAndTerminalIdAndStatusAndTransactionTypeInAndCreateDate(brandId, outletId, terminalId, createDate).subscribeOn(Schedulers.io()).blockingFirst();
        List<String> txRedeems = getDistinctTransactionRedeemByBrandIdOutletIdTerminalIdTransactionDateNotInCampaignCode(brandId, outletId, terminalId, createDate).subscribeOn(Schedulers.io()).blockingFirst();
        List<CampaignDto> campaigns = new ArrayList<>();
        if (!txRedeems.isEmpty()) {
            campaigns = getCampaignsByBrandIdOutletIdTerminalIdTransactionDate(txRedeems, brandId, outletId, terminalId, createDate).subscribeOn(Schedulers.io()).blockingFirst();
        }
        int earnEstampTotal = 0;
        int maskuseEstampTotal = 0;
        List<String> txEstamp = getTransactionRedeemByBrandIdOutletIdTerminalIdTransactionDateCampaignCode(brandId, outletId, terminalId, createDate, Campaign.E_STAMP.getParameterLike()).subscribeOn(Schedulers.io()).blockingFirst();
        if (!txEstamp.isEmpty()) {
            earnEstampTotal = transactionRepository.getCountCouponCodeByBrandIdAndOutletIdAndTerminalIdAndTransactionTypeAndStatusAndCamapignCodeAndCreateDate(brandId, outletId, terminalId, TransactionType.REDEEM_BENEFIT.name(), TransactionType.REDEEM_MARKUSE.name(), Status.SUCCESS.name(), Campaign.E_STAMP.getParameterLike(), createDate, Estamp.EARN_ESTAMP.getParameterLike());
            maskuseEstampTotal = transactionRepository.getCountCouponCodeByBrandIdAndOutletIdAndTerminalIdAndTransactionTypeAndStatusAndCamapignCodeAndCreateDate(brandId, outletId, terminalId, TransactionType.REDEEM_BENEFIT.name(), TransactionType.REDEEM_MARKUSE.name(), Status.SUCCESS.name(), Campaign.E_STAMP.getParameterLike(), createDate, Estamp.MARKUSE_ESTAMP.getParameterLike());
        }

        List<String> txECoupon = getTransactionRedeemByBrandIdOutletIdTerminalIdTransactionDateCampaignCode(brandId, outletId, terminalId, createDate, Campaign.E_COUPON.getParameterLike()).subscribeOn(Schedulers.io()).blockingFirst();
        List<CampaignDto> campaignECoupon = new ArrayList<>();
        if (!txECoupon.isEmpty()) {
            campaignECoupon = getCampaignsByBrandIdOutletIdTerminalIdTransactionDate(txECoupon, brandId, outletId, terminalId, createDate).subscribeOn(Schedulers.io()).blockingFirst();
        }

        int pointTotal = transactionRepository.getSumPointRedeemByBrandIdAndOutletIdAndTerminalIdAndTransactionTypeAndStatusAndCreateDate(brandId, outletId, terminalId, TransactionType.REDEEM_BENEFIT.name(), TransactionType.REDEEM_MARKUSE.name(), Status.SUCCESS.name(), createDate);
        Observable<RedeemDto> redeemObs = setRedeem(campaigns, pointTotal).subscribeOn(Schedulers.io());
        Observable<EstampDto> eStampObs = setEstamp(earnEstampTotal, maskuseEstampTotal).subscribeOn(Schedulers.io());
        Observable<String> eCouponDtoObservable = setECoupon(campaignECoupon).subscribeOn(Schedulers.io());
        Observable<List<PaymentDto>> paymentsObs = setPayments(transactions).subscribeOn(Schedulers.io());
        Observable<EarnDto> earnObs = setEarn(transactions).subscribeOn(Schedulers.io());
        return setSummaryReport(redeemObs, eStampObs, eCouponDtoObservable, paymentsObs, earnObs);
    }


    private SummaryReport setSummaryReport(Observable<RedeemDto> redeemObs, Observable<EstampDto> eStampObs, Observable<String> eCouponObs, Observable<List<PaymentDto>> paymentsObs, Observable<EarnDto> earnObs) {
        return Observable.zip(redeemObs, eStampObs, eCouponObs, paymentsObs, earnObs, (redeem, eStamp, eCoupon, payments, earn) -> new SummaryReport(payments, redeem, Collections.singletonList(earn), eStamp, eCoupon)).blockingFirst();
    }


    @Override
    public DetailSummaryReportNew detailSummaryByBrandIdOutletIdTerminalIdTransactionDate(String brandId, String outletId, String terminalId, String transactionDate) {
        Date date = new Date();
        Date dateStart = new Date();
        Date dateEnd = new Date();
        if (Strings.isNullOrEmpty(transactionDate)) {
            dateStart = DateUtils.toStartDate(date);
            dateEnd = DateUtils.toEndDate(date);
            transactionDate = DateUtils.formatDate(date, "yyyy-MM-dd");
        } else {
            try {
                date = DateUtils.parseDate(transactionDate, "yyyy-MM-dd");
                dateStart = DateUtils.toStartDate(date);
                dateEnd = DateUtils.toEndDate(date);
            } catch (Exception e) {
                log.error(null, e);
            }
        }

        List<String> txRedeems = getDistinctTransactionRedeemByBrandIdOutletIdTerminalIdTransactionDate(brandId, outletId, terminalId, transactionDate).subscribeOn(Schedulers.io()).blockingFirst();
        if (!txRedeems.isEmpty()) {
            campaignInfo = getCampaignInfo(txRedeems, brandId, outletId, terminalId);
        }

        //getTransaction redeem
        List<TransactionType> transactionTypeRedeem = Arrays.asList(TransactionType.REDEEM_BENEFIT, TransactionType.REDEEM_MARKUSE, TransactionType.REDEEM_ROLLBACK);
        List<Transaction> transactionStreamRedeem = transactionRepository.findByBrandIdAndOutletIdAndTerminalIdAndStatusAndTransactionTypeInAndCreateDateBetweenAndCampaignCodeNotLikeAndCampaignCodeNotLikeAndCampaignTypeIsNot(brandId, outletId, terminalId, Status.SUCCESS, transactionTypeRedeem, dateStart, dateEnd, Campaign.E_COUPON.getParameterLike(), Campaign.E_STAMP.getParameterLike(), CampaignType.MERCHANT);
        Supplier<List<Transaction>> streamRedeemSupplier = () -> transactionStreamRedeem;

        //getTransaction e_stamp
        List<TransactionType> transactionTypeEstamp = Arrays.asList(TransactionType.REDEEM_BENEFIT, TransactionType.REDEEM_MARKUSE);
        List<Transaction> transactionStreamEarnEstamp = transactionRepository.findByBrandIdAndOutletIdAndTerminalIdAndStatusAndTransactionTypeInAndCreateDateBetweenAndCampaignCodeLikeAndCouponCodeLikeAndCampaignTypeIsNot(brandId, outletId, terminalId, Status.SUCCESS, transactionTypeRedeem, dateStart, dateEnd, Campaign.E_STAMP.getParameterLike(), Estamp.EARN_ESTAMP.getParameterLike(), CampaignType.MERCHANT);
        Supplier<List<Transaction>> streamEarnEstampSupplier = () -> transactionStreamEarnEstamp;

        List<Transaction> transactionStreamMarkuseEstamp = transactionRepository.findByBrandIdAndOutletIdAndTerminalIdAndStatusAndTransactionTypeInAndCreateDateBetweenAndCampaignCodeLikeAndCouponCodeLikeAndCampaignTypeIsNot(brandId, outletId, terminalId, Status.SUCCESS, transactionTypeRedeem, dateStart, dateEnd, Campaign.E_STAMP.getParameterLike(), Estamp.MARKUSE_ESTAMP.getParameterLike(), CampaignType.MERCHANT);
        Supplier<List<Transaction>> streamMarkuseEstampSupplier = () -> transactionStreamMarkuseEstamp;

        //getTransaction e_Coupon
        List<TransactionType> transactionTypeECoupon = Arrays.asList(TransactionType.REDEEM_BENEFIT, TransactionType.REDEEM_MARKUSE, TransactionType.REDEEM_ROLLBACK);
        List<Transaction> transactionStreamECoupon = transactionRepository.findByBrandIdAndOutletIdAndTerminalIdAndStatusAndTransactionTypeInAndCreateDateBetweenAndCampaignCodeLikeAndCampaignTypeIsNot(brandId, outletId, terminalId, Status.SUCCESS, transactionTypeECoupon, dateStart, dateEnd, Campaign.E_COUPON.getParameterLike(), CampaignType.MERCHANT);
        Supplier<List<Transaction>> streamECouponSupplier = () -> transactionStreamECoupon;

        //getTransaction earn
        List<TransactionType> transactionTypeEarn = Arrays.asList(TransactionType.POINT_EARN, TransactionType.POINT_CANCEL);
        List<Transaction> transactionStreamEarn = transactionRepository.findByBrandIdAndOutletIdAndTerminalIdAndStatusAndTransactionTypeInAndCreateDateBetween(brandId, outletId, terminalId, Status.SUCCESS, transactionTypeEarn, dateStart, dateEnd);
        Supplier<List<Transaction>> streamEarnSupplier = () -> transactionStreamEarn;

        //getTransaction payment
        List<TransactionType> transactionTypePayment = Arrays.asList(TransactionType.PAYMENT_CHARGE, TransactionType.PAYMENT_CANCEL);
        List<Transaction> transactionStreamPayment = transactionRepository.findByBrandIdAndOutletIdAndTerminalIdAndStatusAndTransactionTypeInAndCreateDateBetween(brandId, outletId, terminalId, Status.SUCCESS, transactionTypePayment, dateStart, dateEnd);
        Supplier<List<Transaction>> streamPaymentSupplier = () -> transactionStreamPayment;

        String trueyouId = "";
        Optional<Merchant> merchant = merchantRepository.findByMerchantId(brandId);
        if (merchant.isPresent()) {
            trueyouId = merchant.get().getTrueyouId();
        }

        Observable<DetailSummaryObject> redeemOp = getDetailSummary(brandId, outletId, terminalId, streamRedeemSupplier, TransactionType.REDEEM_BENEFIT).subscribeOn(Schedulers.io());
        Observable<DetailSummaryObject> redeemVoidOp = getDetailSummary(brandId, outletId, terminalId, streamRedeemSupplier, TransactionType.REDEEM_ROLLBACK).subscribeOn(Schedulers.io());

        Observable<DetailSummaryObject> earnOp = getDetailSummary(brandId, outletId, terminalId, streamEarnSupplier, TransactionType.POINT_EARN).subscribeOn(Schedulers.io());
        Observable<DetailSummaryObject> earnVoidOp = getDetailSummary(brandId, outletId, terminalId, streamEarnSupplier, TransactionType.POINT_CANCEL).subscribeOn(Schedulers.io());

        Observable<DetailSummaryObject> paymentOp = getDetailSummary(brandId, outletId, terminalId, streamPaymentSupplier, TransactionType.PAYMENT_CHARGE).subscribeOn(Schedulers.io());
        Observable<DetailSummaryObject> paymentVoidOp = getDetailSummary(brandId, outletId, terminalId, streamPaymentSupplier, TransactionType.PAYMENT_CANCEL).subscribeOn(Schedulers.io());

        Observable<DetailSummaryObject> eStampEarn = getDetailSummary(brandId, outletId, terminalId, streamEarnEstampSupplier, TransactionType.REDEEM_MARKUSE).subscribeOn(Schedulers.io());
        Observable<DetailSummaryObject> eStampMarkuse = getDetailSummary(brandId, outletId, terminalId, streamMarkuseEstampSupplier, TransactionType.REDEEM_MARKUSE).subscribeOn(Schedulers.io());

        Observable<DetailSummaryObject> eCouponOp = getDetailSummary(brandId, outletId, terminalId, streamECouponSupplier, TransactionType.REDEEM_MARKUSE).subscribeOn(Schedulers.io());

        Observable<DetailSummaryVoid> detailSummaryVoidOp = setDetailSummaryVoid(redeemVoidOp, earnVoidOp, paymentVoidOp).subscribeOn(Schedulers.io());

        Observable<DetailSummaryEstamp> detailSummaryEstamp = setDetailSummaryEstamp(eStampEarn, eStampMarkuse).subscribeOn(Schedulers.io());

        return setDetailSummaryReportNew(trueyouId, terminalId, redeemOp, detailSummaryEstamp, eCouponOp, earnOp, paymentOp, detailSummaryVoidOp);
    }

    private Set<Map> getCampaignInfo(List<String> txRedeems, String brandId, String outletId, String terminalId) {
        return txRedeems.stream()
                .map(campaignCode -> {
                    String campaignName = getCampaignByBrandIdAndOutletIdAndTerminalIdAndCampaign(brandId, outletId, terminalId, campaignCode).blockingFirst();
                    Map<String, String> campaign = new HashMap<>();
                    campaign.put("campaignName", campaignName);
                    campaign.put("campaignCode", campaignCode);
                    return campaign;
                }).collect(Collectors.toSet());
    }

    private Observable<DetailSummaryVoid> setDetailSummaryVoid(Observable<DetailSummaryObject> redeemVoidOp, Observable<DetailSummaryObject> earnVoidOp, Observable<DetailSummaryObject> paymentVoidOp) {
        return Observable.zip(redeemVoidOp, earnVoidOp, paymentVoidOp, (redeem, earn, payment) -> new DetailSummaryVoid(redeem, earn, payment));
    }

    private Observable<DetailSummaryEstamp> setDetailSummaryEstamp(Observable<DetailSummaryObject> eStampEarn, Observable<DetailSummaryObject> eStampMarkUse) {
        return Observable.zip(eStampEarn, eStampMarkUse, (earn, markuse) -> new DetailSummaryEstamp(earn, markuse));
    }

    private DetailSummaryReportNew setDetailSummaryReportNew(String trueyouId, String terminalId, Observable<DetailSummaryObject> redeemOp, Observable<DetailSummaryEstamp> estampOp, Observable<DetailSummaryObject> eCouponOp, Observable<DetailSummaryObject> earnOp, Observable<DetailSummaryObject> paymentOp, Observable<DetailSummaryVoid> detailSummaryVoid) {
        return Observable.zip(eCouponOp, redeemOp, estampOp, earnOp, paymentOp, detailSummaryVoid, (eCoupon, redeem, eStamp, earn, payment, voidDto) -> new DetailSummaryReportNew(trueyouId, terminalId, eCoupon, redeem, eStamp, earn, payment, voidDto)).blockingFirst();
    }

    private Observable<DetailSummaryObject> getDetailSummary(String brandId, String outletId, String terminalId, Supplier<List<Transaction>> streamRedeemSupplier, TransactionType transactionType) {
        return Observable.create(sub -> {
            List<DetailSummaryTransaction> transactionsRedeem;
            if (transactionType == TransactionType.REDEEM_BENEFIT || transactionType == TransactionType.REDEEM_MARKUSE) {
                transactionsRedeem = streamRedeemSupplier.get().stream()
                        .filter(tx -> (tx.getTransactionType() == TransactionType.REDEEM_BENEFIT || tx.getTransactionType() == TransactionType.REDEEM_MARKUSE))
                        .map(tx -> setDetailSummaryTransaction(tx))
                        .collect(Collectors.toList());
            } else {
                transactionsRedeem = streamRedeemSupplier.get().stream()
                        .filter(tx -> tx.getTransactionType() == transactionType)
                        .map(tx -> setDetailSummaryTransaction(tx))
                        .collect(Collectors.toList());
            }

            DetailSummaryObject redeem = new DetailSummaryObject();
            if (transactionType == TransactionType.REDEEM_BENEFIT || transactionType == TransactionType.REDEEM_MARKUSE) {
                redeem.setTotal((int) streamRedeemSupplier.get().stream().filter(tx -> tx.getTransactionType() == TransactionType.REDEEM_BENEFIT || tx.getTransactionType() == TransactionType.REDEEM_MARKUSE).count());
            } else {
                redeem.setTotal((int) streamRedeemSupplier.get().stream().filter(tx -> tx.getTransactionType() == transactionType).count());
            }
            redeem.setTransactions(transactionsRedeem);
            sub.onNext(redeem);
            sub.onComplete();
        });
    }

    private SettlementReport getSettlementByTerminalInfo(VerifyUserDto dto) {
        String brandId = dto.getBrandId();
        String outletId = dto.getOutletId();
        String terminalId = dto.getTerminalId();

        String brandName = getMerchantByMerchantId(brandId).subscribeOn(Schedulers.io()).blockingFirst();

        List<Transaction> transactionData = getTransactionByBrandIdAndOutletIdAndTerminalIdAndTransactionTypeInAndStatusAndSettleStatus(brandId, outletId, terminalId).subscribeOn(Schedulers.io()).blockingFirst();
        Optional<Transaction> transactionOptional = transactionData.stream().findFirst();
        String batchId = "";
        if (transactionOptional.isPresent()) {
            Transaction transaction = transactionOptional.get();
            batchId = transaction.getBatchId();
        }

        PaymentDto paymentDto = setPayment(transactionData).subscribeOn(Schedulers.io()).blockingFirst();

        List<String> txRedeems = getDistinctTransactionRedeemByBrandIdOutletIdTerminalIdSettleStatusPending(brandId, outletId, terminalId).subscribeOn(Schedulers.io()).blockingFirst();
        List<CampaignDto> campaigns = getCampaignsByBrandIdOutletIdTerminalId(txRedeems, brandId, outletId, terminalId).subscribeOn(Schedulers.io()).blockingFirst();
        RedeemDto redeem = setRedeem(campaigns, 0).subscribeOn(Schedulers.io()).blockingFirst();

        EstampDto estampDto = getEarnEstampTotalAndMarkuseEstampTotal(brandId, outletId, terminalId);

        String eCouponDto = getECouponTotal(brandId, outletId, terminalId);

        EarnDto earn = setEarn(transactionData).subscribeOn(Schedulers.io()).blockingFirst();

        Optional<Merchant> merchant = merchantRepository.findByMerchantId(dto.getBrandId());

        SettlementReport settlementReport = new SettlementReport();
        settlementReport.setBrandId(brandId);
        settlementReport.setBrandName(brandName);
        settlementReport.setOutletId(outletId);
        settlementReport.setTerminalId(terminalId);
        settlementReport.setBatchId(batchId);
        settlementReport.setPaymentDto(paymentDto);
        settlementReport.setRedeemDto(redeem);
        settlementReport.setEstampDto(estampDto);
        settlementReport.seteCoupon(eCouponDto);
        settlementReport.setEarnDto(earn);
        settlementReport.setTrueyouId(merchant.isPresent() ? merchant.get().getTrueyouId() : "");
        return settlementReport;
    }

    private String getECouponTotal(String brandId, String outletId, String terminalId) {
        List<String> txECoupon = getTransactionByBrandIdOutletIdTerminalIdSettleStatusPendingCampaign(brandId, outletId, terminalId, Campaign.E_COUPON.getParameterLike()).subscribeOn(Schedulers.io()).blockingFirst();
        List<CampaignDto> campaignECoupon = getCampaignByBrandIdOutletIdTerminalIdCampaignCode(txECoupon, brandId, outletId, terminalId, Campaign.E_COUPON.getParameterLike()).subscribeOn(Schedulers.io()).blockingFirst();
        return setECoupon(campaignECoupon).subscribeOn(Schedulers.io()).blockingFirst();
    }

    private EstampDto getEarnEstampTotalAndMarkuseEstampTotal(String brandId, String outletId, String terminalId) {
        List<String> txEstamp = getTransactionByBrandIdOutletIdTerminalIdSettleStatusPendingCampaign(brandId, outletId, terminalId, Campaign.E_STAMP.getParameterLike()).subscribeOn(Schedulers.io()).blockingFirst();
        int earnEstampTotal = getCountCouponCodeByEarnEstampEstampByBrandIdAndOutletIdAndTerminalIdAndTransactionTypeAndStatusAndSettleStatus(txEstamp, brandId, outletId, terminalId, Campaign.E_STAMP.getParameterLike(), Estamp.EARN_ESTAMP.getParameterLike()).subscribeOn(Schedulers.io()).blockingFirst();
        int maskuseEstampTotal = getCountCouponCodeByEarnEstampEstampByBrandIdAndOutletIdAndTerminalIdAndTransactionTypeAndStatusAndSettleStatus(txEstamp, brandId, outletId, terminalId, Campaign.E_STAMP.getParameterLike(), Estamp.MARKUSE_ESTAMP.getParameterLike()).subscribeOn(Schedulers.io()).blockingFirst();
        return setEstamp(earnEstampTotal, maskuseEstampTotal).subscribeOn(Schedulers.io()).blockingFirst();
    }


    private Observable<List<String>> getTransactionByBrandIdOutletIdTerminalIdSettleStatusPendingCampaign(String brandId, String outletId, String terminalId, String campaignCode) {
        return Observable.create(sub -> {
            List<String> txRedeems = transactionRepository.getTransactionByBrandIdOutletIdTerminalIdSettleStatusPendingCampaign(brandId, outletId, terminalId, TransactionType.REDEEM_BENEFIT.name(), TransactionType.REDEEM_MARKUSE.name(), Status.SUCCESS.name(), Status.PENDING.name(), campaignCode);
            if (!txRedeems.isEmpty()) {
                sub.onNext(txRedeems);
            } else {
                sub.onNext(Collections.emptyList());
            }
            sub.onComplete();
        });
    }

    private ReceiptDto setReceiptDto(Optional<Transaction> transactionOp) {
        if (transactionOp.isPresent()) {
            Transaction transaction = transactionOp.get();
            ReceiptDto receipt = new ReceiptDto();
            receipt.setTerminalId(transaction.getTerminalId());
            receipt.setTxRefId(transaction.getRefId());
            receipt.setTraceId(transaction.getTraceId());
            receipt.setBatchId(transaction.getBatchId());
            receipt.setTransactionDate(transaction.getModifyDate());
            receipt.setBrandId(transaction.getBrandId());
            setBrandInfo(transaction.getBrandId(), receipt);
            receipt.setOutletId(transaction.getOutletId());
            setOutletInfo(transaction.getOutletId(), receipt);
            receipt.setTransactionType(transaction.getTransactionType());
            receipt.setAmount(StringUtils.defaultString(transaction.getAmount()));
            receipt.setCurrency(transaction.getCurrency());
            receipt.setPoint(transaction.getPoint());
            receipt.setPaymentCode(StringUtils.defaultString(transaction.getPaymentCode()));
            receipt.setCustomerRef(StringUtils.defaultString(transaction.getCustomerRef()));
            receipt.setCampaignCode(StringUtils.defaultString(transaction.getCampaignCode()));
            receipt.setCouponCode(StringUtils.defaultString(transaction.getCouponCode()));
            return receipt;
        }
        return null;
    }

    private void setBrandInfo(String brandId, ReceiptDto receipt) {
        Optional<Merchant> merchant = merchantRepository.findByMerchantId(brandId);
        if (merchant.isPresent()) {
            receipt.setBrandNameTh(StringUtils.defaultString(merchant.get().getStoreNameTh()));
            receipt.setBrandNameEn(StringUtils.defaultString(merchant.get().getStoreNameEn()));
        }
    }

    private void setOutletInfo(String outletId, ReceiptDto receipt) {
        Outlet outlet = outletRepository.findByOutletId(outletId);
        if (Objects.nonNull(outlet)) {
            receipt.setOutletNameTh(StringUtils.defaultString(outlet.getOutletNameTh()));
            receipt.setOutletNameEn(StringUtils.defaultString(outlet.getOutletNameEn()));
        }
    }

    private Observable<List<String>> getDistinctTransactionRedeemByBrandIdOutletIdTerminalIdSettleStatusPending(String brandId, String outletId, String terminalId) {
        return Observable.create(sub -> {
            List<String> txRedeems = transactionRepository.getDistinctCampaignCodeByBrandIdAndOutletIdAndTerminalIdAndTransactionTypeAndStatusAndSettleStatus(brandId, outletId, terminalId, TransactionType.REDEEM_BENEFIT.name(), TransactionType.REDEEM_MARKUSE.name(), Status.SUCCESS.name(), Status.PENDING.name(), Campaign.E_COUPON.getParameterLike(), Campaign.E_STAMP.getParameterLike());
            if (!txRedeems.isEmpty()) {
                sub.onNext(txRedeems);
            } else {
                sub.onNext(Collections.emptyList());
            }
            sub.onComplete();
        });
    }

    private Observable<List<String>> getDistinctTransactionRedeemByBrandIdOutletIdTerminalIdTransactionDate(String brandId, String outletId, String terminalId, String transactionDate) {
        return Observable.create(sub -> {
            List<String> txRedeems = transactionRepository.getDistinctCampaignCodeByBrandIdAndOutletIdAndTerminalIdAndTransactionTypeAndStatusAndCreateDate(brandId, outletId, terminalId, TransactionType.REDEEM_BENEFIT.name(), TransactionType.REDEEM_MARKUSE.name(), Status.SUCCESS.name(), transactionDate);
            if (!txRedeems.isEmpty()) {
                sub.onNext(txRedeems);
            } else {
                sub.onNext(Collections.emptyList());
            }
            sub.onComplete();
        });
    }

    private Observable<List<String>> getDistinctTransactionRedeemByBrandIdOutletIdTerminalIdTransactionDateNotInCampaignCode(String brandId, String outletId, String terminalId, String transactionDate) {
        return Observable.create(sub -> {
            List<String> txRedeems = transactionRepository.getDistinctCampaignCodeByBrandIdAndOutletIdAndTerminalIdAndTransactionTypeAndStatusAndCreateDateNotInCampaignCode(brandId, outletId, terminalId, TransactionType.REDEEM_BENEFIT.name(), TransactionType.REDEEM_MARKUSE.name(), Status.SUCCESS.name(), transactionDate, Campaign.E_COUPON.getParameterLike(), Campaign.E_STAMP.getParameterLike());
            if (!txRedeems.isEmpty()) {
                sub.onNext(txRedeems);
            } else {
                sub.onNext(Collections.emptyList());
            }
            sub.onComplete();
        });
    }

    private Observable<List<String>> getTransactionRedeemByBrandIdOutletIdTerminalIdTransactionDateCampaignCode(String brandId, String outletId, String terminalId, String transactionDate, String campaignCode) {
        return Observable.create(sub -> {
            List<String> txRedeems = transactionRepository.getCampaignByBrandIdAndOutletIdAndTerminalIdAndTransactionTypeAndStatusAndCreateDateCampaignCode(brandId, outletId, terminalId, TransactionType.REDEEM_BENEFIT.name(), TransactionType.REDEEM_MARKUSE.name(), Status.SUCCESS.name(), transactionDate, campaignCode);
            if (!txRedeems.isEmpty()) {
                sub.onNext(txRedeems);
            } else {
                sub.onNext(Collections.emptyList());
            }
            sub.onComplete();
        });
    }


    private Observable<List<CampaignDto>> getCampaignsByBrandIdOutletIdTerminalId(List<String> txRedeems, String brandId, String outletId, String terminalId) {
        return Observable.create(sub -> {
            if (!txRedeems.isEmpty()) {
                List<CampaignDto> campaigns = txRedeems.stream().map(campaignCode -> {
                    String campaignName = getCampaignByBrandIdAndOutletIdAndTerminalIdAndCampaign(brandId, outletId, terminalId, campaignCode).subscribeOn(Schedulers.io()).blockingFirst();
                    int campaignTotal = transactionRepository.getCountCampaignCodeByBrandIdAndOutletIdAndTerminalIdAndTransactionTypeAndStatusAndSettleStatus(brandId, outletId, terminalId, TransactionType.REDEEM_BENEFIT.name(), TransactionType.REDEEM_MARKUSE.name(), Status.SUCCESS.name(), Status.PENDING.name(), campaignCode);
                    return new CampaignDto(String.valueOf(campaignTotal), campaignCode, campaignName);
                }).collect(Collectors.toList());
                sub.onNext(campaigns);
            } else {
                sub.onNext(Collections.emptyList());
            }
            sub.onComplete();
        });
    }

    private Observable<List<CampaignDto>> getCampaignByBrandIdOutletIdTerminalIdCampaignCode(List<String> txRedeems, String brandId, String outletId, String terminalId, String campaginCodeMain) {
        return Observable.create(sub -> {
            if (!txRedeems.isEmpty()) {
                List<CampaignDto> campaigns = txRedeems.stream().map(campaignCode -> {
                    String campaignName = getCampaignByBrandIdAndOutletIdAndTerminalIdAndCampaign(brandId, outletId, terminalId, campaignCode).subscribeOn(Schedulers.io()).blockingFirst();
                    int campaignTotal = transactionRepository.getCountCampaignCodeEstampByBrandIdAndOutletIdAndTerminalIdAndTransactionTypeAndStatusAndSettleStatus(brandId, outletId, terminalId, TransactionType.REDEEM_BENEFIT.name(), TransactionType.REDEEM_MARKUSE.name(), Status.SUCCESS.name(), Status.PENDING.name(), campaginCodeMain);
                    return new CampaignDto(String.valueOf(campaignTotal), campaignCode, campaignName);
                }).collect(Collectors.toList());
                sub.onNext(campaigns);
            } else {
                sub.onNext(Collections.emptyList());
            }
            sub.onComplete();
        });
    }

    private Observable<Integer> getCountCouponCodeByEarnEstampEstampByBrandIdAndOutletIdAndTerminalIdAndTransactionTypeAndStatusAndSettleStatus(List<String> txRedeems, String brandId, String outletId, String terminalId, String campaginCodeMain, String couponCode) {
        return Observable.create(sub -> {
            if (!txRedeems.isEmpty()) {
                int eStampTotal = transactionRepository.getCountCouponCodeEstampByBrandIdAndOutletIdAndTerminalIdAndTransactionTypeAndStatusAndSettleStatus(brandId, outletId, terminalId, TransactionType.REDEEM_BENEFIT.name(), TransactionType.REDEEM_MARKUSE.name(), Status.SUCCESS.name(), Status.PENDING.name(), campaginCodeMain, couponCode);
                sub.onNext(eStampTotal);
            } else {
                sub.onNext(0);
            }
            sub.onComplete();
        });
    }

    private Observable<List<CampaignDto>> getCampaignsByBrandIdOutletIdTerminalIdTransactionDate(List<String> txRedeems, String brandId, String outletId, String terminalId, String transactionDate) {
        return Observable.create(sub -> {
            if (!txRedeems.isEmpty()) {
                List<CampaignDto> campaigns = txRedeems.stream().map(campaignCode -> {
                    String campaignName = getCampaignByBrandIdAndOutletIdAndTerminalIdAndCampaign(brandId, outletId, terminalId, campaignCode).subscribeOn(Schedulers.io()).blockingFirst();
                    int campaignTotal = transactionRepository.getCountCampaignCodeByBrandIdAndOutletIdAndTerminalIdAndTransactionTypeAndStatusAndCamapignCodeAndCreateDate(brandId, outletId, terminalId, TransactionType.REDEEM_BENEFIT.name(), TransactionType.REDEEM_MARKUSE.name(), Status.SUCCESS.name(), campaignCode, transactionDate);
                    return new CampaignDto(String.valueOf(campaignTotal), campaignCode, campaignName);
                }).collect(Collectors.toList());
                sub.onNext(campaigns);
            } else {
                sub.onNext(Collections.emptyList());
            }
            sub.onComplete();
        });
    }

    private Observable<String> getCampaignByBrandIdAndOutletIdAndTerminalIdAndCampaign(String brandId, String outletId, String terminalId, String campaignId) {
        return Observable.create((ObservableEmitter<String> sub) -> {

            ApiEdcCampaignDetailResponseDto campaignDetailResponseDto = apiEdcCampaignService.getCampaignDetail(brandId, outletId, terminalId, campaignId);
            List<ApiEdcCampaignDetailResponseDto.Data> campaigns = (List<ApiEdcCampaignDetailResponseDto.Data>) campaignDetailResponseDto.getData();
            String campaignName = campaigns
                    .stream()
                    .filter(campaign -> campaign.getCampaignId().equals(campaignId))
                    .findFirst().orElseThrow(() -> new CustomThirdPartyExcepton(new ApiStatus("410", "campaignId not found")))
                    .getCampaignName();

            sub.onNext(campaignName);
            sub.onComplete();
        });
    }

    private void builderQueryParam(UriComponentsBuilder builder, String key, Object value) {
        if (Objects.nonNull(value)) {
            builder.queryParam(key, value);
        }
    }

    private Observable<List<Transaction>> getTransactionByBrandIdAndOutletIdAndTerminalIdAndTransactionTypeInAndStatusAndSettleStatus(String brandId, String outletId, String terminalId) {
        return Observable.create(sub -> {
            List<TransactionType> transactionTypes = Arrays.asList(TransactionType.PAYMENT_CHARGE, TransactionType.PAYMENT_CANCEL, TransactionType.POINT_EARN, TransactionType.REDEEM_MARKUSE);
            List<Transaction> transactions = transactionRepository.findByBrandIdAndOutletIdAndTerminalIdAndTransactionTypeInAndStatusAndSettleStatus(brandId, outletId, terminalId, transactionTypes, Status.SUCCESS, Status.PENDING);
            if (!transactions.isEmpty()) {
                sub.onNext(transactions);
            } else {
                sub.onNext(Collections.emptyList());
            }
            sub.onComplete();
        });
    }

    private Observable<List<Transaction>> getTransactionByBrandIdAndOutletIdAndTerminalIdAndStatusAndTransactionTypeInAndCreateDate(String brandId, String outletId, String terminalId, String transactionDate) {
        return Observable.create(sub -> {
            List<Transaction> transactions = transactionRepository.findByBrandIdAndOutletIdAndTerminalIdAndStatusAndTransactionTypeInAndCreateDate(brandId, outletId, terminalId, Status.SUCCESS.name(), transactionDate);
            if (!transactions.isEmpty()) {
                sub.onNext(transactions);
            } else {
                sub.onNext(Collections.emptyList());
            }
            sub.onComplete();
        });
    }

    private Observable<PaymentDto> setPayment(List<Transaction> transactions) {
        return Observable.create(sub -> {
            Supplier<Stream<Transaction>> paymentStream = () -> transactions.stream().filter(tx -> (tx.getTransactionType() == TransactionType.PAYMENT_CHARGE));
            Supplier<Stream<Transaction>> paymentCancelStream = () -> transactions.stream().filter(tx -> (tx.getTransactionType() == TransactionType.PAYMENT_CANCEL));
            int paymentChargesTotal = (int) paymentStream.get().count();
            int paymentCancelTotal = (int) paymentCancelStream.get().count();
            String paymentTotal = String.valueOf(paymentChargesTotal - paymentCancelTotal);
            int paymentAmount = paymentStream.get().mapToInt(tx -> Integer.valueOf(tx.getAmount())).sum();
            int paymentCancelAmount = paymentCancelStream.get().mapToInt(tx -> Integer.valueOf(tx.getAmount())).sum();
            String paymentAmountTotal = String.valueOf(paymentAmount - paymentCancelAmount);
            PaymentDto paymentDto = new PaymentDto();
            paymentDto.setAmount(getAmountDivideOneHundred2Digit(paymentAmountTotal));
            paymentDto.setTotal(paymentTotal);
            sub.onNext(paymentDto);
            sub.onComplete();
        });
    }

    private Observable<RedeemDto> setRedeem(List<CampaignDto> campaigns, int pointTotal) {
        return Observable.create(sub -> {
            int redeemTotal = campaigns.stream().mapToInt(tx -> Integer.valueOf(tx.getTotal())).sum();
            RedeemDto redeem = new RedeemDto();
            redeem.setTotal(String.valueOf(redeemTotal));
            redeem.setPoint(String.valueOf(pointTotal));
            redeem.setCampaigns(campaigns);
            sub.onNext(redeem);
            sub.onComplete();
        });
    }

    private Observable<EstampDto> setEstamp(int earnEstampTotal, int markuseEstampTotal) {
        return Observable.create(sub -> {
            EstampDto estampDto = new EstampDto();
            estampDto.setTotalEarnEstamp((String.valueOf(earnEstampTotal)));
            estampDto.setTotalMarkuseEstamp((String.valueOf(markuseEstampTotal)));
            sub.onNext(estampDto);
            sub.onComplete();
        });
    }

    private Observable<String> setECoupon(List<CampaignDto> campaigns) {
        return Observable.create(sub -> {
            int eStampTotal = campaigns.stream().mapToInt(tx -> Integer.valueOf(tx.getTotal())).sum();
            sub.onNext(String.valueOf(eStampTotal));
            sub.onComplete();
        });
    }

    private Observable<EarnDto> setEarn(List<Transaction> transactions) {
        return Observable.create((ObservableEmitter<EarnDto> sub) -> {
            EarnDto earn = new EarnDto();
            Supplier<Stream<Transaction>> earnStream = () -> transactions.stream().filter(tx -> (tx.getTransactionType() == TransactionType.POINT_EARN));
            String earnTotal = String.valueOf(earnStream.get().count());
            //String earnAmountTotal = String.valueOf(earnStream.get().mapToInt(e -> new BigInteger(e.getAmount()).sum());
            //String earnPointTotal = String.valueOf(earnStream.get().mapToInt(Transaction::getPoint).sum());
            String earnAmountTotal = summaryAmount(transactions);
            String earnPointTotal = summaryPoint(transactions);
            earn.setTotal(earnTotal);
            earn.setAmount(getAmountDivideOneHundred2Digit(earnAmountTotal));
            earn.setPoint(earnPointTotal);
            sub.onNext(earn);
            sub.onComplete();
        });
    }


    public String summaryAmount(List<Transaction> transactions) {
        BigInteger sum = BigInteger.ZERO;
        for (Transaction transaction : transactions) {
            if (transaction.getTransactionType() == TransactionType.POINT_EARN) {
                sum = sum.add(new BigInteger(transaction.getAmount()));
            }
        }
        return String.valueOf(sum);
    }

    public String summaryPoint(List<Transaction> transactions) {
        BigInteger sum = BigInteger.ZERO;
        for (Transaction transaction : transactions) {
            if (transaction.getTransactionType() == TransactionType.POINT_EARN) {
                sum = sum.add(BigInteger.valueOf(transaction.getPoint()));
            }
        }
        return String.valueOf(sum);
    }

    private String getAmountDivideOneHundred2Digit(String amount) {
        BigDecimal amountDivide = new BigDecimal("100");
        BigDecimal total = new BigDecimal(amount).divide(amountDivide);
        return new DecimalFormat("##############0.00").format(total);
    }

    private Observable<List<PaymentDto>> setPayments(List<Transaction> transactions) {
        return Observable.create((ObservableEmitter<List<PaymentDto>> sub) -> {
            Supplier<Stream<Transaction>> paymentWalletStream = () -> transactions.stream().filter(tx -> (tx.getPaymentMethod() == PaymentMethod.WALLET));
            Supplier<Stream<Transaction>> paymentAliPayStream = () -> transactions.stream().filter(tx -> (tx.getPaymentMethod() == PaymentMethod.ALIPAY));

            PaymentDto wallet = setPayment(paymentWalletStream.get().collect(Collectors.toList())).subscribeOn(Schedulers.io()).blockingFirst();
            wallet.setPaymentMethod(PaymentMethod.WALLET);

            PaymentDto aliPay = setPayment(paymentAliPayStream.get().collect(Collectors.toList())).subscribeOn(Schedulers.io()).blockingFirst();
            aliPay.setPaymentMethod(PaymentMethod.ALIPAY);
            sub.onNext(Arrays.asList(wallet, aliPay));
            sub.onComplete();
        });
    }

    private Observable<String> getMerchantByMerchantId(String merchantId) {
        return Observable.create(sub -> {
            Optional<Merchant> merchant = merchantRepository.findByMerchantId(merchantId);
            String merchantName = "";
            if (merchant.isPresent()) {
                if (merchant.get().getStoreNameTh() != null) {
                    merchantName = merchant.get().getStoreNameTh();
                }
            }
            sub.onNext(merchantName);
            sub.onComplete();
        });
    }


    public String getRefType(AccountType accountType) {
        switch (accountType) {
            case TRUECARD:
                return "True card";
            case MOBILE:
                return "Mobile";
            case THAIID:
                return "Thai ID";
            default:
                return "";
        }
    }

    private DetailSummaryTransaction setDetailSummaryTransaction(Transaction tx) {
        DetailSummaryTransaction transaction = new DetailSummaryTransaction();
        String refTpe = tx.getAccountType() != null ? getRefType(tx.getAccountType()) : "";
        transaction.setRefType(refTpe);
        transaction.setRefValue(tx.getCustomerRef());
        transaction.setTraceId(tx.getTraceId());
        transaction.setAmount(getAmountDivideOneHundred2Digit(tx.getAmount()));
        transaction.setPoint(String.valueOf(tx.getPoint()));
        transaction.setTransactionDate(tx.getCreateDate());
        if (tx.getTransactionType() == TransactionType.REDEEM_BENEFIT || tx.getTransactionType() == TransactionType.REDEEM_MARKUSE || tx.getTransactionType() == TransactionType.REDEEM_ROLLBACK) {
            String campaignName = "";
            Optional<Map> campaignDto = campaignInfo.stream()
                    .filter(camp -> Objects.equals(camp.get("campaignCode"), tx.getCampaignCode()))
                    .findFirst();
            if (campaignDto.isPresent()) {
                campaignName = (String) campaignDto.get().get("campaignName");
            }
            transaction.setTransactionName(campaignName);
            transaction.setRefValue(tx.getCustomerRef() != null ? tx.getCustomerRef() : tx.getCouponCode());
        } else if (tx.getTransactionType() == TransactionType.POINT_EARN || tx.getTransactionType() == TransactionType.POINT_CANCEL) {
            transaction.setTransactionName("Point earn");
        } else {
            transaction.setRefType("Payment code");
            transaction.setRefValue(tx.getPaymentCode());
            transaction.setTransactionName(tx.getPaymentMethod().name());
        }
        return transaction;
    }
}
