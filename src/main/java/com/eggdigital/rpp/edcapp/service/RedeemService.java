package com.eggdigital.rpp.edcapp.service;

import com.eggdigital.rpp.edcapp.dto.RedeemByCampaignRequestDto;
import com.eggdigital.rpp.edcapp.dto.RedeemByCampaignResponseDto;
import com.eggdigital.rpp.edcapp.dto.RedeemByCodeRequestDto;
import com.eggdigital.rpp.edcapp.dto.RedeemByCodeResponseDto;
import com.eggdigital.rpp.edcapp.handler.ApiErrorException;

import java.io.IOException;

/**
 * Created by jaruwan.yen on 10/16/2017 AD.
 */
public interface RedeemService {

    RedeemByCampaignResponseDto redeemByCampaign(RedeemByCampaignRequestDto dto) throws ApiErrorException, IOException;

    RedeemByCodeResponseDto redeemByCode(RedeemByCodeRequestDto dto) throws ApiErrorException, IOException;

}
