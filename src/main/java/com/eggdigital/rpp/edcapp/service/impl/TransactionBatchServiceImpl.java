package com.eggdigital.rpp.edcapp.service.impl;

import com.eggdigital.common.util.DateUtils;
import com.eggdigital.rpp.edcapp.domain.Status;
import com.eggdigital.rpp.edcapp.domain.TransactionBatch;
import com.eggdigital.rpp.edcapp.repository.TransactionBatchRepository;
import com.eggdigital.rpp.edcapp.service.TransactionBatchService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Created by bombay on 10/19/2017 AD.
 */
@Service
@Transactional
public class TransactionBatchServiceImpl implements TransactionBatchService {

    @Autowired
    private TransactionBatchRepository transactionBatchRepository;

    @Override
    public TransactionBatch genTransactionBatch(String brandId, String outletId, String terminalId) {
        String txRefId = StringUtils.rightPad(DateUtils.formatDate(new Date(), "yyyyMMddHHmmss") + getRandomNumberInRange(1, 99), 16, '0');
        Optional<TransactionBatch> transactionBatchOp = transactionBatchRepository.findFirstByBrandIdAndOutletIdAndTerminalIdAndStatusOrderByTraceIdDesc(brandId, outletId, terminalId, Status.PENDING.name());
        TransactionBatch transactionBatchNew = new TransactionBatch();
        if (transactionBatchOp.isPresent()) {
            TransactionBatch transactionBatch = transactionBatchOp.get();
            transactionBatchNew.setBatchId(transactionBatch.getBatchId());
            transactionBatchNew.setTraceId(genIdLeftPad6Digit(transactionBatch.getTraceId()));
        } else {
            Optional<TransactionBatch> transactionBatchSettleOp = transactionBatchRepository.findFirstByBrandIdAndOutletIdAndTerminalIdAndStatusOrderByBatchIdDesc(brandId, outletId, terminalId, Status.SETTLE.name());
            if (transactionBatchSettleOp.isPresent()) {
                transactionBatchNew.setBatchId(genIdLeftPad6Digit(transactionBatchSettleOp.get().getBatchId()));
            } else {
                transactionBatchNew.setBatchId(genIdLeftPad6Digit("0"));
            }
            transactionBatchNew.setTraceId(genIdLeftPad6Digit("0"));
        }
        transactionBatchNew.setBrandId(brandId);
        transactionBatchNew.setOutletId(outletId);
        transactionBatchNew.setTerminalId(terminalId);
        transactionBatchNew.setTxRefId(txRefId);
        transactionBatchNew.setStatus(Status.PENDING);
        return transactionBatchRepository.save(transactionBatchNew);
    }

    @Override
    public List<TransactionBatch> updateByBrandIdOutletIdTerminalIdStatusSettle(String brandId, String outletId, String terminalId, Status status) {
        List<TransactionBatch> transactionBatches = transactionBatchRepository.findByBrandIdAndOutletIdAndTerminalIdAndStatus(brandId, outletId, terminalId, status)
                .stream()
                .peek(t -> t.setStatus(Status.SETTLE))
                .collect(Collectors.toList());
        return transactionBatchRepository.save(transactionBatches);
    }

    private int getRandomNumberInRange(int min, int max) {
        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }
        Random random = new Random();
        return random.nextInt((max - min) + 1) + min;
    }

    public String genIdLeftPad6Digit(String index) {
        int lastIndex = Integer.valueOf(index);
        return StringUtils.leftPad(String.valueOf(lastIndex + 1), 6, "0");
    }

}
