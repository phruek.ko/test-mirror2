package com.eggdigital.rpp.edcapp.service;

import com.eggdigital.rpp.edcapp.dto.EarnPointRequestDto;
import com.eggdigital.rpp.edcapp.dto.EarnPointResponseDto;
import com.eggdigital.rpp.edcapp.handler.ApiErrorException;

import java.io.IOException;

public interface PointService {

    EarnPointResponseDto issue(EarnPointRequestDto requestDto) throws ApiErrorException, IOException;
}
