package com.eggdigital.rpp.edcapp.service.impl;

import com.eggdigital.rpp.edcapp.api.model.ApiStatus;
import com.eggdigital.rpp.edcapp.common.EdcCommonHelper;
import com.eggdigital.rpp.edcapp.domain.*;
import com.eggdigital.rpp.edcapp.dto.EarnPointRequestDto;
import com.eggdigital.rpp.edcapp.dto.EarnPointResponseDto;
import com.eggdigital.rpp.edcapp.dto.PointIssueRequest;
import com.eggdigital.rpp.edcapp.enumuration.TransactionChannel;
import com.eggdigital.rpp.edcapp.exception.CustomThirdPartyExcepton;
import com.eggdigital.rpp.edcapp.handler.ApiErrorException;
import com.eggdigital.rpp.edcapp.merchant.service.MerchantService;
import com.eggdigital.rpp.edcapp.ms.RppBsApi;
import com.eggdigital.rpp.edcapp.ms.dto.BsPointIssueResponse;
import com.eggdigital.rpp.edcapp.repository.MerchantRepository;
import com.eggdigital.rpp.edcapp.service.PointService;
import com.eggdigital.rpp.edcapp.service.TransactionBatchService;
import com.eggdigital.rpp.edcapp.service.TransactionService;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Optional;

import static com.eggdigital.rpp.edcapp.domain.TransactionType.POINT_EARN;

@Service
@Transactional
public class PointServiceImpl implements PointService {

    private final Logger log = LoggerFactory.getLogger(PointServiceImpl.class);

    @Autowired
    private RppBsApi rppBsApi;

    @Autowired
    private TransactionBatchService transactionBatchService;

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private MerchantRepository merchantRepository;

    @Autowired
    private MerchantService merchantService;

    @Override
    public EarnPointResponseDto issue(EarnPointRequestDto requestDto) throws ApiErrorException, IOException {
        //todo implement after migration data and implement registry on prem
        //Optional<Merchant> merchant = merchantService.findByMerchantId(requestDto.getBrandId());
        Optional<Merchant> merchant = merchantRepository.findByMerchantId(requestDto.getBrandId());
        if (!merchant.isPresent()) {
            throw new CustomThirdPartyExcepton(new ApiStatus("4102", "Not found data for Merchant"));
        }

        TransactionBatch batch = transactionBatchService.genTransactionBatch(requestDto.getBrandId(), requestDto.getOutletId(), requestDto.getTerminalId());
        Transaction tx = new Transaction(batch, POINT_EARN);
        tx.setBrandId(requestDto.getBrandId());
        tx.setOutletId(requestDto.getOutletId());
        tx.setTerminalId(requestDto.getTerminalId());
        tx.setAccountType(AccountType.valueOf(requestDto.getAccType()));
        tx.setCustomerRef(requestDto.getAccValue());
        tx.setAmount(requestDto.getAmount());
        tx.setCurrency(Currency.THB);
        if(!Strings.isNullOrEmpty(requestDto.getTransactionChannel())) {
            tx.setTransactionChannel(TransactionChannel.valueOf(requestDto.getTransactionChannel()));
        }
        tx = transactionService.saveTx(tx);

        try {
            String amountSatang = requestDto.getAmount();
            String amountBaht = EdcCommonHelper.Companion.convertSatangStringToBahtTwoDecimalFormat(amountSatang);
            String accountType = requestDto.getAccType();
            if (accountType.equals("TRUECARD")) {
                accountType = "CARD";
            }
            PointIssueRequest pointIssueRequest = new PointIssueRequest();
            pointIssueRequest.setTxRefId(batch.getTxRefId());
            pointIssueRequest.setAccType(accountType);
            pointIssueRequest.setAccValue(requestDto.getAccValue());
            pointIssueRequest.setSaleAmt(amountBaht);
            pointIssueRequest.setDiscountAmt("0");
            pointIssueRequest.setPaymentAmt("0");
            pointIssueRequest.setBrandId(requestDto.getBrandId());
            pointIssueRequest.setOutletId(requestDto.getOutletId());
            pointIssueRequest.setTerminalId(requestDto.getTerminalId());

            BsPointIssueResponse result = rppBsApi.issuePoint(pointIssueRequest);
            tx.setStatus(Status.SUCCESS);
            tx.setStatusCode(String.valueOf(result.getStatus().getCode()));
            tx.setStatusDescription(result.getStatus().getMessage());

            BsPointIssueResponse.Data resultData = result.getData();
            tx.setPoint(resultData.getPoint());
            tx = transactionService.saveTx(tx);

            resultData.setTransaction(tx);

            LocalDateTime createdAt = LocalDateTime.ofInstant(tx.getCreateDate().toInstant(), ZoneId.systemDefault());

            EarnPointResponseDto responseDto = new EarnPointResponseDto(
                    tx.getBrandId(),
                    tx.getOutletId(),
                    tx.getTerminalId(),
                    tx.getTraceId(),
                    tx.getBatchId(),
                    tx.getRefId(),
                    createdAt,
                    merchant.isPresent() ? merchant.get().getTrueyouId() : "",
                    tx.getAmount(),
                    tx.getAccountType().name(),
                    tx.getCustomerRef(),
                    String.valueOf(tx.getPoint())
            );

            return responseDto;

        } catch (Exception ex) {
            log.error(null, ex);
            transactionService.updateTxFailureByException(tx.getId(), ex);
            throw ex;
        }
    }
}
