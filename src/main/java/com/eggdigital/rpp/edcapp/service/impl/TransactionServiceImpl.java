package com.eggdigital.rpp.edcapp.service.impl;

import com.eggdigital.rpp.edcapp.api.ApiKotlinErrorException;
import com.eggdigital.rpp.edcapp.api.model.ApiStatus;
import com.eggdigital.rpp.edcapp.domain.Status;
import com.eggdigital.rpp.edcapp.domain.Transaction;
import com.eggdigital.rpp.edcapp.handler.ApiError;
import com.eggdigital.rpp.edcapp.handler.ApiErrorException;
import com.eggdigital.rpp.edcapp.repository.TransactionRepository;
import com.eggdigital.rpp.edcapp.service.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;

/**
 * Created by bombay on 10/16/2017 AD.
 */
@Service
@Transactional
public class TransactionServiceImpl implements TransactionService {

    private final Logger log = LoggerFactory.getLogger(TransactionServiceImpl.class);

    @Autowired
    private TransactionRepository transactionRepository;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Override
    public Transaction saveTx(Transaction transaction) {
        return transactionRepository.save(transaction);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Override
    public void updateTxFailureByException(BigInteger id, Exception ex) {
        Transaction tx = transactionRepository.findOne(id);
        if (tx != null) {
            tx.setStatus(Status.FAIL);

            if (ex instanceof ApiErrorException) {
                ApiError apiError = ((ApiErrorException) ex).getApiError();
                tx.setStatusCode(apiError.getStatus().getCode());
                String msg = apiError.getStatus().getMessage().length() > 100 ?
                        apiError.getStatus().getMessage().substring(0, 100) :
                        apiError.getStatus().getMessage();
                tx.setStatusDescription(msg);
            } else if (ex instanceof ApiKotlinErrorException) {
                ApiStatus apiStatus = ((ApiKotlinErrorException) ex).getStatus();
                tx.setStatusCode(apiStatus.getCode());
                String msg = apiStatus.getMessage().length() > 100 ?
                        apiStatus.getMessage().substring(0, 100) :
                        apiStatus.getMessage();
                tx.setStatusDescription(msg);
            }
        }
    }
}
