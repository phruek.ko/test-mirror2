package com.eggdigital.rpp.edcapp.service;


import com.eggdigital.common.exception.SmsException;

import java.math.BigDecimal;

/**
 * Created by sitthi on 2/14/2018 AD.
 */
public interface SMSService {

    String sendRedeemByCampaign(String toNumber, String campaignName, String traceId) throws SmsException;

    String sendRedeemByCode(String toNumber, String campaignName, String rewardCode, String traceId) throws SmsException;

    String sendEarnPoint(String toNumber, BigDecimal total, BigDecimal point) throws SmsException;

//    String sendCheckPointTrueCard(String toNumber, String name, String status, String expireDateStr, Integer point, Integer expiringPoint1, Integer expiringPoint2, Integer expiringPoint3) throws SmsException;

//    String sendCheckPointNonTrue(String toNumber, String name, Integer point, Integer expiringPoint1, Integer expiringPoint2, Integer expiringPoint3) throws SmsException;

    String sendVoidRedeemByCampaign(String toNumber, String campaignName) throws SmsException;

//    String sendVoidIssuePoint(String toNumber, String campaignName) throws SmsException;

    String sendVoidEarnPoint(String toNumber, String amount, String point) throws SmsException;

    String sendVoidPayment(String toNumber, String amount) throws SmsException;
}