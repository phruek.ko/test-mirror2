package com.eggdigital.rpp.edcapp.service.impl;

import com.eggdigital.rpp.edcapp.api.bs.ApiBsService;
import com.eggdigital.rpp.edcapp.api.bs.dto.ApiBsCustomerInfoResponseDto;
import com.eggdigital.rpp.edcapp.api.mdc.ApiMdcService;
import com.eggdigital.rpp.edcapp.api.mdc.dto.ApiMdcSearchAllProductResponseDto;
import com.eggdigital.rpp.edcapp.domain.AccountType;
import com.eggdigital.rpp.edcapp.domain.Status;
import com.eggdigital.rpp.edcapp.domain.Transaction;
import com.eggdigital.rpp.edcapp.domain.TransactionBatch;
import com.eggdigital.rpp.edcapp.dto.CardInfoRequest;
import com.eggdigital.rpp.edcapp.dto.CardInfoSlip;
import com.eggdigital.rpp.edcapp.handler.ApiErrorException;
import com.eggdigital.rpp.edcapp.ms.RppBsApi;
import com.eggdigital.rpp.edcapp.ms.dto.BsCustomerCardInfoResponse;
import com.eggdigital.rpp.edcapp.service.CustomerService;
import com.eggdigital.rpp.edcapp.service.TransactionBatchService;
import com.eggdigital.rpp.edcapp.service.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.eggdigital.rpp.edcapp.domain.TransactionType.CUSTOMER_CARD_SLIP;

@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {

    private final Logger log = LoggerFactory.getLogger(CustomerServiceImpl.class);

    @Autowired
    private RppBsApi rppBsApi;

    @Autowired
    private TransactionBatchService transactionBatchService;

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private ApiBsService apiBsService;

    @Autowired
    private ApiMdcService apiMdcService;


    @Override
    public List<String> getCustomerInfo(String terminalId, AccountType accType, String accValue) {

        List<String> mobileNumbers = new ArrayList<>();

        String newAccountType = accType.toString();
        if (accType == AccountType.TRUECARD) {
            newAccountType = "CARD";
        }

        ApiBsCustomerInfoResponseDto customerInfoResponseDto = apiBsService.getCustomerInfo(terminalId, newAccountType, accValue);

        if (customerInfoResponseDto.getError() != null) {
            return mobileNumbers;
        }

        String thaiId = customerInfoResponseDto.getCardInfo().getThaiId();

        ApiMdcSearchAllProductResponseDto mdcSearchAllProductResponseDto = apiMdcService.getSearchAllProduct(thaiId);

        if (!mdcSearchAllProductResponseDto.getHeader().getCode().equals("200")) {
            return mobileNumbers;
        }

        HashMap<String, Object> mobileNumbersMap = (HashMap) ((ApiMdcSearchAllProductResponseDto.Data) mdcSearchAllProductResponseDto.getData())
                .getProduct().getTmvhProduct();


        mobileNumbersMap.forEach((s, o) -> {
            mobileNumbers.add(s);
        });

        return mobileNumbers;
    }

    @Override
    public CardInfoSlip saveCardSlipTx(CardInfoRequest requestDto) throws ApiErrorException {
        TransactionBatch batch = transactionBatchService.genTransactionBatch(requestDto.getBrand_id(), requestDto.getOutlet_id(), requestDto.getTerminal_id());
        Transaction tx = new Transaction(batch, CUSTOMER_CARD_SLIP);
        tx.setBrandId(requestDto.getBrand_id());
        tx.setOutletId(requestDto.getOutlet_id());
        tx.setTerminalId(requestDto.getTerminal_id());
        tx.setAccountType(AccountType.fromType(requestDto.getAcc_type()));
        tx.setCustomerRef(requestDto.getAcc_value());
        tx = transactionService.saveTx(tx);

        try {
            requestDto.setTx_ref_id(batch.getTxRefId());

            BsCustomerCardInfoResponse result = rppBsApi.getCustomerCardInfo(requestDto);
            tx.setStatus(Status.SUCCESS);
            tx.setStatusCode(String.valueOf(result.getStatus().getCode()));
            tx.setStatusDescription(result.getStatus().getMessage());
            tx = transactionService.saveTx(tx);
            return new CardInfoSlip(tx, result.getData());

        } catch (Exception ex) {
            log.error(null, ex);
            transactionService.updateTxFailureByException(tx.getId(), ex);
            throw ex;
        }
    }
}
