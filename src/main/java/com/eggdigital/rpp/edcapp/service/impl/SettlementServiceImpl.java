package com.eggdigital.rpp.edcapp.service.impl;

import com.eggdigital.rpp.edcapp.domain.Status;
import com.eggdigital.rpp.edcapp.domain.Transaction;
import com.eggdigital.rpp.edcapp.domain.TransactionBatch;
import com.eggdigital.rpp.edcapp.dto.SettlementTransaction;
import com.eggdigital.rpp.edcapp.dto.SettlementTransactionByBrand;
import com.eggdigital.rpp.edcapp.repository.TransactionBatchRepository;
import com.eggdigital.rpp.edcapp.repository.TransactionRepository;
import com.eggdigital.rpp.edcapp.service.SettlementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by bombay on 11/14/2017 AD.
 */
@Transactional
@Service
public class SettlementServiceImpl implements SettlementService {

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private TransactionBatchRepository transactionBatchRepository;

    @Override
    public SettlementTransaction settlements() {
        try {
            List<Transaction> transactions = transactionRepository.findBySettleStatus(Status.PENDING);
            updateTransactionSettleStatus(transactions);

            List<TransactionBatch> transactionBatches = transactionBatchRepository.findByStatus(Status.PENDING);
            updateTransactionฺBatchSettleStatus(transactionBatches);
            return new SettlementTransaction("settlement success");
        } catch (Exception e) {
            return new SettlementTransaction("settlement fail");
        }
    }

    @Override
    public SettlementTransactionByBrand settlementsByBrandId(String brandId) {
        try {
            List<Transaction> transactions = transactionRepository.findByBrandIdAndSettleStatus(brandId, Status.PENDING);
            updateTransactionSettleStatus(transactions);

            List<TransactionBatch> transactionBatches = transactionBatchRepository.findByBrandIdAndStatus(brandId, Status.PENDING);
            updateTransactionฺBatchSettleStatus(transactionBatches);
            return new SettlementTransactionByBrand("settlement by brand id success", brandId);
        } catch (Exception e) {
            return new SettlementTransactionByBrand("settlement by brand id fail", brandId);
        }
    }

    @Transactional
    private void updateTransactionSettleStatus(List<Transaction> transactions) {
        transactions = transactions.stream().map(tx -> {
            tx.setSettleStatus(Status.SETTLE);
            tx.setSettleDate(new Date());
            return tx;
        }).collect(Collectors.toList());
        transactionRepository.save(transactions);
    }

    @Transactional
    private void updateTransactionฺBatchSettleStatus(List<TransactionBatch> transactionBatches) {
        transactionBatches = transactionBatches.stream().map(tx -> {
            tx.setStatus(Status.SETTLE);
            return tx;
        }).collect(Collectors.toList());

        transactionBatchRepository.save(transactionBatches);
    }
}
