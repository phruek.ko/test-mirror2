package com.eggdigital.rpp.edcapp.service.impl;

import com.eggdigital.rpp.edcapp.api.ApiKotlinErrorException;
import com.eggdigital.rpp.edcapp.api.edccampaign.ApiEdcCampaignService;
import com.eggdigital.rpp.edcapp.api.edccampaign.dto.ApiEdcCampaignDetailResponseDto;
import com.eggdigital.rpp.edcapp.api.edccampaign.dto.ApiEdcCampaignShortRedeemResponseDto;
import com.eggdigital.rpp.edcapp.api.model.ApiStatus;
import com.eggdigital.rpp.edcapp.domain.*;
import com.eggdigital.rpp.edcapp.dto.*;
import com.eggdigital.rpp.edcapp.enumuration.CampaignType;
import com.eggdigital.rpp.edcapp.enumuration.TransactionChannel;
import com.eggdigital.rpp.edcapp.exception.CustomThirdPartyExcepton;
import com.eggdigital.rpp.edcapp.handler.ApiErrorException;
import com.eggdigital.rpp.edcapp.merchant.service.MerchantService;
import com.eggdigital.rpp.edcapp.ms.RppBsApi;
import com.eggdigital.rpp.edcapp.ms.dto.BsRedeemByCampaignResponse;
import com.eggdigital.rpp.edcapp.ms.dto.BsRedeemByCodeResponse;
import com.eggdigital.rpp.edcapp.repository.MerchantRepository;
import com.eggdigital.rpp.edcapp.service.RedeemService;
import com.eggdigital.rpp.edcapp.service.TransactionBatchService;
import com.eggdigital.rpp.edcapp.service.TransactionService;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;

import static com.eggdigital.rpp.edcapp.domain.TransactionType.REDEEM_BENEFIT;
import static com.eggdigital.rpp.edcapp.domain.TransactionType.REDEEM_MARKUSE;

/**
 * Created by jaruwan.yen on 10/16/2017 AD.
 */
@Service
@Transactional
public class RedeemServiceImpl implements RedeemService {

    private final Logger log = LoggerFactory.getLogger(RedeemServiceImpl.class);

    @Autowired
    private RppBsApi rppBsApi;

    @Autowired
    private TransactionBatchService transactionBatchService;

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private ApiEdcCampaignService apiEdcCampaignService;

    @Autowired
    private MerchantService merchantService;

    @Autowired
    private MerchantRepository merchantRepository;

    @Override
    public RedeemByCampaignResponseDto redeemByCampaign(RedeemByCampaignRequestDto requestDto) throws ApiErrorException, IOException {

        Optional<Merchant> merchant = merchantRepository.findByMerchantId(requestDto.getBrandId());
        if (!merchant.isPresent()) {
            Optional<Merchant> merchantOptional = merchantService.findByMerchantId(requestDto.getBrandId());
            if (merchantOptional.isPresent()) {
                merchant = merchantOptional;
                merchantService.saveMerchantFromMasterMerchant(merchantOptional.get());
            } else {
                throw new CustomThirdPartyExcepton(new ApiStatus("4102", "Not found data for Merchant"));
            }
        }

        TransactionBatch batch = transactionBatchService.genTransactionBatch(requestDto.getBrandId(), requestDto.getOutletId(), requestDto.getTerminalId());
        Transaction tx = new Transaction(batch, REDEEM_BENEFIT);

        tx.setBrandId(requestDto.getBrandId());
        tx.setOutletId(requestDto.getOutletId());
        tx.setTerminalId(requestDto.getTerminalId());
        tx.setAccountType(AccountType.valueOf(requestDto.getAccType()));
        tx.setCustomerRef(requestDto.getAccValue());
        tx.setCampaignCode(requestDto.getCampaignCode());
        if (!Strings.isNullOrEmpty(requestDto.getTransactionChannel())) {
            tx.setTransactionChannel(TransactionChannel.valueOf(requestDto.getTransactionChannel()));
        }
        if (!Strings.isNullOrEmpty(requestDto.getCampaignType())) {
            tx.setCampaignType(CampaignType.valueOf(requestDto.getCampaignType()));
        }
        if (!Strings.isNullOrEmpty(requestDto.getMobile())) {
            tx.setMobile(requestDto.getMobile());
        }
        tx = transactionService.saveTx(tx);

        try {
            RedeemRequest redeemRequest = new RedeemRequest();
            redeemRequest.setBrandId(requestDto.getBrandId());
            redeemRequest.setOutletId(requestDto.getOutletId());
            redeemRequest.setTerminalId(requestDto.getTerminalId());
            redeemRequest.setTxRefId(batch.getTxRefId());

            String accountType = "THAIID";
            if (AccountType.TRUECARD.equals(AccountType.valueOf(requestDto.getAccType()))) {
                accountType = "CARD";
            }

            redeemRequest.setAccType(accountType);
            redeemRequest.setAccValue(requestDto.getAccValue());
            redeemRequest.setCampaignCode(requestDto.getCampaignCode());

            redeemRequest.setCampaignType(requestDto.getCampaignType());
            redeemRequest.setMobile(requestDto.getMobile());

            BsRedeemByCampaignResponse result = rppBsApi.redeemByCampaign(redeemRequest);
            tx.setStatus(Status.SUCCESS);
            tx.setStatusCode(result.getStatus().getCode());
            tx.setStatusDescription(result.getStatus().getMessage());

            RedeemByCampaignData resultData = result.getData();
            tx.setCouponCode(resultData.getRewardCode());
            tx.setPoint(resultData.getPointsUsed());
            tx = transactionService.saveTx(tx);

            resultData.setTransaction(tx);

            LocalDateTime createdAt = LocalDateTime.ofInstant(tx.getCreateDate().toInstant(), ZoneId.systemDefault());

            RedeemByCampaignResponseDto response = new RedeemByCampaignResponseDto(tx.getBrandId(),
                    tx.getOutletId(),
                    tx.getTerminalId(),
                    tx.getTraceId(),
                    tx.getBatchId(), tx.getRefId(),
                    createdAt,
                    merchant.isPresent() ? merchant.get().getTrueyouId() : "",
                    tx.getCustomerRef(),
                    tx.getPoint() == null ? "0" : String.valueOf(tx.getPoint()),
                    getPointBalance(result));

            return response;

        } catch (ApiKotlinErrorException ex) {
            log.error("{}", ex);
            transactionService.updateTxFailureByException(tx.getId(), ex);
            throw ex;
        } catch (Exception ex) {
            log.error("{}", ex);
            transactionService.updateTxFailureByException(tx.getId(), ex);
            throw ex;
        }
    }

    @Override
    public RedeemByCodeResponseDto redeemByCode(RedeemByCodeRequestDto requestDto) throws ApiErrorException, IOException {
        //todo implement after migration data and implement registry on prem
        //Optional<Merchant> merchant = merchantService.findByMerchantId(requestDto.getBrandId());
        Optional<Merchant> merchant = merchantRepository.findByMerchantId(requestDto.getBrandId());
        if (!merchant.isPresent()) {
            throw new CustomThirdPartyExcepton(new ApiStatus("4102", "Not found data for Merchant"));
        }

        String camapiagnName = "";
        if (Campaign.SHORT_REDEEM.toString().equalsIgnoreCase(requestDto.campaignCode)) {
            ApiEdcCampaignShortRedeemResponseDto campaignDetail = getCampaignDetailByRewardCode(requestDto, camapiagnName);

            camapiagnName = getCampaignDetail(requestDto, camapiagnName, campaignDetail);

        } else {
            ApiEdcCampaignDetailResponseDto campaignDetailResponseDto = apiEdcCampaignService.getCampaignDetail(requestDto.campaignCode);
            List<ApiEdcCampaignDetailResponseDto.Data> campaigns = (List<ApiEdcCampaignDetailResponseDto.Data>) campaignDetailResponseDto.getData();
            if (campaigns.size() == 0) {
                throw new CustomThirdPartyExcepton(new ApiStatus("407", "not found campaignId"));
            }
        }

        TransactionBatch batch = transactionBatchService.genTransactionBatch(requestDto.getBrandId(), requestDto.getOutletId(), requestDto.getTerminalId());
        Transaction tx = new Transaction(batch, REDEEM_MARKUSE);
        tx.setBrandId(requestDto.getBrandId());
        tx.setOutletId(requestDto.getOutletId());
        tx.setTerminalId(requestDto.getTerminalId());
        tx.setCampaignCode(requestDto.getCampaignCode());
        tx.setCouponCode(requestDto.getRewardCode());
        if (!Strings.isNullOrEmpty(requestDto.getTransactionChannel())) {
            tx.setTransactionChannel(TransactionChannel.valueOf(requestDto.getTransactionChannel()));
        }
        if (!Strings.isNullOrEmpty(requestDto.getCampaignType())) {
            tx.setCampaignType(CampaignType.valueOf(requestDto.getCampaignType()));
        }

        tx = transactionService.saveTx(tx);

        try {
            RedeemRequest redeemRequest = new RedeemRequest();
            redeemRequest.setBrandId(requestDto.getBrandId());
            redeemRequest.setOutletId(requestDto.getOutletId());
            redeemRequest.setTerminalId(requestDto.getTerminalId());
            redeemRequest.setTxRefId(batch.getTxRefId());
            redeemRequest.setCampaignCode(requestDto.getCampaignCode());
            redeemRequest.setRewardCode(requestDto.getRewardCode());

            BsRedeemByCodeResponse result = rppBsApi.redeemByCode(redeemRequest);
            tx.setStatus(Status.SUCCESS);
            tx.setStatusCode(result.getStatus().getCode());
            tx.setStatusDescription(result.getStatus().getMessage());

            RedeemByCodeData resultData = result.getData();
            tx.setPoint(resultData.getPointBalance());

            resultData.setTransaction(transactionService.saveTx(tx));

            LocalDateTime createdAt = LocalDateTime.ofInstant(tx.getCreateDate().toInstant(), ZoneId.systemDefault());

            RedeemByCodeResponseDto response = new RedeemByCodeResponseDto(
                    tx.getBrandId(),
                    tx.getOutletId(),
                    tx.getTerminalId(),
                    tx.getTraceId(),
                    tx.getBatchId(),
                    tx.getRefId(),
                    createdAt,
                    merchant.isPresent() ? merchant.get().getTrueyouId() : "",
                    requestDto.getRewardCode(),
                    camapiagnName);
            return response;
        } catch (ApiKotlinErrorException ex) {
            log.error("{}", ex);
            transactionService.updateTxFailureByException(tx.getId(), ex);
            throw ex;
        } catch (Exception ex) {
            log.error("{}", ex);
            transactionService.updateTxFailureByException(tx.getId(), ex);
            throw ex;
        }
    }

    private String getCampaignDetail(RedeemByCodeRequestDto requestDto, String camapiagnName, ApiEdcCampaignShortRedeemResponseDto campaignDetail) {
        if (campaignDetail != null) {
            List<ApiEdcCampaignShortRedeemResponseDto.Data> campaignData = (List<ApiEdcCampaignShortRedeemResponseDto.Data>) campaignDetail.getData();

            Optional<ApiEdcCampaignShortRedeemResponseDto.Data> campaignOp = Optional.ofNullable(campaignData.stream().findFirst().orElseThrow(() -> new CustomThirdPartyExcepton(new ApiStatus("407", "not found campaignId"))));
            if (campaignOp.isPresent()) {
                requestDto.setCampaignCode(campaignOp.get().getCampaignId());
                camapiagnName = campaignOp.get().getCampaignName();
            }
        }
        return camapiagnName;
    }

    private ApiEdcCampaignShortRedeemResponseDto getCampaignDetailByRewardCode(RedeemByCodeRequestDto requestDto, String camapiagnName) {
        ApiEdcCampaignShortRedeemResponseDto campaignShortRedeemResponseDto = apiEdcCampaignService.getCampaignShortRedeem(requestDto.brandId, requestDto.outletId, requestDto.terminalId, requestDto.rewardCode);
        if (campaignShortRedeemResponseDto != null) {
            return campaignShortRedeemResponseDto;
        }

        return null;
    }

    private String getPointBalance(BsRedeemByCampaignResponse result) {
        return result.getData() != null ? String.valueOf(result.getData().getPointBalance()) : "0";
    }
}
