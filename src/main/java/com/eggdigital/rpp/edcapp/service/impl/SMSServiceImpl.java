package com.eggdigital.rpp.edcapp.service.impl;

import com.eggdigital.common.exception.SmsException;
import com.eggdigital.common.util.SMSUtils;
import com.eggdigital.rpp.edcapp.service.SMSService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Locale;
import java.util.Objects;

/**
 * Created by sitthi on 2/14/2018 AD.
 */
@Service
public class SMSServiceImpl implements SMSService {

    private Logger log = LoggerFactory.getLogger(SMSServiceImpl.class);

    @Autowired
    SMSUtils smsUtils;

    @Autowired
    private MessageSource messageSource;

    @Value("${sms.senderName}")
    private String senderName;


    @Override
    public String sendRedeemByCampaign(String toNumber, String campaignName, String traceId) throws SmsException {
        String text = messageSource.getMessage("sms.redeem_by_campaign", null, Locale.getDefault());
        text = text.replace("{{campaign_name}}", campaignName)
                .replace("{{trace_id}}", traceId);
        return sendSMS(toNumber, text);
    }

    @Override
    public String sendRedeemByCode(String toNumber, String campaignName, String rewardCode, String traceId) throws SmsException {
        String text = messageSource.getMessage("sms.redeem_by_code", null, LocaleContextHolder.getLocale());
        text = text.replace("{{campaign_name}}", campaignName)
                .replace("{{reward_code}}", rewardCode)
                .replace("{{trace_id}}", traceId);
        return sendSMS(toNumber, text);
    }

    @Override
    public String sendEarnPoint(String toNumber, BigDecimal total, BigDecimal point) throws SmsException {
        String text = messageSource.getMessage("sms.earn", null, LocaleContextHolder.getLocale());
        text = text.replace("{{total}}", total.toString())
                .replace("{{point}}", point.toString());
        return sendSMS(toNumber, text);
    }


    @Override
    public String sendVoidRedeemByCampaign(String toNumber, String campaignName) throws SmsException {
        String text = messageSource.getMessage("sms.void.redeem_by_campaign", null, LocaleContextHolder.getLocale());
        text = text.replace("{{campaign_name}}", campaignName);
        return sendSMS(toNumber, text);
    }

    @Override
    public String sendVoidEarnPoint(String toNumber, String amount, String point) throws SmsException {
        String text = messageSource.getMessage("sms.void.point", null, LocaleContextHolder.getLocale());
        text = text.replace("{{amount}}", amount)
                .replace("{{point}}", point);
        return sendSMS(toNumber, text);
    }

    @Override
    public String sendVoidPayment(String toNumber, String amount) throws SmsException {
        String text = messageSource.getMessage("sms.void.payment", null, LocaleContextHolder.getLocale());
        text = text.replace("{{amount}}", amount);
        return sendSMS(toNumber, text);
    }

//    @Override
//    public String sendCheckPointTrueCardSMS(String toNumber, String name, String status, String expireDateStr, Integer point, Integer expiringPoint1, Integer expiringPoint2, Integer expiringPoint3) throws SmsException {
//        String text = messageSource.getMessage("sms.checkpoint_truecard", null, LocaleContextHolder.getLocale());
//        Integer year1 = Calendar.getInstance().get(Calendar.YEAR);
//        Integer year2 = year1 + 1;
//        Integer year3 = year2 + 1;
//        text = text.replace("{{name}}", name)
//                .replace("{{status}}", status)
//                .replace("{{expire_date}}", expireDateStr)
//                .replace("{{point}}", point.toString())
//                .concat("\nExpire ").concat(year1.toString()).concat(" : ").concat(expiringPoint1.toString())
//                .concat("\nExpire ").concat(year2.toString()).concat(" : ").concat(expiringPoint2.toString())
//                .concat("\nExpire ").concat(year3.toString()).concat(" : ").concat(expiringPoint3.toString());
//        return sendSMS(toNumber, text);
//    }

//    @Override
//    public String sendCheckPointNonTrueSMS(String toNumber, String name, Integer point, Integer expiringPoint1, Integer expiringPoint2, Integer expiringPoint3) throws SmsException {
//        String text = messageSource.getMessage("sms.checkpoint_nontrue", null, LocaleContextHolder.getLocale());
//        Integer year1 = Calendar.getInstance().get(Calendar.YEAR);
//        Integer year2 = year1 + 1;
//        Integer year3 = year2 + 1;
//        text = text.replace("{{name}}", name)
//                .replace("{{status}}", "ลูกค้าทรู")
//                .replace("{{point}}", point.toString())
//                .concat("\nExpire ").concat(year1.toString()).concat(" : ").concat(expiringPoint1.toString())
//                .concat("\nExpire ").concat(year2.toString()).concat(" : ").concat(expiringPoint2.toString())
//                .concat("\nExpire ").concat(year3.toString()).concat(" : ").concat(expiringPoint3.toString());
//        return sendSMS(toNumber, text);
//    }


//    @Override
//    public String sendVoidIssuePointSMS(String toNumber, String campaignName) throws SmsException {
//        String text = messageSource.getMessage("sms.void_issuepoint", null, LocaleContextHolder.getLocale());
//        text = text.replace("{{campaign_name}}", campaignName);
//        return sendSMS(toNumber, text);
//    }


    private String sendSMS(String toNumber, String text) throws SmsException {
        Objects.requireNonNull(toNumber, "");
        SMSUtils.SmsResponseDto response = smsUtils.send(senderName, toNumber, text);
        return response.getMessage() == null ? "" : response.getMessage().get(0).getStatus().getName();
    }


}
