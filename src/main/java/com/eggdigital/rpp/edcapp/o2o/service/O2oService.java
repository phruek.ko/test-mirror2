package com.eggdigital.rpp.edcapp.o2o.service;

import java.io.IOException;

public interface O2oService {
    String getToken() throws IOException;
}
