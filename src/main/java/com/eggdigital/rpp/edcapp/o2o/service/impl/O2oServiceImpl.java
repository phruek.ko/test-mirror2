package com.eggdigital.rpp.edcapp.o2o.service.impl;

import com.eggdigital.rpp.edcapp.api.model.ApiStatus;
import com.eggdigital.rpp.edcapp.exception.CustomThirdPartyExcepton;
import com.eggdigital.rpp.edcapp.o2o.service.O2oService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Base64;
import java.util.Map;

@Service
public class O2oServiceImpl implements O2oService {

    private final Logger log = LoggerFactory.getLogger(O2oServiceImpl.class);

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private RestTemplate restTemplate;

    @Value("${ms.o2o.uaa.getToken}")
    private String o2oGetToken;

    @Value("${ms.o2o.uaa.username}")
    private String username;

    @Value("${ms.o2o.uaa.password}")
    private String password;

    @Override
    public String getToken() throws IOException {
        try {
            MultiValueMap<String, String> headers = setRequestHeader();
            MultiValueMap<String, String> body = setRequestBody();
            String response = restTemplate.postForObject(o2oGetToken, new HttpEntity<>(body, headers), String.class);
            Map<String, Object> map = objectMapper.readValue(response, new TypeReference<Map<String, String>>() {
            });
            return map.get("access_token") != null ? String.valueOf(map.get("access_token")) : "";
        } catch (Exception e) {
            log.error(null, e);
            throw new CustomThirdPartyExcepton(new ApiStatus("0413", "Could not connect to O2O Service"));
        }
    }

    @NotNull
    private MultiValueMap<String, String> setRequestBody() {
        MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
        body.add("grant_type", "client_credentials");
        return body;
    }

    @NotNull
    private MultiValueMap<String, String> setRequestHeader() {
        byte[] encodedO2oUsernameAndPassword = getEncodeUserPassword(username, password);
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED_VALUE);
        headers.add(HttpHeaders.AUTHORIZATION, "Basic " + new String(encodedO2oUsernameAndPassword));
        return headers;
    }

    private byte[] getEncodeUserPassword(String userName, String password) {
        return Base64.getEncoder().encode((userName + ":" + password).getBytes());
    }

}
