package com.eggdigital.rpp.edcapp.handler;

import com.eggdigital.rpp.edcapp.dto.base.ResponseStatus;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.http.HttpStatus;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by jaruwan.yen on 10/12/2017 AD.
 */
public class ApiError implements Serializable {

    @JsonProperty("status")
    private ResponseStatus status;

    @JsonProperty("errors")
    private List<PropertyError> errors;

    public ApiError(ResponseStatus status, List<PropertyError> errors) {
        this.status = status;
        this.errors = errors;
    }

    public static ApiError.Builder builder() {
        return new ApiError.Builder();
    }

    public ResponseStatus getStatus() {
        return status;
    }

    public void setStatus(ResponseStatus status) {
        this.status = status;
    }

    public List<PropertyError> getErrors() {
        if (errors == null) {
            errors = new ArrayList<>();
        }
        return errors;
    }

    public void setErrors(List<PropertyError> errors) {
        this.errors = errors;
    }

    public static class Builder {

        private ResponseStatus status;
        private List<PropertyError> errors;

        public ApiError.Builder status(HttpStatus httpStatus) {
            if (Objects.isNull(status)) {
                status = new ResponseStatus();
            }
            status.setCode(httpStatus.toString());
            status.setMessage(httpStatus.getReasonPhrase());
            return status(httpStatus.toString(), httpStatus.getReasonPhrase());
        }

        public ApiError.Builder status(ResponseStatus responseStatus) {
            status = responseStatus;
            return this;
        }

        public ApiError.Builder status(String code, String message) {
            if (Objects.isNull(status)) {
                status = new ResponseStatus();
            }
            status.setCode(code);
            status.setMessage(message);
            return this;
        }

        private List<PropertyError> getErrors() {
            if (Objects.isNull(errors)) {
                errors = new ArrayList<>();
            }
            return errors;
        }

        public ApiError.Builder addError(String property, String message) {
            getErrors().add(new PropertyError(property, message));
            return this;
        }

        public ApiError.Builder errors(List<PropertyError> errors) {
            this.errors = errors;
            return this;
        }

        public ApiError build() {
            return new ApiError(status, errors);
        }
    }
}
