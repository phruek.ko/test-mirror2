package com.eggdigital.rpp.edcapp.handler;

/**
 * Created by jaruwan.yen on 10/12/2017 AD.
 */
public class ApiErrorException extends Exception {

    private final ApiError apiError;

    public ApiErrorException(String message, ApiError apiError) {
        super(message);
        this.apiError = apiError;
    }

    public ApiError getApiError() {
        return apiError;
    }
}
