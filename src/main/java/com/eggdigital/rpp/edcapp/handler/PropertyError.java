package com.eggdigital.rpp.edcapp.handler;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by jaruwan.yen on 10/12/2017 AD.
 */
public class PropertyError {
    @JsonProperty("property")
    private String property;

    @JsonProperty("message")
    private String message;

    public PropertyError(String property, String message) {
        this.property = property;
        this.message = message;
    }

    public PropertyError() {
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
