package com.eggdigital.rpp.edcapp.api;

import com.eggdigital.rpp.edcapp.api.model.ApiError;
import com.eggdigital.rpp.edcapp.api.model.ApiStatus;
import com.eggdigital.rpp.edcapp.ms.dto.AbstractMsResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.http.*;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class ApiSender {

    private Logger log = LoggerFactory.getLogger(ApiSender.class);

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private ApplicationContext applicationContext;

    private String getUrlFromMicroservice(Microservice ms) {

        Environment env = applicationContext.getEnvironment();

        switch (ms) {
            case REDEEM_BY_CAMPAIGN:
                return env.getProperty("ms.bs.redeem.campaign");
            case REDEEM_BY_CODE:
                return env.getProperty("ms.bs.redeem.code");
            case REDEEM_REVERSAL:
                return env.getProperty("ms.bs.redeem.reversal");
            case CUSTOMER_CARD_INFO:
                return env.getProperty("ms.bs.customer.card");
            case CUSTOMER_CHECK_POINT:
                return env.getProperty("ms.bs.customer.checkpoints");
            case POINT_ISSUE:
                return env.getProperty("ms.bs.point.issue");
            default:
                return "";
        }
    }

    public <T> T get(Microservice ms, Class<T> responseType) throws ApiKotlinErrorException {
        return send(ms, HttpMethod.GET, responseType, null, null, null, true);
    }

    public <T> T get(Microservice ms, Class<T> responseType, Map<String, String> params) throws ApiKotlinErrorException {
        return send(ms, HttpMethod.GET, responseType, null, params, null, true);
    }

    public <T> T get(Microservice ms, Class<T> responseType, Map<String, String> params, Map<String, String> uriVariables) throws ApiKotlinErrorException {
        return send(ms, HttpMethod.GET, responseType, uriVariables, params, null, true);
    }

    public <T> T get(Microservice ms, Class<T> responseType, Map<String, String> params, Map<String, String> uriVariables, boolean checkSuccess) throws ApiKotlinErrorException {
        return send(ms, HttpMethod.GET, responseType, uriVariables, params, null, checkSuccess);
    }

    public <T> T post(Microservice ms, Class<T> responseType, Object body) throws ApiKotlinErrorException {
        return send(ms, HttpMethod.POST, responseType, null, null, body, true);
    }

    public <T> T post(Microservice ms, Class<T> responseType, Object body, Map<String, String> uriVariables) throws ApiKotlinErrorException {
        return send(ms, HttpMethod.POST, responseType, uriVariables, null, body, true);
    }

    public <T> T post(Microservice ms, Class<T> responseType, Object body, Map<String, String> uriVariables, boolean checkSuccess) throws ApiKotlinErrorException {
        return send(ms, HttpMethod.POST, responseType, uriVariables, null, body, checkSuccess);
    }

    public <T> T post(Microservice ms, Class<T> responseType, Map<String, String> body) throws ApiKotlinErrorException {
        return send(ms, HttpMethod.POST, responseType, null, body, null, true);
    }

    public <T> T post(Microservice ms, Class<T> responseType, Map<String, String> body, Map<String, String> uriVariables) throws ApiKotlinErrorException {
        return send(ms, HttpMethod.POST, responseType, uriVariables, body, null, true);
    }

    public <T> T post(Microservice ms, Class<T> responseType, Map<String, String> body, Map<String, String> uriVariables, boolean checkSuccess) throws ApiKotlinErrorException {
        return send(ms, HttpMethod.POST, responseType, uriVariables, body, null, checkSuccess);
    }

    public <T> T put(Microservice ms, Class<T> responseType, Map<String, String> body) throws ApiKotlinErrorException {
        return send(ms, HttpMethod.PUT, responseType, null, body, null, true);
    }

    public <T> T put(Microservice ms, Class<T> responseType, Map<String, String> body, Map<String, String> uriVariables) throws ApiKotlinErrorException {
        return send(ms, HttpMethod.PUT, responseType, uriVariables, body, null, true);
    }

    public <T> T put(Microservice ms, Class<T> responseType, Map<String, String> body, Map<String, String> uriVariables, boolean checkSuccess) throws ApiKotlinErrorException {
        return send(ms, HttpMethod.PUT, responseType, uriVariables, body, null, checkSuccess);
    }

    public <T> T exchange(Microservice ms,
                          HttpMethod httpMethod,
                          Class<T> responseType,
                          Map<String, String> uriVariables,
                          Map<String, String> paramsOrBody,
                          Object object,
                          boolean checkSuccess) throws ApiKotlinErrorException {
        return send(ms, httpMethod, responseType, uriVariables, paramsOrBody, object, checkSuccess);
    }

    private <T> T send(Microservice ms, HttpMethod httpMethod,
                       Class<T> responseType,
                       Map<String, String> uriVariables,
                       Map<String, String> paramsOrBody,
                       Object body,
                       boolean checkSuccess) throws ApiKotlinErrorException {

        log.info("request api {} [{}] : {} {} {} ", ms.toString(),
                httpMethod.toString(),
                getJson(uriVariables),
                getJson(paramsOrBody),
                getJson(body));

        ResponseEntity<T> response = null;

        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_UTF8_VALUE);

        String url = getUrlFromMicroservice(ms);
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);

        restTemplate.setErrorHandler(new ResponseErrorHandler() {
            @Override
            public boolean hasError(ClientHttpResponse response) throws IOException {
                return false;
            }

            @Override
            public void handleError(ClientHttpResponse response) throws IOException {

            }
        });

        try {

            switch (httpMethod) {
                case GET:

                    HttpEntity<String> entity = new HttpEntity<>(headers);

                    if (paramsOrBody != null) {
                        paramsOrBody.forEach(builder::queryParam);
                    }
                    if (uriVariables == null) {
                        response = restTemplate.exchange(builder.build().toUriString(), httpMethod, entity, responseType);
                    } else {
                        response = restTemplate.exchange(builder.build().toUriString(), httpMethod, entity, responseType, uriVariables);
                    }

                    break;

                case POST:
                case PUT:

                    if (paramsOrBody != null) {

                        HttpEntity<Map<String, String>> entityMap = new HttpEntity<>(paramsOrBody, headers);
                        if (uriVariables == null) {
                            response = restTemplate.exchange(builder.build().toUriString(), httpMethod, entityMap, responseType);
                        } else {
                            response = restTemplate.exchange(builder.build().toUriString(), httpMethod, entityMap, responseType, uriVariables);
                        }
                    }

                    if (body != null) {

                        HttpEntity<Object> entityObject = new HttpEntity<>(body, headers);
                        if (uriVariables == null) {
                            response = restTemplate.exchange(builder.build().toUriString(), httpMethod, entityObject, responseType);
                        } else {
                            response = restTemplate.exchange(builder.build().toUriString(), httpMethod, entityObject, responseType, uriVariables);
                        }
                    }
                    break;

                default:
                    break;
            }

            assert response != null;

            AbstractMsResponse<T> responseBody = (AbstractMsResponse<T>) response.getBody();
            log.info("response api {} : {}", ms.toString(), getJson(response.getBody()));
            if (checkSuccess) {

                if (!response.getStatusCode().is2xxSuccessful()) {
                    ApiStatus apiStatus = new ApiStatus(String.valueOf(response.getStatusCodeValue()),
                            response.getStatusCode().getReasonPhrase());
                    List<ApiError> apiErrors = new ArrayList<>();
                    throw new ApiKotlinErrorException(response.getStatusCode().getReasonPhrase(),
                            apiErrors,
                            apiStatus);
                }

                if (!responseBody.getStatus().getCode().equals(String.valueOf(HttpStatus.OK.value()))) {
                    ApiStatus apiStatus = new ApiStatus(responseBody.getStatus().getCode(),
                            responseBody.getStatus().getMessage() == null ? "" : responseBody.getStatus().getMessage());

                    List<ApiError> apiErrors = new ArrayList<>();
                    if (responseBody.getErrors() != null) {
                        responseBody.getErrors().forEach(propertyError -> {
                            ApiError apiError = new ApiError(propertyError.getProperty(),
                                    propertyError.getMessage());
                            apiErrors.add(apiError);
                        });
                    }
                    throw new ApiKotlinErrorException(responseBody.getStatus().getMessage(),
                            apiErrors,
                            apiStatus);
                }
            }
        } catch (Exception ex) {
            log.error("{}", ex);
            throw ex;
        }
        return response.getBody();
    }

    private String getJson(Object o) {
        String json = null;
        try {
            json = objectMapper.writeValueAsString(o);
        } catch (JsonProcessingException e) {
            log.error("{}", e);
        }
        return json;
    }

    public enum Microservice {
        REDEEM_BY_CAMPAIGN,
        REDEEM_BY_CODE,
        REDEEM_REVERSAL,
        CUSTOMER_CARD_INFO,
        CUSTOMER_CHECK_POINT,
        POINT_ISSUE,
    }

}