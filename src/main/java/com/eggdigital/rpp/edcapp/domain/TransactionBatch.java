package com.eggdigital.rpp.edcapp.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Objects;

/**
 * Created by bombay on 10/17/2017 AD.
 */
@Entity
@Table(name = "transaction_batch")
public class TransactionBatch extends AbstractAuditEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", columnDefinition = "number(19,0)")
    private BigInteger id;

    @Column(name = "batch_id", length = 6)
    private String batchId;

    @Column(name = "trace_id", length = 6)
    private String traceId;

    @Column(name = "tx_ref_id")
    private String txRefId;

    @Column(name = "brand_id")
    private String brandId;

    @Column(name = "outlet_id")
    private String outletId;

    @Column(name = "terminal_id")
    private String terminalId;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Status status;

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getTraceId() {
        return traceId;
    }

    public void setTraceId(String traceId) {
        this.traceId = traceId;
    }

    public String getTxRefId() {
        return txRefId;
    }

    public void setTxRefId(String txRefId) {
        this.txRefId = txRefId;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getOutletId() {
        return outletId;
    }

    public void setOutletId(String outletId) {
        this.outletId = outletId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TransactionBatch)) return false;
        TransactionBatch that = (TransactionBatch) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
