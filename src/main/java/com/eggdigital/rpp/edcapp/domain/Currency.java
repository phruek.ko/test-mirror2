package com.eggdigital.rpp.edcapp.domain;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Objects;

/**
 * Created by bombay on 10/17/2017 AD.
 */
public enum Currency {
    THB("thb");

    private String value;

    Currency(String value) {
        this.value = value;
    }

    public static Currency fromString(String s) {
        if (s == null || s.isEmpty()) {
            return null;
        }
        for (Currency currency : Currency.values()) {
            if (Objects.equals(currency.getValue(), s)) {
                return currency;
            }
        }
        return null;
    }

    @JsonValue
    public String getValue() {
        return value;
    }
}
