package com.eggdigital.rpp.edcapp.domain;

public enum Estamp {

    EARN_ESTAMP("661%"), MARKUSE_ESTAMP("662%");

    private String parameterLike;

    private Estamp(String parameterLike) {
        this.parameterLike = parameterLike;
    }

    public String getParameterLike() {
        return parameterLike;
    }

    public void setParameterLike(String parameterLike) {
        this.parameterLike = parameterLike;
    }
}
