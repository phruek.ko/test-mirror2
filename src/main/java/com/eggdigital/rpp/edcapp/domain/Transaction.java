package com.eggdigital.rpp.edcapp.domain;

import com.eggdigital.rpp.edcapp.enumuration.CampaignType;
import com.eggdigital.rpp.edcapp.enumuration.TransactionChannel;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "transaction",
        indexes = {
                @Index(name = "transaction_dx01", columnList = "ref_id")
        })
public class Transaction extends AbstractAuditEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", columnDefinition = "number(19,0)")
    private BigInteger id;

    @Column(name = "ref_id")
    private String refId;

    @Column(name = "settle_date")
    private Date settleDate;

    @Column(name = "settle_by")
    private String settleBy;

    @Enumerated(EnumType.STRING)
    @Column(name = "settle_status")
    private Status settleStatus = Status.PENDING;

    @Column(name = "brand_id")
    private String brandId;

    @Column(name = "outlet_id")
    private String outletId;

    @Column(name = "terminal_id")
    private String terminalId;

    @Enumerated(EnumType.STRING)
    @Column(name = "transaction_type")
    private TransactionType transactionType;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Status status;

    @Enumerated(EnumType.STRING)
    @Column(name = "account_type")
    private AccountType accountType;

    @Column(name = "customer_ref")
    private String customerRef;

    @Column(name = "campaign_code")
    private String campaignCode;

    @Column(name = "coupon_code")
    private String couponCode;

    @Column(name = "amount")
    private String amount = "0";

    @Column(name = "point")
    private Integer point = 0;

    @Column(name = "payment_code")
    private String paymentCode;

    @Enumerated(EnumType.STRING)
    @Column(name = "payment_method")
    private PaymentMethod paymentMethod;

    @Enumerated(EnumType.STRING)
    @Column(name = "currency")
    private Currency currency;

    @Column(name = "batch_id")
    private String batchId;

    @Column(name = "trace_id")
    private String traceId;

    @Column(name = "status_code")
    private String statusCode;

    @Column(name = "status_description")
    private String statusDescription;

    @Enumerated(EnumType.STRING)
    @Column(name = "transaction_channel")
    private TransactionChannel transactionChannel;

    @Enumerated(EnumType.STRING)
    @Column(name = "campaign_type")
    private CampaignType campaignType;

    @Column(name = "mobile")
    private String mobile;

    public Transaction() {
    }

    public Transaction(TransactionBatch batch, TransactionType transactionType) {
        this.refId = batch.getTxRefId();
        this.traceId = batch.getTraceId();
        this.batchId = batch.getBatchId();
        this.transactionType = transactionType;
    }

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public Date getSettleDate() {
        return settleDate;
    }

    public void setSettleDate(Date settleDate) {
        this.settleDate = settleDate;
    }

    public String getSettleBy() {
        return settleBy;
    }

    public void setSettleBy(String settleBy) {
        this.settleBy = settleBy;
    }

    public Status getSettleStatus() {
        return settleStatus;
    }

    public void setSettleStatus(Status settleStatus) {
        this.settleStatus = settleStatus;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getOutletId() {
        return outletId;
    }

    public void setOutletId(String outletId) {
        this.outletId = outletId;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }

    public String getCustomerRef() {
        return customerRef;
    }

    public void setCustomerRef(String customerRef) {
        this.customerRef = customerRef;
    }

    public String getCampaignCode() {
        return campaignCode;
    }

    public void setCampaignCode(String campaignCode) {
        this.campaignCode = campaignCode;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public Integer getPoint() {
        return point;
    }

    public void setPoint(Integer point) {
        this.point = point;
    }

    public String getPaymentCode() {
        return paymentCode;
    }

    public void setPaymentCode(String paymentCode) {
        this.paymentCode = paymentCode;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getTraceId() {
        return traceId;
    }

    public void setTraceId(String traceId) {
        this.traceId = traceId;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    public TransactionChannel getTransactionChannel() {
        return transactionChannel;
    }

    public void setTransactionChannel(TransactionChannel transactionChannel) {
        this.transactionChannel = transactionChannel;
    }

    public CampaignType getCampaignType() {
        return campaignType;
    }

    public void setCampaignType(CampaignType campaignType) {
        this.campaignType = campaignType;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Transaction)) return false;
        Transaction that = (Transaction) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
