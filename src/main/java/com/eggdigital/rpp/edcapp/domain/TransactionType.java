package com.eggdigital.rpp.edcapp.domain;

/**
 * Created by bombay on 10/16/2017 AD.
 */
public enum TransactionType {
    POINT_EARN,
    POINT_CANCEL,
    REDEEM_BENEFIT,
    REDEEM_MARKUSE,
    REDEEM_ROLLBACK,
    PAYMENT_CHARGE,
    PAYMENT_CANCEL,
    PAYMENT_REFUND,
    CUSTOMER_CARD_SLIP
}
