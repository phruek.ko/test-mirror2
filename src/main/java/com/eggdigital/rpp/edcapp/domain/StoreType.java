package com.eggdigital.rpp.edcapp.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

/**
 * Created by wanatchapong on 1/11/2017 AD.
 */
@Entity
@Table(name = "store_type")
public class StoreType implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty("id")
    @Id
    @Column(name = "ID", nullable = false)
    private Integer id;

    @JsonProperty("name")
    @Column(name = "name", nullable = false)
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StoreType storeType = (StoreType) o;
        return Objects.equals(id, storeType.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
