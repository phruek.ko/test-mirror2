package com.eggdigital.rpp.edcapp.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "slip_channel")
public class SlipChannel extends AbstractAuditEntity implements Serializable {

    @Id
    @Column(name = "thai_id", nullable = false)
    private String thaiId;

    @Enumerated(EnumType.STRING)
    @Column(name = "channel")
    private com.eggdigital.rpp.edcapp.model.SlipChannel channel;

    @Column(name = "value")
    private String value;

    public SlipChannel() {
    }

    public String getThaiId() {
        return thaiId;
    }

    public void setThaiId(String thaiId) {
        this.thaiId = thaiId;
    }

    public com.eggdigital.rpp.edcapp.model.SlipChannel getChannel() {
        return channel;
    }

    public void setChannel(com.eggdigital.rpp.edcapp.model.SlipChannel channel) {
        this.channel = channel;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
