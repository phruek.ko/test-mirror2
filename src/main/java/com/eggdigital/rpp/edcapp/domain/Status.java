package com.eggdigital.rpp.edcapp.domain;

/**
 * Created by bombay on 10/16/2017 AD.
 */
public enum Status {
    PENDING,
    // success
    // 1. client -- request --> remote
    // 2. client <-- response success -- remote
    SUCCESS,
    // failure
    // 1. client -- request --> remote
    // 2. client <-- response failed -- remote
    FAIL,
    // timeout
    // 1. client -- request --> remote
    // 2. client <-- (X) no response -- remote
    SETTLE,
}
