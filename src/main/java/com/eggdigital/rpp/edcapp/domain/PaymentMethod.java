package com.eggdigital.rpp.edcapp.domain;

/**
 * Created by bombay on 10/17/2017 AD.
 */
public enum PaymentMethod {
    WALLET,
    TRUEYOU_MASTER_CARD,
    ALIPAY,
    CREDIT_CARD,
    DEBIT_CARD,
    CASH
}
