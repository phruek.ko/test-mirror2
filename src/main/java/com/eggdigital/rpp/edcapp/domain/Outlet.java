package com.eggdigital.rpp.edcapp.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;

/**
 * Created by bombay on 10/20/2017 AD.
 */
@Entity
@Table(name = "outlet")
public class Outlet  implements Serializable {

    public Outlet() {
    }

    public Outlet(String outletId, String outletNameTh, String outletNameEn, String merchantId) {
        this.outletId = outletId;
        this.outletNameTh = outletNameTh;
        this.outletNameEn = outletNameEn;
        this.merchantId = merchantId;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, columnDefinition = "number(19,0)")
    private BigInteger id;

    /**
     * OUTLET_ID
     */
    @Column(name = "outlet_id", unique = true)
    private String outletId;

    /**
     * ชื่อร้านภาษาไทย (TH)
     */
    @Column(name = "outlet_name_th")
    private String outletNameTh;
    /**
     * ชื่อร้านภาษาอังกฤษ (EN)
     */
    @Column(name = "outlet_name_en")
    private String outletNameEn;

    @Column(name = "merchant_id")
    private String merchantId;



    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public String getOutletId() {
        return outletId;
    }

    public void setOutletId(String outletId) {
        this.outletId = outletId;
    }

    public String getOutletNameTh() {
        return outletNameTh;
    }

    public void setOutletNameTh(String outletNameTh) {
        this.outletNameTh = outletNameTh;
    }

    public String getOutletNameEn() {
        return outletNameEn;
    }

    public void setOutletNameEn(String outletNameEn) {
        this.outletNameEn = outletNameEn;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }
}
