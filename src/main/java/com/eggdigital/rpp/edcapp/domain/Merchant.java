package com.eggdigital.rpp.edcapp.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by wanatchapong on 1/6/2017 AD.
 */
@Entity
@Table(name = "merchant")
public class Merchant implements Serializable {

    private static final long serialVersionUID = 1L;

    public Merchant() {
    }

    public Merchant(String storeNameTh, String storeNameEn, String merchantId, String trueyouId, String tmnApiKey, String tmnMerchantId) {
        this.storeNameTh = storeNameTh;
        this.storeNameEn = storeNameEn;
        this.merchantId = merchantId;
        this.trueyouId = trueyouId;
        this.tmnApiKey = tmnApiKey;
        this.tmnMerchantId = tmnMerchantId;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    /**
     * ชื่อร้านภาษาไทย (TH)
     */
    @Column(name = "store_name_th")
    private String storeNameTh;
    /**
     * ชื่อร้านภาษาอังกฤษ (EN)
     */
    @Column(name = "store_name_en")
    private String storeNameEn;


    @Column(name = "merchant_id", unique = true)
    private String merchantId;

    @Column(name = "trueyou_id")
    private String trueyouId;

    @Column(name = "tmn_api_key")
    private String tmnApiKey;

    @Column(name = "tmn_merchant_id")
    private String tmnMerchantId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStoreNameTh() {
        return storeNameTh;
    }

    public void setStoreNameTh(String storeNameTh) {
        this.storeNameTh = storeNameTh;
    }

    public String getStoreNameEn() {
        return storeNameEn;
    }

    public void setStoreNameEn(String storeNameEn) {
        this.storeNameEn = storeNameEn;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getTrueyouId() {
        return trueyouId;
    }

    public void setTrueyouId(String trueyouId) {
        this.trueyouId = trueyouId;
    }

    public String getTmnApiKey() {
        return tmnApiKey;
    }

    public void setTmnApiKey(String tmnApiKey) {
        this.tmnApiKey = tmnApiKey;
    }

    public String getTmnMerchantId() {
        return tmnMerchantId;
    }

    public void setTmnMerchantId(String tmnMerchantId) {
        this.tmnMerchantId = tmnMerchantId;
    }
}
