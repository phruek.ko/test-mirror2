package com.eggdigital.rpp.edcapp.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.google.common.base.Strings;

/**
 * Created by jaruwan.yen on 10/12/2017 AD.
 */
public enum AccountType {
    THAIID,
    MOBILE,
    TRUECARD;

    @JsonCreator
    public static AccountType fromType(String type) {
        if (Strings.isNullOrEmpty(type)) {
            return null;
        }

        for (AccountType accountType : values()) {
            if (accountType.name().equalsIgnoreCase(type)) {
                return accountType;
            }
        }

        return null;
    }
}
